/*actions joueurs*/
//---------------------------------------------------------------------------------------

/**on met à jour le placement de la carte dans le jeu**/
function discardCard(battleRow, battlePlace){
	if (battleCardSelected !== undefined && currentTurn){
		var clientAction = {
				action : "discardCard",
				cardId : battleCardSelected.name.toString()
		};
		webSocket.send(JSON.stringify(clientAction));
		var discardCardButton = document.getElementById("discardCardButton");
		discardCardButton.disabled = true;
		battleCardSelected = undefined;
	}
}

function endDiscardHandTurn(){
	if (currentTurn){
		if (battleCardSelected !== undefined){
			battleCardSelected.alpha = 1;
			battleCardSelected = undefined;
		}
		clearEventAndHighlight();
		var clientAction = {
				action : "endDiscardHandTurn",
		};
		webSocket.send(JSON.stringify(clientAction));
	}
}

//---------------------------------------------------------------------------------------
/*fin actions joueurs*/

/*ajout d'évènements*/
//---------------------------------------------------------------------------------------

function addCardSelectionEvent(card){
	if (currentTurn &&  !card.hasEventListener("click")){
		addObjectWithEvents(card);
		card.addEventListener("click", function(event) {
			var discardCardButton = document.getElementById("discardCardButton");
			if (battleCardSelected === undefined){
				//sélection de la carte
				card.alpha = 0.6;
				battleCardSelected = card;
				discardCardButton.disabled = false;
			} else if(battleCardSelected.name === card.name){
				//désélection
				battleCardSelected = undefined;
				card.alpha =1;
				discardCardButton.disabled = true;
			}
			stage.update(event);
			combatStage.update(event);
		})
	}
}

//---------------------------------------------------------------------------------------
/*fin ajout d'évènements*/



/*réception des messages du serveur*/
//---------------------------------------------------------------------------------------

function removeCardDisplay(serverAction){
	var card = stage.getChildByName("card" + serverAction.name.toString());
	if (card){
		stage.removeChild(card);
		stage.update();
	}
}


/**ajoute les boutons correspondant aux actions possibles pendant cette étape**/
function addDiscardHandTurnButton(serverAction){
	
	var actionMenu = document.getElementById("buttonContainer");
	
	//bouton de fin de tour
	var endTurnButton =  document.createElement("button");
	endTurnButton.setAttribute("id", "endTurnButton");
	endTurnButton.setAttribute("class", "gameButton");
	endTurnButton.setAttribute("type", "button");
	endTurnButton.setAttribute("onclick", "this.disabled=true;endDiscardHandTurn();");
	endTurnButton.innerHTML = "Next step";
	endTurnButton.disabled = true;
	
	actionMenu.appendChild(endTurnButton);
	
	var discardCard =  document.createElement("button");
	discardCard.setAttribute("id", "discardCardButton");
	discardCard.setAttribute("class", "gameButton");
	discardCard.setAttribute("type", "button");
	discardCard.setAttribute("onclick", "this.disabled=true;discardCard();");
	discardCard.innerHTML = "Discard selected card";
	discardCard.disabled = true;
	
	actionMenu.appendChild(discardCard);
}

/**affiche le choix des cartes de combat**/
function displayCardChoice(serverAction){
	var cardNumber = 0;
	if (stage.numChildren < 2){
		cardNumber = 1;
	}else{
		cardNumber = stage.numChildren;
	}
	setCanvasPixelHeight((Math.floor(cardNumber/2) + 1) * (0.75 * cardHeight + 10));
	var card = drawCombatCard(serverAction);
	card.scaleX = 0.75;
	card.scaleY = 0.75;
	card.y = 5 + Math.floor((cardNumber-1)/2) * (0.75 * cardHeight + 10);
	card.x = ((cardNumber - 1)%2) * (0.75 * cardWidth + 10) + 5;
	addCardSelectionEvent(card);
	stage.addChild(card);
	stage.update();
}

//---------------------------------------------------------------------------------------
/*fin réception des messages du serveur*/