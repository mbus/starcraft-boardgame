package gameEntities.methods;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import gameEntities.GameConstants;
import gameEntities.StarcraftGame;
import gameEntities.StarcraftPlayer;
import gameEntities.gameMap.Galaxy;
import gameEntities.playerItems.CombatCard;
import gameEntities.playerItems.EventCard;

public class EventCardHandler {
	
	/**indique au joueur quelles places sont possibles pour l'unitée sélectionnée**/
	public void askFreeUnitsPlacements(String player, StarcraftGame game){
		Galaxy galaxy = game.getGalaxy();
		StarcraftPlayer starcraftPlayer = game.getPlayerList().get(player);
		try {
			ArrayList<int[]> validCoordinates = galaxy.getEventUnitPlacement(starcraftPlayer, game);
			JSONArray coordinateArray = new JSONArray();
			for (int[] coordinate:validCoordinates){
				JSONObject coordinateJS = new JSONObject()
						.put("coordinate", String.valueOf(coordinate[0]) + "." + String.valueOf(coordinate[1]))
						.put("areaId", coordinate[2]);
				coordinateArray.put(coordinateJS);
			}
			JSONObject askFreeUnitsPlacements = new JSONObject()
							.put("action", "askFreeUnitsPlacements")
							.put("coordinates", coordinateArray);
			GlobalMethods.sendPlayerAction(player, askFreeUnitsPlacements);
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
	
	public void displayEventCardInDeck(String player, StarcraftGame game){
		StarcraftPlayer starcraftPlayer = game.getPlayerList().get(player);
		for (EventCard card:game.getEventCards1()){
			JSONObject eventCardJS = card.getCardJS("displayCardInHand");
			try {
				eventCardJS.put("color", starcraftPlayer.getPlayerColor());
			} catch (JSONException e) {
				e.printStackTrace();
			}
			GlobalMethods.sendPlayerAction(player, eventCardJS);
		}
	}
	
	public void activeDrawingEventCards(String player, StarcraftGame game){
		StarcraftPlayer starcraftPlayer = game.getPlayer(player);
		if (starcraftPlayer.getSpecies()!=null){
			try {
				JSONObject activeDrawingEventCards = new JSONObject()
						.put("action", "activeDrawingEventCards");
				GlobalMethods.sendPlayerAction(player, activeDrawingEventCards);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	/**Ajoute à la partie info un indicateur des cartes de combats actives**/
	public void displayActiveEventCardNumber(String player, StarcraftGame game){
		StarcraftPlayer starcraftPlayer = game.getPlayerList().get(player);
		if (starcraftPlayer.getSpecies()!=null){
			int activeEventCards = starcraftPlayer.getEventCards().getUsableEventCards().size();
			if (starcraftPlayer.getEventCards().getStartegyCard() != null){
				activeEventCards++;
			}
			try {
				JSONObject displayActiveCardNumber = new JSONObject()
						.put("action", "displayActiveEventCardNumber")
						.put("cardNumber", activeEventCards);
				GlobalMethods.sendPlayerAction(player, displayActiveCardNumber);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public void fetchActiveCardNumber(String player, StarcraftGame game){
		for (String playerName:game.getPlayerList().keySet()){
			if (!playerName.equals(player)){
				displayActiveEventCardNumber2(playerName, game, player);
			}
		}
	}
	
	public void sendUpdateActiveCardNumber(String player, StarcraftGame game){
		for (String playerName:game.getPlayerList().keySet()){
			if (!playerName.equals(player)){
				displayActiveEventCardNumber2(player, game, playerName);
			}
		}
	}
	
	/**Ajoute à la partie info un indicateur des cartes de combats actives**/
	private void displayActiveEventCardNumber2(String player, StarcraftGame game, String playerName){
		StarcraftPlayer starcraftPlayer = game.getPlayerList().get(player);
		if (starcraftPlayer.getSpecies()!=null){
			int activeEventCards = starcraftPlayer.getEventCards().getUsableEventCards().size();
			if (starcraftPlayer.getEventCards().getStartegyCard() != null){
				activeEventCards++;
			}
			try {
				JSONObject factionChoice = new JSONObject()
						.put("action", "displayActiveEventCardNumber2")
						.put("playerName", player)
						.put("cardNumber", activeEventCards);
				GlobalMethods.sendPlayerAction(playerName, factionChoice);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public void updateDrawnEventCardNumber(StarcraftPlayer player){
		if (player.getSpecies()!=null){
			try {
				JSONObject updateDrawnEventCardNumber = new JSONObject()
						.put("action", "updateDrawnEventCardNumber")
						.put("cardNumber", player.getEventCards().getEventCardList().size());
				GlobalMethods.sendPlayerAction(player.getName(), updateDrawnEventCardNumber);
				StarcraftGame game = player.getGame();
				for (String playerName:game.getPlayerList().keySet()){
					if (!playerName.equals(player.getName())){
						JSONObject updateDrawnEventCardNumber2 = new JSONObject()
								.put("action", "updateDrawnEventCardNumber2")
								.put("playerName", player.getName())
								.put("cardNumber", player.getEventCards().getEventCardList().size());
						GlobalMethods.sendPlayerAction(playerName, updateDrawnEventCardNumber2);
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public void displayDrawnEventCardNumber(String player, StarcraftGame game){
		StarcraftPlayer starcraftPlayer = game.getPlayer(player);
		if (starcraftPlayer.getSpecies()!=null){
			try {
				JSONObject updateDrawnEventCardNumber = new JSONObject()
						.put("action", "updateDrawnEventCardNumber")
						.put("cardNumber", starcraftPlayer.getEventCards().getEventCardList().size());
				GlobalMethods.sendPlayerAction(player, updateDrawnEventCardNumber);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public void fetchDrawnEventCardNumber(String player, StarcraftGame game){
		for (String playerName:game.getPlayerList().keySet()){
			StarcraftPlayer starcraftPlayer = game.getPlayer(playerName);
			if (starcraftPlayer.getSpecies()!=null){
				try {
					JSONObject updateDrawnEventCardNumber2 = new JSONObject()
							.put("action", "updateDrawnEventCardNumber2")
							.put("playerName", playerName)
							.put("cardNumber", starcraftPlayer.getEventCards().getEventCardList().size());
					GlobalMethods.sendPlayerAction(player, updateDrawnEventCardNumber2);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public void displayUsableCards(String player, StarcraftGame game, String turnName){
		if (game.getPlayerCurrentlyPlaying().equals(player)){
			StarcraftPlayer starcraftPlayer = game.getPlayer(player);
			ArrayList<CombatCard> combatCards= starcraftPlayer.getSpecialActions().getTriggeredCombatCards(turnName);
			ArrayList<EventCard> eventCards= starcraftPlayer.getSpecialActions().getTriggeredEventCards(turnName);
			if (combatCards.size() + eventCards.size() > 0){
				try {
					JSONArray cardListJS = new JSONArray();
					for (EventCard card:eventCards){
						JSONObject cardJS = new JSONObject()
								.put("id", card.getId())
								.put("color", starcraftPlayer.getPlayerColor())
								.put("name", card.getName())
								.put("text", card.getText());
						cardListJS.put(cardJS);
					}
					for (CombatCard card:combatCards){
						JSONObject cardJS = new JSONObject()
								.put("id", card.getId())
								.put("color", starcraftPlayer.getPlayerColor())
								.put("name", card.getName())
								.put("text", card.getText());
						cardListJS.put(cardJS);
					}
					JSONObject displayBuyableCards = new JSONObject()
							.put("action", "displayBuyableCards")
							.put("cardList", cardListJS);
					GlobalMethods.sendPlayerAction(player, displayBuyableCards);
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public void askRedrawRoad(String player, StarcraftGame game, int[] road, String color){
		try {
			JSONObject askRedrawRoad = new JSONObject()
					.put("action", "askRedrawRoad")
					.put("coordinates", Integer.toString(road[0])+"."+Integer.toString(road[1]))
					.put("roadPosition", road[2])
					.put("color", color);
			GlobalMethods.sendPlayerAction(player, askRedrawRoad);
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
	
	/** Ajoute les boutons pour la fin du tour**/
	public void addSwapZRoadButtons(String player, StarcraftGame game){
		if (game.getPlayerCurrentlyPlaying().equals(player)){
			try {
				JSONObject addSwapZRoadButtons = new JSONObject()
						.put("action", "addSwapZRoadButtons");
				GlobalMethods.sendPlayerAction(player, addSwapZRoadButtons);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
	}
	
	/** Ajoute les boutons pour la fin du tour**/
	public void addSkipTurnButton(String player, StarcraftGame game){
		if (game.getPlayerCurrentlyPlaying().equals(player)){
			try {
				JSONObject addSkipTurnButton = new JSONObject()
						.put("action", "addSkipTurnButton");
				GlobalMethods.sendPlayerAction(player, addSkipTurnButton);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
	}
	
	/** Ajoute les boutons pour la fin du tour**/
	public void addChooseBuildingButton(String player, StarcraftGame game){
		if (game.getPlayerCurrentlyPlaying().equals(player)){
			try {
				JSONObject addSkipTurnButton = new JSONObject()
						.put("action", "addChooseBuildingButton");
				GlobalMethods.sendPlayerAction(player, addSkipTurnButton);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
	}
	
	/**active les routes que l'on peut sélectionner**/
	public void activateSwapRoads(String player, String coordinates, int roadPosition, StarcraftGame game){
		if (game.getPlayerCurrentlyPlaying().equals(player)){
			int separatorIndex = coordinates.indexOf('.');
			int xCoord  = Integer.parseInt(coordinates.substring(0, separatorIndex));
			int yCoord  = Integer.parseInt(coordinates.substring(separatorIndex + 1));
			int[] selectedRoad = new int[]{xCoord, yCoord, roadPosition};
			ArrayList<int[]> validRoads = game.getGalaxy().getSwappableRoads(selectedRoad);
			if (validRoads.size() > 0){
				try {
					JSONArray roadListJS = new JSONArray();
					for (int[] coordinate:validRoads){
						JSONObject roadJS = new JSONObject()
								.put("coordinates", Integer.toString(coordinate[0])+"."+Integer.toString(coordinate[1]))
								.put("roadPosition", coordinate[2]);
						roadListJS.put(roadJS);
					}
					JSONObject activateSwapRoads = new JSONObject()
							.put("action", "activateSwapRoads")
							.put("roadList", roadListJS);
					GlobalMethods.sendPlayerAction(player, activateSwapRoads);
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	/**active les routes que l'on peut sélectionner**/
	public void activateSwapRoads(String player, StarcraftGame game){
		if (game.getPlayerCurrentlyPlaying().equals(player)){
			ArrayList<int[]> validRoads = game.getGalaxy().getSwappableRoads();
			if (validRoads.size() > 0){
				try {
					JSONArray roadListJS = new JSONArray();
					for (int[] coordinate:validRoads){
						JSONObject roadJS = new JSONObject()
								.put("coordinates", Integer.toString(coordinate[0])+"."+Integer.toString(coordinate[1]))
								.put("roadPosition", coordinate[2]);
						roadListJS.put(roadJS);
					}
					JSONObject activateSwapRoads = new JSONObject()
							.put("action", "activateSwapRoads")
							.put("roadList", roadListJS);
					GlobalMethods.sendPlayerAction(player, activateSwapRoads);
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public void activateEventCards(String player, StarcraftGame game){
		if (game.getPlayerCurrentlyPlaying().equals(player)){
			StarcraftPlayer starcraftPlayer = game.getPlayerList().get(player);
			if (starcraftPlayer.getEventCards().canChooseEventCard()){
				try {
					JSONObject activateEventCards = new JSONObject()
							.put("action", "activateEventCards");
					GlobalMethods.sendPlayerAction(player, activateEventCards);
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public void activateStartOfBattleCards(String player, StarcraftGame game){
		if (game.getPlayerCurrentlyPlaying().equals(player)){
			StarcraftPlayer starcraftPlayer = game.getPlayerList().get(player);
			ArrayList<CombatCard> combatCards= starcraftPlayer.getSpecialActions().getTriggeredCombatCards("placeFrontlineUnits");
			ArrayList<EventCard> eventCards= starcraftPlayer.getSpecialActions().getTriggeredEventCards("placeFrontlineUnits");
			if (combatCards.size() + eventCards.size() > 0){
				try {
					JSONObject activateStartOfBattleCards = new JSONObject()
							.put("action", "activateStartOfBattleCards");
					GlobalMethods.sendPlayerAction(player, activateStartOfBattleCards);
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void displayEventCards(String player, StarcraftGame game){
		if (game.getPlayerCurrentlyPlaying().equals(player)){
			StarcraftPlayer starcraftPlayer = game.getPlayerList().get(player);
			if (starcraftPlayer.getEventCards().canChooseEventCard()){
				try {
					JSONArray cardListJS = new JSONArray();
					for (EventCard card:starcraftPlayer.getEventCards().getEventCardList()){
						JSONObject cardJS = new JSONObject()
								.put("id", card.getId())
								.put("color", starcraftPlayer.getPlayerColor())
								.put("name", card.getName())
								.put("text", card.getText());
						cardListJS.put(cardJS);
					}
					JSONObject displayBuyableCards = new JSONObject()
							.put("action", "displayBuyableCards")
							.put("cardList", cardListJS);
					GlobalMethods.sendPlayerAction(player, displayBuyableCards);
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void askEventCardUse(String playerName, int cardId, StarcraftGame game) {
		StarcraftPlayer starcraftPlayer = game.getPlayerList().get(playerName);
		starcraftPlayer.getEventCards().useEventCard(cardId, game);
		if (starcraftPlayer.getEventCards().canChooseEventCard()){
			GlobalMethods.clearActionStage(playerName);
			displayEventCards(playerName, game);
			activateEventCards(playerName, game);
		}else{
			starcraftPlayer.getEventCards().applyBonuses(game);
			game.nextTurn();
			// on met à jour l'affichage reflétant les changements qui on eu lieu lors du tour précédent
			for (String player : game.getPlayerList().keySet()){
				GameTurnHandler.printNextTurnScreen(player, game);
			}
		}
	}

	public void endRoadSwapTurn(String playerName, String coordinates1, int roadPosition1, String coordinates2,
			int roadPosition2, StarcraftGame game) {
		int separatorIndex1 = coordinates1.indexOf('.');
		int xCoord1  = Integer.parseInt(coordinates1.substring(0, separatorIndex1));
		int yCoord1  = Integer.parseInt(coordinates1.substring(separatorIndex1 + 1));
		int separatorIndex2 = coordinates2.indexOf('.');
		int xCoord2  = Integer.parseInt(coordinates2.substring(0, separatorIndex2));
		int yCoord2  = Integer.parseInt(coordinates2.substring(separatorIndex2 + 1));
		int[] road1 = new int[]{xCoord1, yCoord1, roadPosition1};
		int[] road2 = new int[]{xCoord2, yCoord2, roadPosition2};
		game.getGalaxy().swapRoads(road1, road2, game);
		game.nextTurn();
		for (String player : game.getPlayerList().keySet()){
			GameTurnHandler.printNextTurnScreen(player, game);
		}
	}

	public void addBuildBuidingTurn(String playerName, StarcraftGame game) {
		game.getPlayer(game.getPlayerCurrentlyPlaying()).addTempBonus("costReduction");
		game.addSpecialTurn(playerName, GameConstants.buildBuildingsTurnName, 1);
		game.addSpecialTurn(playerName, "endExecutionTurn", 2);
		game.nextTurn();
		// on met à jour l'affichage reflétant les changements qui on eu lieu lors du tour précédent
		for (String player : game.getPlayerList().keySet()){
			GameTurnHandler.printNextTurnScreen(player, game);
		}
	}

	public void addBuildModuleTurn(String playerName, StarcraftGame game) {
		game.getPlayer(game.getPlayerCurrentlyPlaying()).addTempBonus("costReduction");
		game.addSpecialTurn(playerName, GameConstants.buildModuleTurnName, 1);
		game.addSpecialTurn(playerName, "endExecutionTurn", 2);
		game.nextTurn();
		// on met à jour l'affichage reflétant les changements qui on eu lieu lors du tour précédent
		for (String player : game.getPlayerList().keySet()){
			GameTurnHandler.printNextTurnScreen(player, game);
		}
	}

	public void askStartOfBattleCardUse(String playerName, int cardId, StarcraftGame game) {
		StarcraftPlayer starcraftPlayer = game.getPlayerList().get(playerName);
		if (game.getGalaxy().getStarcraftBattle() != null){
			starcraftPlayer.getSpecialActions().useStartOfBattleCard(cardId, game.getGalaxy().getStarcraftBattle());
		}
		game.nextTurn();
		// on met à jour l'affichage reflétant les changements qui on eu lieu lors du tour précédent
		for (String player : game.getPlayerList().keySet()){
			GameTurnHandler.printNextTurnScreen(player, game);
		}
	}
	
	public void displayActiveEventCards(String playerName, StarcraftGame game) {
		StarcraftPlayer activeplayer= game.getPlayer(playerName);
		for (EventCard eventCard:activeplayer.getEventCards().getUsableEventCards()){
			JSONObject eventCardJS = eventCard.getCardJS("displayCardInHand");
			try {
				eventCardJS.put("color", activeplayer.getPlayerColor());
			} catch (JSONException e) {
				e.printStackTrace();
			}
			GlobalMethods.sendPlayerAction(playerName, eventCardJS);
		}
		if (activeplayer.getEventCards().getStartegyCard() != null){
			JSONObject eventCardJS = activeplayer.getEventCards().getStartegyCard().getCardJS("displayCardInHand");
			try {
				eventCardJS.put("color", activeplayer.getPlayerColor());
			} catch (JSONException e) {
				e.printStackTrace();
			}
			GlobalMethods.sendPlayerAction(playerName, eventCardJS);
		}
	}

	public void displayActiveEventCards2(String playerName, String cardOwner, StarcraftGame game) {
		StarcraftPlayer activeplayer= game.getPlayer(cardOwner);
		for (EventCard eventCard:activeplayer.getEventCards().getUsableEventCards()){
			JSONObject eventCardJS = eventCard.getCardJS("displayCardInHand");
			try {
				eventCardJS.put("color", activeplayer.getPlayerColor());
			} catch (JSONException e) {
				e.printStackTrace();
			}
			GlobalMethods.sendPlayerAction(playerName, eventCardJS);
		}
		if (activeplayer.getEventCards().getStartegyCard() != null){
			JSONObject eventCardJS = activeplayer.getEventCards().getStartegyCard().getCardJS("displayCardInHand");
			try {
				eventCardJS.put("color", activeplayer.getPlayerColor());
			} catch (JSONException e) {
				e.printStackTrace();
			}
			GlobalMethods.sendPlayerAction(playerName, eventCardJS);
		}
	}
	
	public void sendFreeUnits(String playerName, int x, int y, int z, StarcraftGame game){
		StarcraftPlayer activeplayer= game.getPlayer(playerName);
		activeplayer.getEventCards().sendFreeUnits(x, y, z);
		game.nextTurn();
		// on met à jour l'affichage reflétant les changements qui on eu lieu lors du tour précédent
		for (String player : game.getPlayerList().keySet()){
			GameTurnHandler.printNextTurnScreen(player, game);
		}
	}
}
