package gameEntities.methods;

import gameEntities.StarcraftGame;
import gameEntities.StarcraftPlayer;
import gameEntities.playerItems.StarcraftModule;
import gameEntities.playerItems.StarcraftUnit;

import java.util.List;

import javax.json.JsonObject;

import gameEntities.GameConstants;

// classe intermédiare entre le websocket et les méthodes modifiant l'état de la partie
public class GameTurnHandler {
    private static ChooseFaction factionChoice =  new ChooseFaction();
    private static ChooseLeadership leadershipChoice =  new ChooseLeadership();
    private static PlacePlanets placePlanets =  new PlacePlanets();
    private static PlaceUnits placeUnits =  new PlaceUnits();
    private static PlaceZRoad placeZRoad =  new PlaceZRoad();
    private static PlanningPhase planningPhase = new PlanningPhase();
    private static GalaxyOrderChoice galaxyOrderChoice = new GalaxyOrderChoice();
    private static BattleHandler battleHandler = new BattleHandler();
    private static ResourceHandler resourceHandler = new ResourceHandler();
    private static BuildTurnHandler buildUnits = new BuildTurnHandler();
    private static RegroupingPhase regroupingPhase = new RegroupingPhase();
    private static HandleResearchCards handleResearchCards = new HandleResearchCards();
    private static EventCardHandler eventCardHandler = new EventCardHandler();
	
	// fonction gérant l'affichage du prochain tour, cette fonction se contente d'envoyer les modifications apportées au plateau
	public static void printNextTurnScreen(String player, StarcraftGame game){
		GlobalMethods.sendHelpMessage(player, game);
		GlobalMethods.printPlayerTurn(player, game);
		String turnName = game.getTurnPart();
		//System.out.println(turnName);
		int currentTurnNumber = game.getCurrentTurnNumber();
		//cette fonction est appelée à chaque fois car le tour des joueurs change fréquement
		factionChoice.printChosenFactions(player, game);
		if (turnName.equals("factionChoice")){
			//correspond au passage d'une étape à une autre
			if (currentTurnNumber == 0){
				factionChoice.printFactionChoice(player, game);
			}
		}else if (turnName.equals("leadershipChoice")){
			if (currentTurnNumber == 0){
				leadershipChoice.printLeadershipChoice(player, game);
			}
		}else if (turnName.equals("choosePlanetToDraw")){
			GlobalMethods.clearByClass(player, "leadershipChoice");
			placePlanets.addPlanetDrawCanvas(player, game);
			placePlanets.displayAllPlanets(player, game);
		}else if (turnName.equals("chooseFirstPlayer")){
			factionChoice.chooseFirstPlayer(player, game);
		}else if (turnName.equals("placePlanet")){
			if (currentTurnNumber == 0){
				GlobalMethods.printGameBoard(player);
				GlobalMethods.upgradeActionMenu(player);
				//le joueur pioche des planètes aléatoirement
				//TODO piocher les planètes à un autre moment
				placePlanets.drawPlanets(player, game);
				placePlanets.addActionCanvas(player, game);
				placePlanets.addGalaxyCanvas(player, game);
			}
			GlobalMethods.clearByClass(player, "gameButton");
			GlobalMethods.clearActionStage(player);
			placePlanets.addPlacePlanetButtons(player, game);
			placePlanets.printPlanetChoice(player, game);
			placePlanets.updateGalaxySize(player, game);
			placeUnits.printAllGalaxyUnits(player, game);
			placePlanets.setValidPlacements(player, game);
		}else if (turnName.equals(GameConstants.placeBaseTurnName)){
			GlobalMethods.clearActionStage(player);
			GlobalMethods.clearByClass(player, "gameButton");
			placePlanets.resizeGalaxy(player, game);
			placeUnits.printAllGalaxyUnits(player, game);
			placeUnits.addUnitPlacementButton(player, game);
			placeUnits.printUnitChoice(player, game);
			placeUnits.activateCurrentPlanet(player, game);
			placeUnits.activateValidUnits(player, game);
		}else if (turnName.equals(GameConstants.placeZRoadTurnName)){
			if (currentTurnNumber < 1){
				placePlanets.resizeGalaxy(player, game);
				placeUnits.printAllGalaxyUnits(player, game);
				GlobalMethods.clearActionStage(player);
				GlobalMethods.clearByClass(player, "gameButton");
				placeZRoad.addPlaceZRoadButtons(player, game);
			}
			placeZRoad.activateAvailableRoads(player, game);
		}else {
			// on peut à partir de ce moment appeler les fonctions d'affichage des
			//éléments sur la galaxie car sa taille ne change plus
			//les unités sont déplacées pendant la partie car il y en a beaucoup,
			//on ne veut pas devoir les réafficher à chaque fois
			planningPhase.clearDisplayedOrders(player, game);
			planningPhase.displayPlacedOrders(player, game);
			if (turnName.equals("placeUnit")){
				if (currentTurnNumber == 0){
					GlobalMethods.clearActionStage(player);
					GlobalMethods.clearByClass(player, "gameButton");
					placeUnits.addUnitPlacementButton(player, game);
					placeUnits.printUnitChoice(player, game);
				}
				placeUnits.activateValidUnits(player, game);
				placeUnits.activateValidPlanets(player, game);
				placeUnits.activateValidLinks(player, game);
			}else if (turnName.startsWith(GameConstants.planningPhaseTurnName)){
				if (currentTurnNumber == 0){
					GlobalMethods.clearByClass(player, "gameButton");
					planningPhase.addPlanningPhaseButton(player, game);
				}
				GlobalMethods.clearActionStage(player);
				planningPhase.displayAvailableOrders(player, game);
				planningPhase.activateValidPlanetSquares(player, game);
			}else if (turnName.equals(GameConstants.galaxyOrderChoiceTurnName)){
				// on doit mettre à jour les actions possibles à chaque tour à cause des tours sautés
				// on ne sait pas à quel moment chaque écran est utilisé pour la première fois
				battleHandler.setCombatModeOff(player, game);
				GlobalMethods.clearActionStage(player);
				GlobalMethods.clearByClass(player, "gameButton");
				galaxyOrderChoice.addGalaxyOrderChoiceButton(player, game);
				galaxyOrderChoice.activateValidSquareOrders(player, game);
			}else if (turnName.equals(GameConstants.executeChoiceTurnName)){
				GlobalMethods.clearActionStage(player);
				GlobalMethods.clearByClass(player, "gameButton");
				galaxyOrderChoice.addActivationChoiceButton(player, game);
			} else if (turnName.equals(GameConstants.moveUnitTurnName) || turnName.equals("warpgateTurn")){
				GlobalMethods.clearActionStage(player);
				GlobalMethods.clearByClass(player, "gameButton");
				placeUnits.addUnitPlacementButton(player, game);
				placeUnits.printUnitChoice(player, game);
				placeUnits.activateValidUnits(player, game);
				placeUnits.activateValidPlanets(player, game);
			} else if (turnName.equals("conclaveFleetTurn")){
				GlobalMethods.clearActionStage(player);
				GlobalMethods.clearByClass(player, "gameButton");
				placeUnits.addConclaveFleetButton(player, game);
				placeUnits.printUnitChoice(player, game);
				placeUnits.activateValidUnits(player, game);
				placeUnits.activateValidPlanets(player, game);
			} else if (turnName.equals("placeFrontlineUnits")){
					GlobalMethods.clearActionStage(player);
					GlobalMethods.clearByClass(player, "gameButton");
					battleHandler.setCombatModeOn(player, game);
					battleHandler.setBattleField(player, game);
					battleHandler.displayUnplacedUnits(player, game);
					battleHandler.activateValidBattleUnits(player, game);
					battleHandler.addEndFrontLineTurnButton(player, game);
					battleHandler.addAllCheckerEvents(player, game);
					battleHandler.addBattleBonusButton(player, game);
			} else if (turnName.equals("placeSupportUnits")){
				GlobalMethods.clearByClass(player, "gameButton");
				battleHandler.addEndSupportLineTurnButton(player, game);
				battleHandler.activateValidBattleUnits(player, game);
				battleHandler.addAllCheckerEvents(player, game);
				battleHandler.addBattleBonusButton(player, game);
			} else if (turnName.equals("placeCombatCards")){
				GlobalMethods.clearByClass(player, "gameButton");
				GlobalMethods.clearActionStage(player);
				battleHandler.displayCombatCardChoice(player, game);
				battleHandler.addAllCheckerEvents(player, game);
				battleHandler.addEndBattleCardTurnButton(player, game);
				battleHandler.addBattleBonusButton(player, game);
			} else if (turnName.equals("revealBattleCards")){
				GlobalMethods.clearByClass(player, "gameButton");
				GlobalMethods.clearActionStage(player);
				battleHandler.addEndSupportLineTurnButton(player, game);
				battleHandler.revealBattleCard(player, game);
				battleHandler.activateEndTurn(player, game);
				battleHandler.addBattleBonusButton(player, game);
			} else if (turnName.equals("destroyUnits")){
				GlobalMethods.clearByClass(player, "gameButton");
				GlobalMethods.clearActionStage(player);
				battleHandler.revealBattleCard(player, game);
				battleHandler.activateDeletableUnits(player, game);
				battleHandler.addBattleBonusButton(player, game);
			} else if (turnName.equals("galaxyUnitDestruction")){
				GlobalMethods.clearByClass(player, "gameButton");
				GlobalMethods.clearActionStage(player);
				battleHandler.activateDeletableGalaxyUnits(player, game);;
			} else if (turnName.equals(GameConstants.moveRetreatUnitTurnName)){
				GlobalMethods.clearByClass(player, "gameButton");
				GlobalMethods.clearActionStage(player);
				battleHandler.setCombatModeOff(player, game);
				placeUnits.activateValidUnits(player, game);
				placeUnits.activateValidPlanets(player, game);
				battleHandler.addEndRetreatButton(player, game);
				placeUnits.checkEndTurn(player, game);
			} else if (turnName.equals(GameConstants.buildUnitsTurnName)){
				GlobalMethods.clearByClass(player, "gameButton");
				buildUnits.displayUnlockedUnits(player, game);
				buildUnits.activateUnlockedUnits(player, game);
				buildUnits.addBuildingUnitTurnButton(player, game);
				buildUnits.checkEndTurn(player, game);
				resourceHandler.addCostReductionButton(player, game);
			} else if (turnName.equals(GameConstants.buildBuildingsTurnName)){
				GlobalMethods.clearByClass(player, "gameButton");
				GlobalMethods.clearActionStage(player);
				buildUnits.displayUnlockedBuildings(player, game);
				buildUnits.addBuildingUnitTurnButton(player, game);
				buildUnits.activateUnlockedBuildings(player, game);
				buildUnits.checkEndTurn(player, game);
				resourceHandler.addCostReductionButton(player, game);
			} else if (turnName.equals(GameConstants.buildModuleTurnName)){
				GlobalMethods.clearByClass(player, "gameButton");
				GlobalMethods.clearActionStage(player);
				buildUnits.displayUnlockedModules(player, game);
				buildUnits.addBuildingUnitTurnButton(player, game);
				buildUnits.activateUnlockedModules(player, game);
				buildUnits.checkEndTurn(player, game);
				resourceHandler.addCostReductionButton(player, game);
			} else if (turnName.equals(GameConstants.buildBaseTurnName)){
				GlobalMethods.clearByClass(player, "gameButton");
				GlobalMethods.clearActionStage(player);
				buildUnits.displayUnlockedUnits(player, game);
				buildUnits.activateUnlockedUnits(player, game);
				buildUnits.addBuildingUnitTurnButton(player, game);
				buildUnits.checkEndTurn(player, game);
				resourceHandler.addCostReductionButton(player, game);
			} else if (turnName.equals("discardHand")){
				GlobalMethods.clearByClass(player, "gameButton");
				GlobalMethods.clearActionStage(player);
				regroupingPhase.displayCardChoice(player, game);
				regroupingPhase.addDiscardHandTurnButton(player, game);
			} else if (turnName.equals(GameConstants.buyResearchTurnName) || turnName.equals("buyBonusResearch")){
				GlobalMethods.clearByClass(player, "gameButton");
				GlobalMethods.clearActionStage(player);
				handleResearchCards.displayBuyableCards(player, game);
				buildUnits.addBuildingUnitTurnButton(player, game);
				handleResearchCards.activateBuyableCards(player, game);
				buildUnits.checkEndTurn(player, game);
				resourceHandler.addCostReductionButton(player, game);
				handleResearchCards.displayResearchBonus(player, game);
			} else if (turnName.equals("rechargeTurn")){
				GlobalMethods.clearByClass(player, "gameButton");
				GlobalMethods.clearActionStage(player);
				battleHandler.revealBattleCard(player, game);
				handleResearchCards.displayCardsToRecharge(player, game);
				battleHandler.addEndSupportLineTurnButton(player, game);
			} else if (turnName.equals(GameConstants.eventCardTurnName)){
				GlobalMethods.clearByClass(player, "gameButton");
				GlobalMethods.clearActionStage(player);
				eventCardHandler.displayEventCards(player, game);
				eventCardHandler.activateEventCards(player, game);
				eventCardHandler.addSkipTurnButton(player, game);
			} else if (turnName.equals("swapRoad")){
				GlobalMethods.clearByClass(player, "gameButton");
				GlobalMethods.clearActionStage(player);
				eventCardHandler.activateSwapRoads(player, game);
				eventCardHandler.addSwapZRoadButtons(player, game);
				eventCardHandler.addSkipTurnButton(player, game);
			} else if (turnName.equals("reducedCostBuilding")){
				GlobalMethods.clearByClass(player, "gameButton");
				GlobalMethods.clearActionStage(player);
				eventCardHandler.addChooseBuildingButton(player, game);
			} else if (turnName.equals("winnerScreen")){
				GlobalMethods.clearByClass(player, "gameButton");
				GlobalMethods.clearActionStage(player);
				regroupingPhase.showWinner(player, game);
			} else if (turnName.equals("starOrderTurn")){
				GlobalMethods.clearByClass(player, "gameButton");
				GlobalMethods.clearActionStage(player);
				galaxyOrderChoice.addStarOrderButtons(player, game);
			} else if (turnName.equals("getResourcesToken")){
				GlobalMethods.clearByClass(player, "gameButton");
				GlobalMethods.clearActionStage(player);
				resourceHandler.displayResourcesTokenChoice(player, game);
			} else if (turnName.equals("restoreResourceArea")){
				GlobalMethods.clearByClass(player, "gameButton");
				GlobalMethods.clearActionStage(player);
				resourceHandler.addRestoreAreasEvent(player, game);
				resourceHandler.addEndRestorationTurnButton(player, game);
			} else if (turnName.equals("exhaustResourceArea")){
				GlobalMethods.clearByClass(player, "gameButton");
				GlobalMethods.clearActionStage(player);
				resourceHandler.addExhaustAreasEvent(player, game);
				resourceHandler.addEndRestorationTurnButton(player, game);
			} else if (turnName.equals("useStartOfBattleCards")){
				GlobalMethods.clearByClass(player, "gameButton");
				GlobalMethods.clearActionStage(player);
				GlobalMethods.addEndNormalTurnButton(player, game);
				eventCardHandler.displayUsableCards(player, game, "placeFrontlineUnits");
				eventCardHandler.activateStartOfBattleCards(player, game);
				battleHandler.addBattleBonusButton(player, game);
			} else if (turnName.equals("swapBattleUnitsTurn")){
				GlobalMethods.clearByClass(player, "gameButton");
				GlobalMethods.clearActionStage(player);
				battleHandler.addSwapUnitsTurnButton(player, game);
				battleHandler.addBattleBonusButton(player, game);
				battleHandler.activateSwappableUnits(player, game);
			} else if (turnName.equals("cardActivation")){
				GlobalMethods.clearByClass(player, "gameButton");
				GlobalMethods.clearActionStage(player);
				buildUnits.activateResourcePlaces(player, game);
				buildUnits.addResourcePlacesEvent(player, game);
				battleHandler.addCardActivationButton(player, game);
			} else if (turnName.equals("freeUnits")){
				GlobalMethods.clearByClass(player, "gameButton");
				GlobalMethods.clearActionStage(player);
				eventCardHandler.askFreeUnitsPlacements(player, game);
			}
		}
	}
	
	//TODO
	// renvoit l'affichage correspondant à l'état du jeu pour un joueur
	// Cette fonction doit pouvoir tout réafficher à partir d'un page vierge
	// Cela permet d'afficher correctement la partie si une personne rafraichit sa page
	// ou se reconnecte, cette fonction peut aussi servir à charger une partie sauvegardée
	public static void printCurrentTurnScreen(String player, StarcraftGame game){
		if (game != null && player != null){
			String turnName = game.getTurnPart();
			factionChoice.setupPlayerInfoOptions(player, game);
			GlobalMethods.sendHelpMessage(player, game);
			//System.out.println(turnName);
			//fonctions affichant les informations nécessaires aux joueurs quelque soit l'étape courante du jeu.
			GlobalMethods.printPlayerTurn(player, game);
			factionChoice.printChosenFactions(player, game);
			printPlayerBaordInfo(player, game);
			if (turnName.equals("factionChoice")){
				factionChoice.printFactionChoice(player, game);
			}else{
				if (turnName.equals("leadershipChoice")){
					leadershipChoice.printLeadershipChoice(player, game);
				}else if (turnName.equals("choosePlanetToDraw")){
					placePlanets.addPlanetDrawCanvas(player, game);
					placePlanets.displayAllPlanets(player, game);
				}else if (turnName.equals("chooseFirstPlayer")){
					factionChoice.chooseFirstPlayer(player, game);
				}else {
					GlobalMethods.printGameBoard(player);
					GlobalMethods.upgradeActionMenu(player);
					placePlanets.addGalaxyCanvas(player, game);
					placePlanets.addActionCanvas(player, game);
					if (turnName.equals("placePlanet")){
						placePlanets.addPlacePlanetButtons(player, game);
						placePlanets.updateGalaxySize(player, game);
						placePlanets.printAllGalaxyPlanets(player, game);
						placeUnits.printAllGalaxyUnits(player, game);
						placePlanets.setValidPlacements(player, game);
						placePlanets.printPlanetChoice(player, game);
					}else{
						placePlanets.resizeGalaxy(player, game);
						placePlanets.printAllGalaxyPlanets(player, game);
						placeUnits.printAllGalaxyUnits(player, game);
						resourceHandler.displayAllWorkersInGalaxy(player, game);
						placeZRoad.displayAllLink(player, game);
						planningPhase.displayPlacedOrders(player, game);
						if (turnName.equals(GameConstants.placeBaseTurnName)){
							placeUnits.addUnitPlacementButton(player, game);
							placeUnits.printUnitChoice(player, game);
							placeUnits.activateCurrentPlanet(player, game);
							placeUnits.activateValidUnits(player, game);
						}else if (turnName.equals(GameConstants.placeZRoadTurnName)){
							placeZRoad.addPlaceZRoadButtons(player, game);
							placeZRoad.activateAvailableRoads(player, game);
						}else if (turnName.equals("placeUnit")){
							placeUnits.addUnitPlacementButton(player, game);
							placeUnits.printUnitChoice(player, game);
							placeUnits.activateValidUnits(player, game);
							placeUnits.activateValidPlanets(player, game);
							placeUnits.activateValidLinks(player, game);
						}else if (turnName.startsWith(GameConstants.planningPhaseTurnName)){
							planningPhase.displayAvailableOrders(player, game);
							planningPhase.addPlanningPhaseButton(player, game);
							planningPhase.activateValidPlanetSquares(player, game);
						}else if (turnName.startsWith(GameConstants.galaxyOrderChoiceTurnName)){
							galaxyOrderChoice.addGalaxyOrderChoiceButton(player, game);
							galaxyOrderChoice.activateValidSquareOrders(player, game);
						}else if (turnName.equals(GameConstants.executeChoiceTurnName)){
							galaxyOrderChoice.addActivationChoiceButton(player, game);
						} else if (turnName.equals(GameConstants.moveUnitTurnName) || turnName.equals("warpgateTurn")){
							placeUnits.addUnitPlacementButton(player, game);
							placeUnits.printUnitChoice(player, game);
							placeUnits.activateValidUnits(player, game);
							placeUnits.activateValidPlanets(player, game);
						} else if (turnName.equals("conclaveFleetTurn")){
							placeUnits.addConclaveFleetButton(player, game);
							placeUnits.printUnitChoice(player, game);
							placeUnits.activateValidUnits(player, game);
							placeUnits.activateValidPlanets(player, game);
						} else if (turnName.equals("placeFrontlineUnits")){
							battleHandler.setCombatModeOn(player, game);
							battleHandler.setBattleField(player, game);
							battleHandler.displayUnplacedUnits(player, game);
							battleHandler.displayAllBattleUnits(player, game);
							battleHandler.activateValidBattleUnits(player, game);
							battleHandler.addEndFrontLineTurnButton(player, game);
							battleHandler.sendEndBattleTurn(player, game);
							battleHandler.addAllCheckerEvents(player, game);
							battleHandler.addBattleBonusButton(player, game);
						} else if (turnName.equals("placeSupportUnits")){
							battleHandler.addEndSupportLineTurnButton(player, game);
							battleHandler.setCombatModeOn(player, game);
							battleHandler.setBattleField(player, game);
							battleHandler.displayUnplacedUnits(player, game);
							battleHandler.displayAllBattleUnits(player, game);
							battleHandler.activateValidBattleUnits(player, game);
							battleHandler.addAllCheckerEvents(player, game);
							battleHandler.sendEndBattleTurn(player, game);
							battleHandler.addBattleBonusButton(player, game);
						} else if (turnName.equals("placeCombatCards")){
							battleHandler.setCombatModeOn(player, game);
							battleHandler.displayCombatCardChoice(player, game);
							battleHandler.setCardBattleField(player, game);
							battleHandler.displayAllBattleUnits(player, game);
							battleHandler.addAllCheckerEvents(player, game);
							battleHandler.displayAllHiddenBattleCard(player, game);
							battleHandler.addEndBattleCardTurnButton(player, game);
							battleHandler.addBattleBonusButton(player, game);
						} else if (turnName.equals("revealBattleCards")){
							battleHandler.setCombatModeOn(player, game);
							battleHandler.setCardBattleField(player, game);
							battleHandler.displayAllBattleUnits(player, game);
							battleHandler.revealBattleCard(player, game);
							battleHandler.addEndSupportLineTurnButton(player, game);
							battleHandler.activateEndTurn(player, game);
							battleHandler.addBattleBonusButton(player, game);
						} else if (turnName.equals("destroyUnits")){
							battleHandler.setCardBattleField(player, game);
							battleHandler.displayAllBattleUnits(player, game);
							battleHandler.revealBattleCard(player, game);
							battleHandler.activateDeletableUnits(player, game);
							battleHandler.addBattleBonusButton(player, game);
						} else if (turnName.equals("galaxyUnitDestruction")){
							battleHandler.activateDeletableGalaxyUnits(player, game);;
						} else if (turnName.equals(GameConstants.moveRetreatUnitTurnName)){
							placeUnits.activateValidUnits(player, game);
							placeUnits.activateValidPlanets(player, game);
							battleHandler.addEndRetreatButton(player, game);
							placeUnits.checkEndTurn(player, game);
						} else if (turnName.equals(GameConstants.buildUnitsTurnName)){
							buildUnits.displayUnlockedUnits(player, game);
							buildUnits.activateUnlockedUnits(player, game);
							buildUnits.activateResourcePlaces(player, game);
							buildUnits.addResourcePlacesEvent(player, game);
							placeBuiltUnit(player, game);
							buildUnits.addBuildingUnitTurnButton(player, game);
							buildUnits.checkEndTurn(player, game);
							resourceHandler.addCostReductionButton(player, game);
						} else if (turnName.equals(GameConstants.buildBuildingsTurnName)){
							buildUnits.displayUnlockedBuildings(player, game);
							buildUnits.activateUnlockedBuildings(player, game);
							buildUnits.addBuildingUnitTurnButton(player, game);
							buildUnits.checkEndTurn(player, game);
							buildUnits.activateResourcePlaces(player, game);
							buildUnits.addResourcePlacesEvent(player, game);
							resourceHandler.addCostReductionButton(player, game);
						} else if (turnName.equals(GameConstants.buildModuleTurnName)){
							buildUnits.displayUnlockedModules(player, game);
							buildUnits.addBuildingUnitTurnButton(player, game);
							buildUnits.activateResourcePlaces(player, game);
							buildUnits.addResourcePlacesEvent(player, game);
							buildUnits.activateUnlockedModules(player, game);
							buildUnits.checkEndTurn(player, game);
							resourceHandler.addCostReductionButton(player, game);
						} else if (turnName.equals(GameConstants.buildBaseTurnName)){
							buildUnits.displayUnlockedUnits(player, game);
							buildUnits.activateUnlockedUnits(player, game);
							buildUnits.activateResourcePlaces(player, game);
							buildUnits.addResourcePlacesEvent(player, game);
							placeBuiltUnit(player, game);
							buildUnits.addBuildingUnitTurnButton(player, game);
							buildUnits.checkEndTurn(player, game);
							resourceHandler.addCostReductionButton(player, game);
						} else if (turnName.equals("discardHand")){
							regroupingPhase.displayCardChoice(player, game);
							regroupingPhase.addDiscardHandTurnButton(player, game);
							regroupingPhase.sendEndTurn(player, game);
						} else if (turnName.equals(GameConstants.buyResearchTurnName) || turnName.equals("buyBonusResearch")){
							handleResearchCards.displayBuyableCards(player, game);
							buildUnits.addBuildingUnitTurnButton(player, game);
							handleResearchCards.activateBuyableCards(player, game);
							buildUnits.checkEndTurn(player, game);
							buildUnits.activateResourcePlaces(player, game);
							buildUnits.addResourcePlacesEvent(player, game);
							resourceHandler.addCostReductionButton(player, game);
							handleResearchCards.displayResearchBonus(player, game);
						} else if (turnName.equals("rechargeTurn")){
							battleHandler.setCombatModeOn(player, game);
							battleHandler.setCardBattleField(player, game);
							battleHandler.displayAllBattleUnits(player, game);
							battleHandler.revealBattleCard(player, game);
							handleResearchCards.displayCardsToRecharge(player, game);
							battleHandler.addEndSupportLineTurnButton(player, game);
						} else if (turnName.equals(GameConstants.eventCardTurnName)){
							eventCardHandler.displayEventCards(player, game);
							eventCardHandler.activateEventCards(player, game);
							eventCardHandler.addSkipTurnButton(player, game);
						} else if (turnName.equals("swapRoad")){
							eventCardHandler.activateSwapRoads(player, game);
							eventCardHandler.addSwapZRoadButtons(player, game);
						} else if (turnName.equals("reducedCostBuilding")){
							eventCardHandler.addChooseBuildingButton(player, game);
						} else if (turnName.equals("winnerScreen")){
							regroupingPhase.showWinner(player, game);
						} else if (turnName.equals("starOrderTurn")){
							galaxyOrderChoice.addStarOrderButtons(player, game);
						} else if (turnName.equals("getResourcesToken")){
							resourceHandler.displayResourcesTokenChoice(player, game);
						} else if (turnName.equals("restoreResourceArea")){
							resourceHandler.addRestoreAreasEvent(player, game);
							resourceHandler.addEndRestorationTurnButton(player, game);
						} else if (turnName.equals("exhaustResourceArea")){
							resourceHandler.addExhaustAreasEvent(player, game);
							resourceHandler.addEndRestorationTurnButton(player, game);
						} else if (turnName.equals("useStartOfBattleCards")){
							GlobalMethods.addEndNormalTurnButton(player, game);
							eventCardHandler.displayUsableCards(player, game, "placeFrontlineUnits");
							eventCardHandler.activateStartOfBattleCards(player, game);
							battleHandler.addBattleBonusButton(player, game);
						} else if (turnName.equals("swapBattleUnitsTurn")){
							battleHandler.setCombatModeOn(player, game);
							battleHandler.addSwapUnitsTurnButton(player, game);
							battleHandler.setBattleField(player, game);
							battleHandler.displayAllBattleUnits(player, game);
							battleHandler.activateSwappableUnits(player, game);
						} else if (turnName.equals("cardActivation")){
							buildUnits.activateResourcePlaces(player, game);
							buildUnits.addResourcePlacesEvent(player, game);
							battleHandler.addCardActivationButton(player, game);
						} else if (turnName.equals("freeUnits")){
							eventCardHandler.askFreeUnitsPlacements(player, game);
						}
					}
				}
			}
		}
	}
	
	/**rafraichit la vue de la galaxie et du menu joueur**/
	public static void updateDisplay(String player, StarcraftGame game){
		printPlayerBaordInfo(player, game);
		if (game.getGalaxy().getStarcraftBattle() == null){
			battleHandler.setCombatModeOff(player, game);
		}
	}
	
	/**met à jour la position des unités de la galaxie pour tous les joueurs**/
	public static void updateUnitDisplay(long unitId, StarcraftGame game){
		for (String player : game.getPlayerList().keySet()){
			placeUnits.printGalaxyUnit(player, unitId, game);
		}
	}

	public static void callChoosePlayerFaction(String player, String speciesName, String factionName, StarcraftGame game){
    	factionChoice.choosePlayerFaction(player, speciesName, factionName, game);
	}
	
	public static void callChooseLeadershipCard(String player, String leadershipCardName, StarcraftGame game){
		leadershipChoice.chooseLeadershipCard(player, leadershipCardName, game);
	}
	
	public static void callPlacePlanet(String player, String planetName, String coordinates, StarcraftGame game){
		placePlanets.placePlanet(player, planetName, coordinates, game);
	}
	
	public static void callRotatePlanet(String player, String planetName, StarcraftGame game) {
		placePlanets.rotatePlanet(player, planetName, game);
	}
	
	
	// fonction affichant les informations sur un joueur (faction, ressources, batiments construits, etc...)
	// cette fonction doit envoyer des informations différentes à chaque joueur selon leur situation
	public static void printPlayerBaordInfo(String player, StarcraftGame game){
		GlobalMethods.clearByClass(player, "playerInfoBoard");
		factionChoice.printPlayerFactionInfo(player, game);
		leadershipChoice.printPlayerLeadershipInfo(player, game);
		factionChoice.printCombatCardInfo(player, game);
		resourceHandler.displayPlayerResourceInfo(player, game);
		resourceHandler.displayBaseWorkers(game.getPlayer(player));
		displayWorkersOnBaseResources(player, game);
		factionChoice.displayActiveCardNumber(player, game);
		factionChoice.showUnlockedUnitsInfo(player, game);
		factionChoice.showOwnedModules(player, game);
		resourceHandler.displayPlayerResourceToken(player, game);
		factionChoice.reservedUnitsInfo(player, game);
		eventCardHandler.displayDrawnEventCardNumber(player, game);
		eventCardHandler.displayActiveEventCardNumber(player, game);
		
		factionChoice.fetchPlayerFactionInfo(player, game);
		String turnName = game.getTurnPart();
		if (!turnName.equals("factionChoice") && !turnName.equals("leadershipChoice")){
			factionChoice.fetchCombatCardInfo(player, game);
			leadershipChoice.fetchLeadershipInfo(player, game);
			resourceHandler.fetchPlayerResourceInfo(player, game);
			resourceHandler.fetchBaseWorkersInfo(player, game);
			resourceHandler.fetchWorkersOnBaseResources(game.getPlayer(player));
			factionChoice.fetchActiveCardNumber(player, game);
			factionChoice.fetchUnlockedUnits(player, game);
			factionChoice.fetchOwnedModules(player, game);
			resourceHandler.fetchResourceTokensInfo(player, game);
			factionChoice.fetchReservedUnits(player, game);
			eventCardHandler.fetchDrawnEventCardNumber(player, game);
			eventCardHandler.fetchActiveCardNumber(player, game);
		}
	}
	
	public static void updateUnlockedUnits(StarcraftPlayer player, String unitName){
		factionChoice.updateUnlockedUnits(player, unitName);
	}
	
	
	public static void callDisplayBaseWorkers(String player, StarcraftGame game){
		resourceHandler.displayBaseWorkers(game.getPlayer(player));
	}

	public static void callAskValidPlacements(String playerName, String unitId, StarcraftGame game) {
		placeUnits.askValidPlacements(playerName, Long.parseLong(unitId), game);
	}

	public static void callSendUnitPlacement(String playerName, String unitId, String coordinates, String areaId, StarcraftGame game) {
		placeUnits.sendUnitPlacement(playerName, Long.parseLong(unitId), coordinates, Integer.parseInt(areaId), game);
		
	}

	public static void callReturnUnitToPool(String playerName, String unitId, StarcraftGame game) {
		placeUnits.returnUnitToPool(playerName, Long.parseLong(unitId), game);
		
	}

	public static void callEndUnitPlacementTurn(String playerName, StarcraftGame game) {
		placeUnits.endUnitPlacementTurn(playerName, game);
		
	}

	public static void callAskAllRoadPlacements(String playerName, StarcraftGame game) {
		placeZRoad.activateAvailableRoads(playerName, game);
	}

	public static void callSendRoadPlacement(String playerName, String coordinates, String roadPosition, StarcraftGame game) {
		placeZRoad.sendRoadPlacement(playerName, coordinates, Integer.parseInt(roadPosition), game);
	}

	public static void callEndRoadPlacementTurn(String playerName,
			String coordinates1,
			String road1,
			String coordinates2,
			String road2,
			StarcraftGame game) {
		placeZRoad.endRoadPlacementTurn(playerName, coordinates1, Integer.parseInt(road1),
				coordinates2, Integer.parseInt(road2), game);
	}

	public static void callEndRoadPlacementTurn2(String playerName, StarcraftGame game) {
		placeZRoad.endRoadPlacementTurn2(playerName, game);
	}

	public static void callSendPlanningPhaseTurn(String playerName, String coordinates, String orderId, StarcraftGame game) {
		planningPhase.sendPlanningPhaseTurn(playerName, coordinates, Integer.parseInt(orderId), game);
	}

	public static void callAskOrderStack(String playerName, String coordinates, StarcraftGame game) {
		planningPhase.askOrderStack(playerName, coordinates, game);
		
	}

	public static void callEndGalaxyOrderChoiceTurn(String playerName, String coordinates, StarcraftGame game) {
		galaxyOrderChoice.endGalaxyOrderChoiceTurn(playerName, coordinates, game);
	}

	public static void callCancelOrder(String playerName, StarcraftGame game) {
		galaxyOrderChoice.cancelOrder(playerName, game);
	}

	public static void callExecuteOrder(String playerName, StarcraftGame game) {
		galaxyOrderChoice.executeOrder(playerName, game);
	}

	public static void callAskValidBattlePlacements(String playerName, String unitId, StarcraftGame game) {
		battleHandler.askValidBattlePlacements(playerName, Long.parseLong(unitId), game);
		
	}

	public static void callSendBattleUnitPlacement
	(String playerName, String unitId, String battleRow, String battlePlace, StarcraftGame game) {
		battleHandler.sendBattleUnitPlacement(playerName, Long.parseLong(unitId), battleRow, battlePlace, game);
	}

	public static void callReturnUnitToBattlePool(String playerName, String unitId, StarcraftGame game) {
		battleHandler.returnUnitToBattlePool(playerName, Long.parseLong(unitId), game);
	}

	public static void callEndFrontLineTurn(String playerName, StarcraftGame game) {
		battleHandler.endFrontLineTurn(playerName, game);
		
	}

	public static void callAskCombatCardHand(String playerName, StarcraftGame game) {
		factionChoice.displayCombatCards(playerName, game);
	}

	public static void callEndSupportLineTurn(String playerName, StarcraftGame game) {
		battleHandler.endSupportLineTurn(playerName, game);
	}

	public static void callAskValidBattleCardPlacements(String playerName, String cardId, StarcraftGame game) {
		battleHandler.askValidBattleCardPlacements(playerName, Integer.parseInt(cardId.substring(4)), game);
	}

	public static void callSendBattleCardPlacement(String playerName, String cardId, String battleRow, String battlePlace,
			StarcraftGame game) {
		battleHandler.sendBattleCardPlacement(playerName, Integer.parseInt(cardId.substring(4)), battleRow, battlePlace, game);
	}

	public static void callReturnCardToBattlePool(String playerName, String cardId, StarcraftGame game) {
		battleHandler.returnCardToBattlePool(playerName, Integer.parseInt(cardId.substring(4)), game);
	}

	public static void callEndBattleCardTurn(String playerName, StarcraftGame game) {
		battleHandler.endBattleCardTurn(playerName, game);
	}

	/**fonction particulière supprimant l'affichage d'une unité pour tous les joueurs**/
	public static void removeUnitDisplay(long unitId, StarcraftGame game){
		for (String playerName:game.getPlayerList().keySet()){
			battleHandler.removeUnitDisplay(unitId, playerName, game);
		}
	}
	
	/**fonction particulière supprimant l'affichage d'une unité pour tous les joueurs**/
	public static void callClearBattleStage(StarcraftGame game){
		for (String playerName:game.getPlayerList().keySet()){
			battleHandler.clearBattleStage(playerName, game);
		}
	}

	public static void callDestroyBattleUnit(String playerName, String unitId, StarcraftGame game) {
		battleHandler.destroyBattleUnit(playerName, Long.parseLong(unitId), game);
	}

	public static void callEndRetreatTurn(String playerName, StarcraftGame game) {
		battleHandler.endRetreatTurn(playerName, game);
	}
	
	public static void callDisplayPlayerResourceInfo(String player, StarcraftGame game) {
		resourceHandler.displayPlayerResourceInfo(player, game);
	}

	public static void callAskBuyingOrder(String playerName, String unitName, StarcraftGame game) {
		buildUnits.askBuyingUnitOrder(playerName, unitName, game);
	}
	
	public static void callUpdateCardNumber(StarcraftPlayer player) {
		factionChoice.updateCardNumber(player);
	}

	public static void callSetWorkerOnArea(String playerName, String coordinates, String areaId, StarcraftGame game) {
		buildUnits.setWorkerOnArea(playerName, coordinates, Integer.parseInt(areaId), game);
	}
	
	public static void updateAreaWorkerDisplay(List<Integer> coordinate, StarcraftPlayer workerOwner, StarcraftGame game){
		for (String player:game.getPlayerList().keySet()){
			resourceHandler.displayWorkersOnArea(player, coordinate, workerOwner, game);
		}
	}
	
	/**montre les travailleurs disponibles et en réserve**/
	public static void updateBaseWorkerDisplay(StarcraftPlayer player){
		resourceHandler.updateBaseWorkerDisplay(player);
	}
	
	public static void placeBuiltUnit(String player, StarcraftGame game){
		StarcraftPlayer starcraftPlayer = game.getPlayer(player);
		if (starcraftPlayer.getBuyingOrder() != null){
			if (starcraftPlayer.getBuyingOrder().isReady()){
				GlobalMethods.clearActionStage(player);
				GlobalMethods.clearObjectEvents(player);
				placeUnits.printUnitChoice(player, game);
				placeUnits.activateCurrentPlanet(player, game);
				placeUnits.activateValidLinks(player, game);
				placeUnits.activateReservedUnitPlacement(player, game);
				placeUnits.activateValidUnits(player, game);
			}
		}
	}
	
	public static void endBuyingOrder(String player, StarcraftGame game){
		StarcraftPlayer starcraftPlayer = game.getPlayer(player);
		starcraftPlayer.executeBuyingOrder();
		setBuildUnitStage(player, game);
	}
	
	/**on clear car is se peut que l'on passe de l'écran de placement d'unités à celui d'achat**/
	public static void setBuildUnitStage(String player, StarcraftGame game){
		buildUnits.checkEndTurn(player, game);
		GlobalMethods.clearActionStage(player);
		GlobalMethods.clearObjectEvents(player);
		buildUnits.displayUnlockedUnits(player, game);
		buildUnits.activateUnlockedUnits(player, game);
	}
	
	public static void setBuildBuildingStage(String player, StarcraftGame game){
		buildUnits.checkEndTurn(player, game);
		buildUnits.activateUnlockedBuildings(player, game);
	}

	public static void setBuyResearchStage(String player, StarcraftGame game){
		buildUnits.checkEndTurn(player, game);
		handleResearchCards.activateBuyableCards(player, game);
	}

	public static void callSetWorkerBaseMineral(String playerName, StarcraftGame game) {
		buildUnits.setWorkerBaseMineral(playerName, game);
	}

	public static void callSetWorkerBaseGas(String playerName, StarcraftGame game) {
		buildUnits.setWorkerBaseGas(playerName, game);
	}
	
	/**montre l'exploitation des ressources de toutes les bases**/
	private static void displayWorkersOnBaseResources(String playerName, StarcraftGame game){
		StarcraftPlayer starcraftPlayer = game.getPlayerList().get(playerName);
		resourceHandler.displayWorkersOnBaseResources(starcraftPlayer);
	}
	
	/**met à jour l'affichage des travailleurs sur la base**/
	public static void callDisplayWorkersOnBaseResources(StarcraftPlayer player){
		resourceHandler.displayWorkersOnBaseResources(player);
		resourceHandler.sendUpdateWorkersOnBaseResources(player);
	}

	public static void callEndBuildingUnitTurn(String playerName, StarcraftGame game) {
		buildUnits.endBuildingUnitTurn(playerName, game);
	}

	public static void callCancelBuyOrder(String playerName, StarcraftGame game) {
		buildUnits.cancelBuyOrder(playerName, game);
	}

	public static void callAskBuyingBuildingOrder(String playerName, String buildingName, StarcraftGame game) {
		int separatorIndex = buildingName.indexOf('.');
		int number  = Integer.parseInt(buildingName.substring(0, separatorIndex));
		int level  = Integer.parseInt(buildingName.substring(separatorIndex + 1));
		buildUnits.askBuyingBuildingOrder(playerName, number, level, game);
	}

	public static void callDiscardCard(String playerName, String cardId, StarcraftGame game) {
		regroupingPhase.discardCard(playerName, Integer.parseInt(cardId.substring(4)), game);
	}

	public static void callEndDiscardHandTurn(String playerName, StarcraftGame game) {
		regroupingPhase.endDiscardHandTurn(playerName, game);
	}

	public static void callAskBuyingCardOrder(String playerName, String cardName, StarcraftGame game){
		buildUnits.askBuyingCardOrder(playerName, cardName, game);
	}
	
	
	/**hack pioche toutes les cartes**/
	public static void drawAllCards(String playerName, StarcraftGame game) {
		StarcraftPlayer starcraftPlayer = game.getPlayerList().get(playerName);
		starcraftPlayer.drawCombatCards(starcraftPlayer.getCombatCardDeck().size());
	}

	public static void callSacrificeUnit(String playerName, String unitId, StarcraftGame game) {
		buildUnits.sacrificeUnit(playerName, Long.parseLong(unitId), game);
	}

	public static void callRechargeCard(String playerName, String cardId, StarcraftGame game) {
		handleResearchCards.rechargeCard(playerName, Integer.parseInt(cardId.substring(4)), game);
	}

	public static void callAskActiveCard(String playerName, StarcraftGame game) {
		factionChoice.displayActiveCards(playerName, game);
	}

	public static void callAskEventCardUse(String playerName, String cardId, StarcraftGame game) {
		eventCardHandler.askEventCardUse(playerName, Integer.parseInt(cardId.substring(4)), game);
	}

	public static void drawAllEventCards(String playerName, StarcraftGame game) {
		eventCardHandler.displayEventCardInDeck(playerName, game);
		eventCardHandler.activeDrawingEventCards(playerName, game);
	}

	public static void sendRoadSwap(String playerName, JsonObject jsonMessage, StarcraftGame game) {
		String coordinates = jsonMessage.getString("coordinates");
		String roadPosition = jsonMessage.getString("roadPosition");
		eventCardHandler.activateSwapRoads(playerName, coordinates, Integer.parseInt(roadPosition), game);
	}

	public static void askAllRoadSwaps(String playerName, JsonObject jsonMessage, StarcraftGame game) {
		eventCardHandler.activateSwapRoads(playerName, game);
	}

	public static void endRoadSwapTurn(String playerName, JsonObject jsonMessage, StarcraftGame game) {
		String coordinates1 = jsonMessage.getString("coordinates1");
		String road1 = jsonMessage.getString("roadPosition1");
		String coordinates2 = jsonMessage.getString("coordinates2");
		String road2 = jsonMessage.getString("roadPosition2");
		eventCardHandler.endRoadSwapTurn(playerName, coordinates1, Integer.parseInt(road1),
				coordinates2, Integer.parseInt(road2), game);
	}
	
	public static void updateRoadDisplay(StarcraftGame game, int[] road, String color) {
		for (String player:game.getPlayerList().keySet()){
			eventCardHandler.askRedrawRoad(player, game, road, color);
		}
	}

	public static void addBuildBuidingTurn(String playerName, JsonObject jsonMessage, StarcraftGame game) {
		eventCardHandler.addBuildBuidingTurn(playerName, game);
	}

	public static void askBuyingModuleOrder(String playerName, JsonObject jsonMessage, StarcraftGame game) {
		String moduleName = jsonMessage.getString("name");
		int moduleId  = Integer.parseInt(moduleName.substring(6));
		buildUnits.askBuyingModuleOrder(playerName, moduleId, game);
	}

	public static void setBuyModuleStage(String playerName, StarcraftGame game) {
		buildUnits.checkEndTurn(playerName, game);
		buildUnits.activateUnlockedModules(playerName, game);
	}

	public static void useCostReduction(String playerName, JsonObject jsonMessage, StarcraftGame game) {
		String resource = jsonMessage.getString("resource");
		buildUnits.useCostReduction(playerName, resource, game);
	}
	
	public static void displayCostReduction(String player, StarcraftGame game) {
		resourceHandler.addCostReductionButton(player, game);
	}

	public static void addBuildModuleTurn(String playerName, JsonObject jsonMessage, StarcraftGame game) {
		eventCardHandler.addBuildModuleTurn(playerName, game);
	}
	
	public static void askResearchBonus(String playerName, StarcraftGame game) {
		handleResearchCards.askResearchBonus(playerName, game);
	}

	public static void applyResearchBonus(String playerName, JsonObject jsonMessage, StarcraftGame game) {
		String bonusName = jsonMessage.getString("name");
		handleResearchCards.applyResearchBonus(playerName, bonusName, game);
	}
	
	public static void setResearchCardName(String cardName) {
		handleResearchCards.setCardName(cardName);;
	}
	
	public static void updateActiveCombatCards(String player, StarcraftGame game) {
		factionChoice.displayActiveCardNumber(player, game);
		factionChoice.sendUpdateActiveCardNumber(player, game);
	}

	public static void askActiveCard2(String playerName, JsonObject jsonMessage, StarcraftGame game) {
		String cardOwner = jsonMessage.getString("name");
		factionChoice.displayActiveCards2(playerName, cardOwner, game);
	}
	
	public static void showOwnedModules(String playerName, StarcraftGame game) {
		factionChoice.showOwnedModules(playerName, game);
		factionChoice.sendUpdateOwnedModules(playerName, game);
	}
	
	public static void updateOwnedModules(StarcraftPlayer player, StarcraftModule module) {
		factionChoice.updateOwnedModules(player, module);
	}

	public static void drawFromPlanetDeck(String playerName, JsonObject jsonMessage, StarcraftGame game) {
		String planetName = jsonMessage.getString("planetName");
		placePlanets.drawFromPlanetDeck(playerName, planetName, game);
	}

	
	public static void sendFirstPlayerChoice(String playerName, JsonObject jsonMessage, StarcraftGame game) {
		String firstPlayer = jsonMessage.getString("name");
		factionChoice.sendFirstPlayerChoice(playerName, firstPlayer, game);
	}

	public static void chooseStarOrder(String playerName, JsonObject jsonMessage, StarcraftGame game) {
		Boolean isSpecial = false;
		if (jsonMessage.getString("special").equals("true")){
			isSpecial = true;
		}
		galaxyOrderChoice.chooseStarOrder(playerName, jsonMessage.getString("name"), isSpecial, game);
	}
	
	public static void addTokenResource(String playerName, JsonObject jsonMessage, StarcraftGame game) {
		String resourceType = jsonMessage.getString("resourceType");
		resourceHandler.addTokenResource(playerName, resourceType, game);
	}

	public static void useResourceToken(String playerName, JsonObject jsonMessage, StarcraftGame game) {
		String resourceType = jsonMessage.getString("resourceType");
		buildUnits.useTokenResource(playerName, resourceType, game);
	}
	
	public static void updateResourceTokenDisplay(String player, StarcraftGame game) {
		resourceHandler.displayPlayerResourceToken(player, game);
		resourceHandler.sendUpdateResourceTokensInfo(player, game);
	}
	
	public static void updateExhaustionDisplay(int[] coordinates, int exhaustion, StarcraftGame game) {
		resourceHandler.updateExhaustionDisplay(coordinates, exhaustion, game);
	}
	
	public static void removeReservedUnitDisplay(String player, StarcraftGame game, long unitId){
		factionChoice.removeReservedUnitDisplay(player, game, unitId);
	}
	
	public static void addReservedUnitDisplay(String player, StarcraftGame game, StarcraftUnit unit){
		factionChoice.addReserveUnitDisplay(player, game, unit);
	}
	
	public static void reservedUnitsInfo(String player, StarcraftGame game){
		factionChoice.reservedUnitsInfo(player, game);
	}
	
	/**rafraichit l'affichage des informations sur les ennemis après que tout le monde ait choisi sa carte de leadership**/
	public static void updateLeadershipCardInfo(String player, StarcraftGame game) {
		leadershipChoice.fetchLeadershipInfo(player, game);
		factionChoice.fetchCombatCardInfo(player, game);
		resourceHandler.fetchPlayerResourceInfo(player, game);
		resourceHandler.fetchBaseWorkersInfo(player, game);
		factionChoice.fetchUnlockedUnits(player, game);
		factionChoice.fetchReservedUnits(player, game);
	}
	
	public static void sendUnitToReserve(String playerName, JsonObject jsonMessage, StarcraftGame game) {
		long unitId = Long.parseLong(jsonMessage.getString("unitId"));
		placeUnits.sendUnitToReserve(playerName, unitId, game);
	}
	
	public static void sendRestoreArea(String playerName, JsonObject jsonMessage, StarcraftGame game) {
		String coordinates = jsonMessage.getString("coordinates");
		int separatorIndex = coordinates.indexOf('.');
		int x  = Integer.parseInt(coordinates.substring(0, separatorIndex));
		int y  = Integer.parseInt(coordinates.substring(separatorIndex + 1));
		int areaId = Integer.parseInt(jsonMessage.getString("areaId"));
		resourceHandler.sendRestoreArea(playerName, x, y, areaId, game);
	}
	
	public static void sendExhaustArea(String playerName, JsonObject jsonMessage, StarcraftGame game) {
		String coordinates = jsonMessage.getString("coordinates");
		int separatorIndex = coordinates.indexOf('.');
		int x  = Integer.parseInt(coordinates.substring(0, separatorIndex));
		int y  = Integer.parseInt(coordinates.substring(separatorIndex + 1));
		int areaId = Integer.parseInt(jsonMessage.getString("areaId"));
		resourceHandler.sendExhaustArea(playerName, x, y, areaId, game);
	}
	
	public static void endRestorationTurn(String playerName, JsonObject jsonMessage, StarcraftGame game) {
		resourceHandler.endRestorationTurn(playerName, game);;
	}
	
	public static void endConclaveFleetTurn(String playerName, JsonObject jsonMessage, StarcraftGame game) {
		placeUnits.endConclaveFleetTurn(playerName, game);
	}
	
	public static void askStartOfBattleCardUse(String playerName, JsonObject jsonMessage, StarcraftGame game) {
		int cardId = Integer.parseInt(jsonMessage.getString("cardId").substring(4));
		eventCardHandler.askStartOfBattleCardUse(playerName, cardId, game);
	}
	
	public static void endNormalTurn(String playerName, JsonObject jsonMessage, StarcraftGame game) {
		game.nextTurn();
		// on met à jour l'affichage reflétant les changements qui on eu lieu lors du tour précédent
		for (String player : game.getPlayerList().keySet()){
			GameTurnHandler.printNextTurnScreen(player, game);
		}
	}
	
	public static void endSwapUnitsTurn(String playerName, JsonObject jsonMessage, StarcraftGame game) {
		battleHandler.endSwapUnitsTurn(playerName, game);
	}
	
	public static void askDisplayAttackerBonuses(String playerName, JsonObject jsonMessage, StarcraftGame game) {
		battleHandler.displayAttackerBonuses(playerName, game);
	}


	public static void askDisplayDefenderBonuses(String playerName, JsonObject jsonMessage, StarcraftGame game) {
		battleHandler.displayDefenderBonuses(playerName, game);
	}
	
	public static void updateDrawnEventCardNumber(StarcraftPlayer player){
		eventCardHandler.updateDrawnEventCardNumber(player);
	}
	
	public static void askActiveEventCard(String playerName, JsonObject jsonMessage, StarcraftGame game) {
		eventCardHandler.displayActiveEventCards(playerName, game);
	}
	
	public static void askActiveEventCard2(String playerName, JsonObject jsonMessage, StarcraftGame game) {
		String cardOwner = jsonMessage.getString("name");
		eventCardHandler.displayActiveEventCards2(playerName, cardOwner, game);
	}
	
	public static void updateActiveEventCardNumber(String playerName, StarcraftGame game) {
		eventCardHandler.displayActiveEventCardNumber(playerName, game);
		eventCardHandler.sendUpdateActiveCardNumber(playerName, game);
	}
	
	public static void askDrawEventCard(String playerName, JsonObject jsonMessage, StarcraftGame game) {
		int cardId = Integer.parseInt(jsonMessage.getString("cardId").substring(4));
		StarcraftPlayer player = game.getPlayer(playerName);
		player.getEventCards().drawEventCardFromDeck(cardId);
	}
	
	public static void askValidSwapUnits(String playerName, JsonObject jsonMessage, StarcraftGame game) {
		long unitId = Long.parseLong(jsonMessage.getString("unitId"));
		battleHandler.askValidSwapUnits(playerName, unitId, game);
	}
	
	public static void cancelCardActivation(String playerName, JsonObject jsonMessage, StarcraftGame game) {
		buildUnits.cancelCardActivation(playerName, game);
	}

	public static void addFreeUnits(String playerName, JsonObject jsonMessage, StarcraftGame game) {
		String coordinates = jsonMessage.getString("coordinates");
		int separatorIndex = coordinates.indexOf('.');
		int x  = Integer.parseInt(coordinates.substring(0, separatorIndex));
		int y  = Integer.parseInt(coordinates.substring(separatorIndex + 1));
		int areaId = Integer.parseInt(jsonMessage.getString("areaId"));
		eventCardHandler.sendFreeUnits(playerName, x, y, areaId, game);
	}
	
	public static void surrenderStarcraftGame(String playerName, JsonObject jsonMessage, StarcraftGame game) {
		GlobalMethods.surrenderStarcraftGame(playerName, game);
	}
	
}

