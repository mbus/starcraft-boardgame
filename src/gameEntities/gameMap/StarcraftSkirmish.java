package gameEntities.gameMap;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import gameEntities.StarcraftGame;
import gameEntities.methods.GameTurnHandler;
import gameEntities.playerItems.CombatCard;
import gameEntities.playerItems.CombatCardAbility;
import gameEntities.playerItems.StarcraftUnit;

public class StarcraftSkirmish implements java.io.Serializable {

	private static final long serialVersionUID = 1659853579887155779L;
	private int id;
	private StarcraftUnit defendingUnit;
	private StarcraftUnit attackingUnit;
	private Map<Long, StarcraftUnit> attackingSupports = new HashMap<Long, StarcraftUnit>();
	private Map<Long, StarcraftUnit> defendingSupports = new HashMap<Long, StarcraftUnit>();
	private CombatCard frontAttackCard;
	private CombatCard frontDefenseCard;
	private CombatCard supportAttackCard;
	private CombatCard supportDefenseCard;
	private int attackerPower = 0;
	private int attackerHealth = 0;
	private int defenderPower = 0;
	private int defenderHealth = 0;
	private ArrayList<CombatCardAbility> attackerAbilities = new ArrayList<CombatCardAbility>();
	private ArrayList<CombatCardAbility> defenderAbilities = new ArrayList<CombatCardAbility>();
	private final Set<String> enemyBattleScope = new HashSet<String>(Arrays.asList("splash"));
	private final Set<String> enemyFrontScope = new HashSet<String>(Arrays.asList("attackBonus", "cancelGroundAttack", 
			"cancelEnemyAttack"));
	private final Set<String> enemyAllScope = new HashSet<String>(Arrays.asList("broodling"));
	
	
	/**fonction résolvant l'escarmouche**/
	public void resolveSkirmish(StarcraftBattle battle, StarcraftGame game){
		//ajout des capacirés de l'attaquant
		addAttackerAbility(battle);
		//ajout des capacirés du défenseur
		addDefenderAbility(battle);
		addTempBonuses(battle);
		applyCancelingAbilities(battle);
		//rend invisible les unités appropriées
		applyCardCloaking(battle);
		//applique diverses capacités
		applyOtherCardAbilities(battle);
		//suppression de l'invisibilité
		useDetector(battle);
		
		//objet stockant les capacités utilisées pour éviter des réapplications accidentelles
		ArrayList<CombatCardAbility> usedAbilities = new ArrayList<CombatCardAbility>();
		//calcul de l'attaque et de la défense des deux participants
		if (this.attackingUnit.getAttackType()!= null){
			if (this.frontAttackCard.getUnitNames().contains(this.attackingUnit.getName())){
				this.attackerPower += this.frontAttackCard.getMaxAttack();
				this.attackerHealth += this.frontAttackCard.getMaxDefense();
			}else{
				this.attackerPower += this.frontAttackCard.getMinAttack();
				this.attackerHealth += this.frontAttackCard.getMinDefense();
			}
			for (long attackSupportId:this.attackingSupports.keySet()){
				StarcraftUnit supportUnit = this.attackingSupports.get(attackSupportId);
				if (supportUnit.getAttackSupport() > 0
						&& (supportUnit.getAttackType().equals("all")
								|| supportUnit.getAttackType().equals(this.defendingUnit.getMoveType()))){
					this.attackerPower +=  supportUnit.getAttackSupport();
				}
			}
			if (battle.getAttackingPlayer().getActiveSpecialOrderBonus()){
				this.attackerPower += 1;
			}
			
			//on active les augmentations d'attaques de l'attaquant
			for (CombatCardAbility battleAbility:this.attackerAbilities){
				if (battleAbility.getName().equals("attackBonus")){
					this.attackerPower += battleAbility.getAmount();
					usedAbilities.add(battleAbility);
				}else if (battleAbility.getName().equals("healthBonus")){
					this.attackerHealth += battleAbility.getAmount();
					usedAbilities.add(battleAbility);
				}
			}
			this.attackerAbilities.removeAll(usedAbilities);
			usedAbilities.removeAll(usedAbilities);
		}else{
			this.attackerHealth += this.frontAttackCard.getMinDefense();
			for (CombatCardAbility battleAbility:this.attackerAbilities){
				if (battleAbility.getName().equals("healthBonus")){
					this.attackerHealth += battleAbility.getAmount();
					usedAbilities.add(battleAbility);
				}
			}
			this.attackerAbilities.removeAll(usedAbilities);
			usedAbilities.removeAll(usedAbilities);
		}
		
		
		if (this.defendingUnit.getAttackType()!= null){
			if (this.frontDefenseCard.getUnitNames().contains(this.defendingUnit.getName())){
				this.defenderPower += this.frontDefenseCard.getMaxAttack();
				this.defenderHealth += this.frontDefenseCard.getMaxDefense();
			}else{
				this.defenderPower += this.frontDefenseCard.getMinAttack();
				this.defenderHealth += this.frontDefenseCard.getMinDefense();
			}
			for (long defenderSupportId:this.defendingSupports.keySet()){
				StarcraftUnit supportUnit = this.defendingSupports.get(defenderSupportId);
				if (supportUnit.getAttackSupport() > 0
						&& (supportUnit.getAttackType().equals("all")
								|| supportUnit.getAttackType().equals(this.attackingUnit.getMoveType()))){
					this.defenderPower +=  supportUnit.getAttackSupport();
				}
			}
			//on active les augmentations d'attaques du défenseur
			for (CombatCardAbility battleAbility:this.defenderAbilities){
				if (battleAbility.getName().equals("attackBonus")){
					this.defenderPower += battleAbility.getAmount();
					usedAbilities.add(battleAbility);
				}else if (battleAbility.getName().equals("healthBonus")){
					this.defenderHealth += battleAbility.getAmount();
					usedAbilities.add(battleAbility);
				}
			}
			this.defenderAbilities.removeAll(usedAbilities);
			usedAbilities.removeAll(usedAbilities);
		}else{
			this.defenderHealth += this.frontDefenseCard.getMinDefense();
			for (CombatCardAbility battleAbility:this.defenderAbilities){
				if (battleAbility.getName().equals("healthBonus")){
					this.defenderHealth += battleAbility.getAmount();
					usedAbilities.add(battleAbility);
				}
			}
			this.defenderAbilities.removeAll(usedAbilities);
			usedAbilities.removeAll(usedAbilities);
		}
		
		//modification potentielle du type d'attaque
		String attackerAttackType = this.attackingUnit.getAttackType();
		String defenderAttackType = this.defendingUnit.getAttackType();
		for (CombatCardAbility cancelAbility:this.attackerAbilities){
			if (cancelAbility.getName().equals("cancelGroundAttack")){
				attackerAttackType = this.modifiyAttackType(cancelAbility, this.attackingUnit);
				defenderAttackType = this.modifiyAttackType(cancelAbility, this.defendingUnit);
			}else if (cancelAbility.getName().equals("cancelEnemyAttack")){
				defenderAttackType = this.modifiyAttackType(cancelAbility, this.defendingUnit);
			}else if (cancelAbility.getName().equals("cancelBothAttacks")){
				attackerAttackType = this.modifiyAttackType(cancelAbility, this.attackingUnit);
				defenderAttackType = this.modifiyAttackType(cancelAbility, this.defendingUnit);
			}else if (cancelAbility.getName().equals("cancelAirAttack")){
				defenderAttackType = this.modifiyAttackType(cancelAbility, this.defendingUnit);
			}
		}
		for (CombatCardAbility cancelAbility:this.defenderAbilities){
			if (cancelAbility.getName().equals("cancelGroundAttack")){
				attackerAttackType = this.modifiyAttackType(cancelAbility, this.attackingUnit);
				defenderAttackType = this.modifiyAttackType(cancelAbility, this.defendingUnit);
			}else if (cancelAbility.getName().equals("cancelEnemyAttack")){
				attackerAttackType = this.modifiyAttackType(cancelAbility, this.attackingUnit);
			}else if (cancelAbility.getName().equals("cancelBothAttacks")){
				attackerAttackType = this.modifiyAttackType(cancelAbility, this.attackingUnit);
				defenderAttackType = this.modifiyAttackType(cancelAbility, this.defendingUnit);
			}else if (cancelAbility.getName().equals("cancelAirAttack")){
				attackerAttackType = this.modifiyAttackType(cancelAbility, this.attackingUnit);
			}
		}
		
		//on ajoute les unités en défense que l'on doit détruire
		HashSet<Long> defenderSet = new HashSet<Long>();
		Boolean destroyDefender = false;
		if (this.attackerPower >= this.defenderHealth){
			if (attackerAttackType.equals("all")
					|| attackerAttackType.equals(this.defendingUnit.getMoveType())){
				destroyDefender = true;
				defenderSet.add(this.defendingUnit.getId());
			}else if (this.defendingSupports.size() > 0){
				//si l'attaquant ne peut détruire l'unité devant, on vérifie si il peut attaquer une unité de support
				for (long defenderID:this.defendingSupports.keySet()){
					if (attackerAttackType.equals(this.defendingSupports.get(defenderID).getMoveType())){
						destroyDefender = true;
						defenderSet.add(defenderID);
					}
				}
			}
			if (defenderSet.size() > 0){
				battle.addDefendersToDestroy(defenderSet);
			}
		}
		
		//si une unité est détruite de manière directe, on active les capacités dépendantes
		if (destroyDefender){
			//si l'unité a sacrifice, on la détruit
			if (this.attackingUnit.getAbilities().contains("sacrifice")){
				battle.addAttackersToDestroy(new HashSet<Long>(Arrays.asList(this.attackingUnit.getId())));
			}
			for (CombatCardAbility ability:this.attackerAbilities){
				if (ability.getName().equals("splash") || ability.getName().equals("spiderSplash")){
					this.applyAttackerSplash(ability, battle);
				}
			}
			//on vérifie le bonus de défense du zergling
			if(this.attackingUnit.getName().equals("zergling")){
				if (battle.getAttackingPlayer().getBonusList().contains("metabolic boost")){
					this.attackerHealth +=2;
				}
			}
		}
		
		//on ajoute les unités en attaque que l'on doit détruire
		HashSet<Long> attackerSet = new HashSet<Long>();
		Boolean destroyAttacker = false;
		if (this.defenderPower >= this.attackerHealth){
			if (defenderAttackType.equals("all")
					|| defenderAttackType.equals(this.attackingUnit.getMoveType())){
				destroyAttacker = true;
				attackerSet.add(this.attackingUnit.getId());
			}else if (this.attackingSupports.size() > 0){
				//si le défenseur ne peut détruire l'unité devant, on vérifie si il peut attaquer une unité de support
				for (long attackerId:this.attackingSupports.keySet()){
					if (defenderAttackType.equals(this.attackingSupports.get(attackerId).getMoveType())){
						destroyAttacker = true;
						attackerSet.add(attackerId);
					}
				}
			}
			if (attackerSet.size() > 0){
				battle.addAttackersToDestroy(attackerSet);
			}
		}
		
		if (destroyAttacker){
			//si l'unité a sacrifice, on la détruit
			if (this.defendingUnit.getAbilities().contains("sacrifice")){
				battle.addDefendersToDestroy(new HashSet<Long>(Arrays.asList(this.defendingUnit.getId())));
			}
			for (CombatCardAbility ability:this.defenderAbilities){
				if (ability.getName().equals("splash") || ability.getName().equals("spiderSplash")){
					this.applyDefenderSplash(ability, battle);
				}
			}
		}
		//application de plague
		for (CombatCardAbility ability:this.attackerAbilities){
			if (ability.getName().equals("plague")){
				this.applyAttackerSplash(ability, battle);
			}
		}
		for (CombatCardAbility ability:this.defenderAbilities){
			if (ability.getName().equals("plague")){
				this.applyDefenderSplash(ability, battle);
			}
		}
		/*System.out.println("skirmish" + Integer.toString(this.id));
		System.out.println("attacker : power = " + Integer.toString(this.attackerPower)
		+ " ; " + "health = " + Integer.toString(this.attackerHealth));
		System.out.println("defender : power = " + Integer.toString(this.defenderPower)
		+ " ; " + "health = " + Integer.toString(this.defenderHealth));*/
	}
	//fin de résolution de l'escarmouche
	
	private void applyCancelingAbilities(StarcraftBattle battle){
		Boolean cancelDefenseSupport = false;
		for (CombatCardAbility attackerAbility:this.attackerAbilities){
			if (!cancelDefenseSupport && attackerAbility.getName().equals("cancelSupportCard")){
				if (this.supportDefenseCard !=  null){
					this.cancelCardAbilities(this.supportDefenseCard, false);
					cancelDefenseSupport = true;
				}
			}
		}
		
		Boolean cancelAttackSupport = false;
		for (CombatCardAbility defenderAbility:this.defenderAbilities){
			if (!cancelAttackSupport && defenderAbility.getName().equals("cancelSupportCard")){
				if (this.supportAttackCard !=  null){
					this.cancelCardAbilities(this.supportAttackCard, true);
					cancelAttackSupport = true;
				}
			}
		}
		
		for (CombatCardAbility attackerAbility:this.attackerAbilities){
			if (attackerAbility.getName().equals("hallucination")){
				if (this.frontDefenseCard !=  null){
					this.cancelCardAbilities(this.frontDefenseCard, false);
					this.frontDefenseCard = null;
					battle.setDelayBattle(true);
				}
				break;
			}
		}
		
		for (CombatCardAbility defenderAbility:this.defenderAbilities){
			if (defenderAbility.getName().equals("hallucination")){
				if (this.frontAttackCard !=  null){
					this.cancelCardAbilities(this.frontAttackCard, true);
					this.frontAttackCard = null;
					battle.setDelayBattle(true);
				}
				break;
			}
		}
	}

	private void applyOtherCardAbilities(StarcraftBattle battle){
		//useWorker
		ArrayList<CombatCardAbility> usedAbilities = new ArrayList<CombatCardAbility>();
		//utilisation des capacités de l'attaquant
		for (CombatCardAbility attackerAbility:this.attackerAbilities){
			if (attackerAbility.getName().equals("useWorker")){
				if (battle.getAttackingPlayer().getAvailableWorkers() > 0){
					battle.getAttackingPlayer().addAvailableWorker(-1);
					battle.getAttackingPlayer().addUnavailableWorker(1);
					GameTurnHandler.updateBaseWorkerDisplay(battle.getAttackingPlayer());
				}
				usedAbilities.add(attackerAbility);
			}else if (attackerAbility.getName().equals("broodling")){
				HashSet<Long> defendersToDestroy = new HashSet<Long>();
				if (this.abilityIsAppliedAgainst(attackerAbility, this.defendingUnit)){
					defendersToDestroy.add(this.defendingUnit.getId());
				}
				for (long unitId:this.defendingSupports.keySet()){
					if (this.abilityIsAppliedAgainst(attackerAbility, this.defendingSupports.get(unitId))){
						defendersToDestroy.add(unitId);
					}
				}
				battle.addDefendersToDestroy(defendersToDestroy);
				usedAbilities.add(attackerAbility);
			}else if (attackerAbility.getName().equals("ensnare")){
				for (long defenderSupportId:this.defendingSupports.keySet()){
					this.defenderPower -=  this.defendingSupports.get(defenderSupportId).getAttackSupport();
				}
			}
		}
		this.attackerAbilities.removeAll(usedAbilities);
		usedAbilities.removeAll(usedAbilities);
		
		//utilisation des capacités de l'attaquant
		for (CombatCardAbility defenderAbility:this.defenderAbilities){
			if (defenderAbility.getName().equals("useWorker")){
				if (battle.getDefendingPlayer().getAvailableWorkers() > 0){
					battle.getDefendingPlayer().addAvailableWorker(-1);
					battle.getDefendingPlayer().addUnavailableWorker(1);
					GameTurnHandler.updateBaseWorkerDisplay(battle.getDefendingPlayer());
				}
				usedAbilities.add(defenderAbility);
			}else if (defenderAbility.getName().equals("broodling")){
				HashSet<Long> attackersToDestroy = new HashSet<Long>();
				if (this.abilityIsAppliedAgainst(defenderAbility, this.attackingUnit)){
					attackersToDestroy.add(this.attackingUnit.getId());
				}
				for (long unitId:this.attackingSupports.keySet()){
					if (this.abilityIsAppliedAgainst(defenderAbility, this.attackingSupports.get(unitId))){
						attackersToDestroy.add(unitId);
					}
				}
				battle.addAttackersToDestroy(attackersToDestroy);
				usedAbilities.add(defenderAbility);
			}else if (defenderAbility.getName().equals("ensnare")){
				for (long attackSupportId:this.attackingSupports.keySet()){
					this.attackerPower -=  this.attackingSupports.get(attackSupportId).getAttackSupport();
				}
			}
		}
		this.defenderAbilities.removeAll(usedAbilities);
	}
	//fin application des capacités diverses
	
	/**ajoute les bonus avant le choix des cartes, cette fonction est appelée avant le placement des cartes car
	 * certaines capacités prennent effet à ce moment (comme parasite)**/
	public void addGeneralBonuses(StarcraftBattle battle){
		//ajout des bonus de l'attaquant
		Map<Long, StarcraftUnit> attackingUnits = getAllPlayerUnits(true);
		for (long attackerId:attackingUnits.keySet()){
			StarcraftUnit attackingUnit = attackingUnits.get(attackerId);
			if (attackingUnit.getAbilities().contains("detector")){
				CombatCardAbility detectorAbility = new CombatCardAbility();
				detectorAbility.setName("detector");
				this.attackerAbilities.add(detectorAbility);
			}
			if (attackingUnit.getName().equals("queen")){
				if (battle.getAttackingPlayer().getBonusList().contains("parasite")){
					CombatCardAbility queenAbility = new CombatCardAbility();
					queenAbility.setName("detector");
					this.attackerAbilities.add(queenAbility);
					CombatCardAbility parasiteAbility = new CombatCardAbility();
					parasiteAbility.setName("parasite");
					this.attackerAbilities.add(parasiteAbility);
				}
			}else if (attackingUnit.getName().equals("science vessel")){
				if (battle.getAttackingPlayer().getBonusList().contains("defense matrix")){
					CombatCardAbility healthBonus = new CombatCardAbility();
					healthBonus.setName("healthBonus");
					healthBonus.setAmount(1);
					this.attackerAbilities.add(healthBonus);
				}
			}else if (attackingUnit.getName().equals("arbiter")){
				if (battle.getAttackingPlayer().getBonusList().contains("cloaking field")){
					CombatCardAbility cloakingField = new CombatCardAbility();
					cloakingField.setName("cloakingField");
					this.attackerAbilities.add(cloakingField);
				}
			}
		}
		if (battle.getAttackingPlayer().getTempBonusList().contains("healthBonus2")){
			this.attackerHealth += 2;
		}
		
		//ajout des bonus du défenseur
		Map<Long, StarcraftUnit> defendingUnits = getAllPlayerUnits(false);
		for (long defenderId:defendingUnits.keySet()){
			StarcraftUnit defendingUnit = defendingUnits.get(defenderId);
			if (defendingUnit.getAbilities().contains("detector")){
				CombatCardAbility detectorAbility = new CombatCardAbility();
				detectorAbility.setName("detector");
				this.defenderAbilities.add(detectorAbility);
			}
			if (defendingUnit.getName().equals("queen")){
				if (battle.getDefendingPlayer().getBonusList().contains("parasite")){
					CombatCardAbility queenAbility = new CombatCardAbility();
					queenAbility.setName("detector");
					this.defenderAbilities.add(queenAbility);
					CombatCardAbility parasiteAbility = new CombatCardAbility();
					parasiteAbility.setName("parasite");
					this.defenderAbilities.add(parasiteAbility);
				}
			}else if (defendingUnit.getName().equals("science vessel")){
				if (battle.getDefendingPlayer().getBonusList().contains("defense matrix")){
					CombatCardAbility healthBonus = new CombatCardAbility();
					healthBonus.setName("healthBonus");
					healthBonus.setAmount(1);
					this.defenderAbilities.add(healthBonus);
				}
			}else if (defendingUnit.getName().equals("arbiter")){
				if (battle.getDefendingPlayer().getBonusList().contains("cloaking field")){
					CombatCardAbility cloakingField = new CombatCardAbility();
					cloakingField.setName("cloakingField");
					this.defenderAbilities.add(cloakingField);
				}
			}
		}
		if (battle.getDefendingPlayer().getTempBonusList().contains("healthBonus2")){
			this.defenderHealth += 2;
		}
	}
	
	/**camoufle les unités si des cartes ont la capacité correspondante**/
	private void applyCardCloaking(StarcraftBattle battle){
		//objet stockant les capacités utilisées pour éviter des réapplications accidentelles
		ArrayList<CombatCardAbility> usedAbilities = new ArrayList<CombatCardAbility>();
		for (CombatCardAbility attackerAbility:this.attackerAbilities){
			if (attackerAbility.getName().equals("cloaking")){
				usedAbilities.add(attackerAbility);
				Map<Long, StarcraftUnit> attackingUnits = getAllPlayerUnits(true);
				for (long attackingSupportId:attackingUnits.keySet()){
					if (attackerAbility.getAlliedUnitNames().contains(attackingUnits.get(attackingSupportId).getName())){
						if (!battle.getCloackedUnits().contains(attackingSupportId)){
							battle.getCloackedUnits().add(attackingSupportId);
						}
					}
				}
			}else if (attackerAbility.getName().equals("cloakingField")){
				usedAbilities.add(attackerAbility);
				Map<Long, StarcraftUnit> attackingUnits = getAllPlayerUnits(true);
				for (long attackingSupportId:attackingUnits.keySet()){
					if (!attackingUnits.get(attackingSupportId).getName().equals("arbiter")){
						if (!battle.getCloackedUnits().contains(attackingSupportId)){
							battle.getCloackedUnits().add(attackingSupportId);
						}
					}
				}
			}
		}
		this.attackerAbilities.removeAll(usedAbilities);
		usedAbilities.removeAll(usedAbilities);
		
		for (CombatCardAbility defenderAbility:this.defenderAbilities){
			if (defenderAbility.getName().equals("cloaking")){
				usedAbilities.add(defenderAbility);
				Map<Long, StarcraftUnit> defendingUnits = getAllPlayerUnits(false);
				for (long defendingSupports:defendingUnits.keySet()){
					if (defenderAbility.getAlliedUnitNames().contains(defendingUnits.get(defendingSupports).getName())){
						if (!battle.getCloackedUnits().contains(defendingSupports)){
							battle.getCloackedUnits().add(defendingSupports);
						}
					}
				}
			}else if (defenderAbility.getName().equals("cloakingField")){
				usedAbilities.add(defenderAbility);
				Map<Long, StarcraftUnit> defendingUnits = getAllPlayerUnits(false);
				for (long defendingSupports:defendingUnits.keySet()){
					if (!defendingUnits.get(defendingSupports).getName().equals("arbiter")){
						if (!battle.getCloackedUnits().contains(defendingSupports)){
							battle.getCloackedUnits().add(defendingSupports);
						}
					}
				}
			}
		}
		this.defenderAbilities.removeAll(usedAbilities);
		usedAbilities.removeAll(usedAbilities);
	}
	
	/**si l'un des joueurs à la détection, toutes les unités de l'adversaire dans cette escarmouche perdent leur
	 * invisibilité**/
	private void useDetector(StarcraftBattle battle){
		Boolean attackerHasDetector = this.attackerHasAbility("detector");
		if (attackerHasDetector){
			if (battle.getCloackedUnits().contains(this.defendingUnit.getId())){
				battle.getCloackedUnits().remove(this.defendingUnit.getId());
			}
			for (long defenderSupportId:this.defendingSupports.keySet()){
				if (battle.getCloackedUnits().contains(defenderSupportId)){
					battle.getCloackedUnits().remove(defenderSupportId);
				}
			}
			//retire la capacité de spiderSplash ennemie
			ArrayList<CombatCardAbility> abilitiesToRemove = new ArrayList<CombatCardAbility>();
			for (CombatCardAbility ability:this.defenderAbilities){
				if (ability.getName().equals("spiderSplash")){
					abilitiesToRemove.add(ability);
				}
			}
			this.defenderAbilities.removeAll(abilitiesToRemove);
			abilitiesToRemove.removeAll(abilitiesToRemove);
		}
		
		Boolean defenderHasDetector =  this.defenderHasAbility("detector");
		if (defenderHasDetector){
			if (battle.getCloackedUnits().contains(this.attackingUnit.getId())){
				battle.getCloackedUnits().remove(this.attackingUnit.getId());
			}
			for (long defenderSupportId:this.attackingSupports.keySet()){
				if (battle.getCloackedUnits().contains(defenderSupportId)){
					battle.getCloackedUnits().remove(defenderSupportId);
				}
			}
			//retire la capacité de spiderSplash ennemie
			ArrayList<CombatCardAbility> abilitiesToRemove = new ArrayList<CombatCardAbility>();
			for (CombatCardAbility ability:this.attackerAbilities){
				if (ability.getName().equals("spiderSplash")){
					abilitiesToRemove.add(ability);
				}
			}
			this.attackerAbilities.removeAll(abilitiesToRemove);
			abilitiesToRemove.removeAll(abilitiesToRemove);
		}
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public StarcraftUnit getDefendingUnit() {
		return defendingUnit;
	}
	
	public void setDefendingUnit(StarcraftUnit defendingUnit) {
		this.defendingUnit = defendingUnit;
	}
	
	public StarcraftUnit getAttackingUnit() {
		return attackingUnit;
	}
	
	public void setAttackingUnit(StarcraftUnit attackingUnit) {
		this.attackingUnit = attackingUnit;
	}
	
	public Map<Long, StarcraftUnit> getAttackingSupports() {
		return attackingSupports;
	}
	
	public void addAttackingSupport(StarcraftUnit unit) {
		this.attackingSupports.put(unit.getId(), unit);
	}
	
	public void removeAttackingSupport(Long unitId) {
		this.attackingSupports.remove(unitId);
	}
	
	public Map<Long, StarcraftUnit> getDefendingSupports() {
		return defendingSupports;
	}
	
	public void addDefendingSupport(StarcraftUnit unit) {
		this.defendingSupports.put(unit.getId(), unit);
	}
	
	public void removeDefendingSupport(Long unitId) {
		this.defendingSupports.remove(unitId);
	}

	public CombatCard getFrontAttackCard() {
		return frontAttackCard;
	}

	public void setFrontAttackCard(CombatCard frontAttackCard) {
		this.frontAttackCard = frontAttackCard;
	}

	public CombatCard getFrontDefenseCard() {
		return frontDefenseCard;
	}

	public void setFrontDefenseCard(CombatCard frontDefenseCard) {
		this.frontDefenseCard = frontDefenseCard;
	}

	public CombatCard getSupportDefenseCard() {
		return supportDefenseCard;
	}

	public void setSupportDefenseCard(CombatCard supportDefenseCard) {
		this.supportDefenseCard = supportDefenseCard;
	}

	public CombatCard getSupportAttackCard() {
		return supportAttackCard;
	}

	public void setSupportAttackCard(CombatCard supportAttackCard) {
		this.supportAttackCard = supportAttackCard;
	}

	public int getAttackerPower() {
		return attackerPower;
	}

	public int getAttackerHealth() {
		return attackerHealth;
	}

	public int getDefenderHealth() {
		return defenderHealth;
	}

	public int getDefenderPower() {
		return defenderPower;
	}
	
	public Boolean attackerHasAbility(String abilityName){
		Boolean result = false;
		for (CombatCardAbility ability:this.attackerAbilities){
			if (abilityName.equals(ability.getName())){
				result = true;
				break;
			}
		}
		return result;
	}
	
	public Boolean defenderHasAbility(String abilityName){
		Boolean result = false;
		for (CombatCardAbility ability:this.defenderAbilities){
			if (abilityName.equals(ability.getName())){
				result = true;
				break;
			}
		}
		return result;
	}
	
	/*fonctions utilitaires*/
	/**vérifie si une capacité de l'attaquant peut s'appliquer contre l'attaquant**/
	private Boolean abilityAgainstDefenderScope(CombatCardAbility ability, StarcraftBattle battle){
		Boolean result = false;
		if (this.enemyBattleScope.contains(ability.getName())){
			if (ability.getHostileUnitNames().size() == 0){
				result = true;
			}else{
				for (long unitId:battle.getDefendingUnits()){
					StarcraftUnit unit = battle.getBattleUnit(unitId);
					if (this.abilityIsAppliedAgainst(ability, unit)){
						result = true;
						break;
					}
				}
			}
		}else if (this.enemyFrontScope.contains(ability.getName())){
			if (ability.getHostileUnitNames().size() == 0){
				result = true;
			}else{
				StarcraftUnit unit = this.defendingUnit;
				result = this.abilityIsAppliedAgainst(ability, unit);
			}
		}else if (this.enemyAllScope.contains(ability.getName())){
			if (ability.getHostileUnitNames().size() == 0){
				result = true;
			}else{
				StarcraftUnit unit = this.defendingUnit;
				if (this.abilityIsAppliedAgainst(ability, unit)){
					result = true;
				}else{
					for (long unitId:this.defendingSupports.keySet()){
						StarcraftUnit defender = this.defendingSupports.get(unitId);
						if (this.abilityIsAppliedAgainst(ability, defender)){
							result = true;
							break;
						}
					}
				}
			}
		}else{
			//si il n'y a pas de conditions pour cette capacité, celle-ci est valide
			result = true;
		}
		return result;
	}
	
	/**vérifie si une capacité du défenseur peut s'appliquer contre l'attaquant**/
	private Boolean abilityAgainstAttackerScope(CombatCardAbility ability, StarcraftBattle battle){
		Boolean result = false;
		if (this.enemyBattleScope.contains(ability.getName())){
			if (ability.getHostileUnitNames().size() == 0){
				result = true;
			}else{
				for (long unitId:battle.getAttackingUnits()){
					StarcraftUnit unit = battle.getBattleUnit(unitId);
					if (this.abilityIsAppliedAgainst(ability, unit)){
						result = true;
						break;
					}
				}
			}
		}else if (this.enemyFrontScope.contains(ability.getName())){
			if (ability.getHostileUnitNames().size() == 0){
				result = true;
			}else{
				StarcraftUnit unit = this.attackingUnit;
				result = this.abilityIsAppliedAgainst(ability, unit);
			}
		}else if (this.enemyAllScope.contains(ability.getName())){
			if (ability.getHostileUnitNames().size() == 0){
				result = true;
			}else{
				StarcraftUnit unit = this.attackingUnit;
				if (this.abilityIsAppliedAgainst(ability, unit)){
					result = true;
				}else{
					for (long unitId:this.attackingSupports.keySet()){
						StarcraftUnit attacker = this.attackingSupports.get(unitId);
						if (this.abilityIsAppliedAgainst(ability, attacker)){
							result = true;
							break;
						}
					}
				}
			}
		}else{
			result = true;
		}
		return result;
	}
	
	/**rajoute les capacités de combat d'une carte**/
	private void addFrontCardAbility(CombatCard card, StarcraftUnit frontUnit, Map<Long,
			StarcraftUnit> supportUnits, ArrayList<CombatCardAbility> abilities, StarcraftBattle battle){
		Boolean isAttacker = true;
		if (frontUnit.getId() == this.defendingUnit.getId()){
			isAttacker = false;
		}
		if (card.getScope().equals("front")){
			//on regarde si cette capacité fonctionne avec l'unité de front actuelle
			for (String abilityName : card.getAbilitiesList().keySet()){
				CombatCardAbility ability = card.getAbilitiesList().get(abilityName);
				if (ability.getAlliedUnitNames().size() == 0){
					if (card.getUnitNames().contains(frontUnit.getName())
							|| card.getUnitNames().contains("all")){
						if (isAttacker){
							if (abilityAgainstDefenderScope(ability, battle)){
								abilities.add(ability);
							}
						}else if (abilityAgainstAttackerScope(ability, battle)){
							abilities.add(ability);
						}
					}
				}else if (ability.getAlliedUnitNames().contains(frontUnit.getName())
						|| ability.getAlliedUnitNames().contains("all")){
					if (isAttacker){
						if (abilityAgainstDefenderScope(ability, battle)){
							abilities.add(ability);
						}
					}else if (abilityAgainstAttackerScope(ability, battle)){
						abilities.add(ability);
					}
				}
			}
		}else if (card.getScope().equals("all")){
			//on regarde si cette capacité fonctionne avec au moins une unité de l'escarmouche
			for (String abilityName : card.getAbilitiesList().keySet()){
				CombatCardAbility ability = card.getAbilitiesList().get(abilityName);
				if (ability.getAlliedUnitNames().size() == 0){
					Boolean validAlliedUnit = false;
					if (card.getUnitNames().contains("all")
							|| card.getUnitNames().contains(frontUnit.getName())){
						validAlliedUnit = true;
					}else{
						
						for (long unitId:supportUnits.keySet()){
							if (card.getUnitNames().contains(supportUnits.get(unitId).getName())){
								validAlliedUnit = true;
								break;
							}
						}
					}
					if (validAlliedUnit){
						if (isAttacker){
							if (abilityAgainstDefenderScope(ability, battle)){
								abilities.add(ability);
							}
						}else if (abilityAgainstAttackerScope(ability, battle)){
							abilities.add(ability);
						}
					}
				}else{
					Boolean validAlliedUnit = false;
					if (ability.getAlliedUnitNames().contains("all")
							|| ability.getAlliedUnitNames().contains(frontUnit.getName())){
						validAlliedUnit = true;
					}else{
						for (long unitId:supportUnits.keySet()){
							if (ability.getAlliedUnitNames().contains(supportUnits.get(unitId).getName())){
								validAlliedUnit = true;
								break;
							}
						}
					}
					if (validAlliedUnit){
						if (isAttacker){
							if (abilityAgainstDefenderScope(ability, battle)){
								abilities.add(ability);
							}
						}else if (abilityAgainstAttackerScope(ability, battle)){
							abilities.add(ability);
						}
					}
				}
			}
		}
	}
	
	/**rajoute les capacités des cartes de combat pour l'attaquant**/
	private void addAttackerAbility(StarcraftBattle battle){
		this.addFrontCardAbility
		(this.frontAttackCard, this.attackingUnit, this.attackingSupports, this.attackerAbilities, battle);

		if (this.supportAttackCard != null){
			CombatCard supportCard = this.supportAttackCard;
			for (String abilityName : supportCard.getAbilitiesList().keySet()){
				//dans les cartes de support, le scope allié est déjà vérifié, on ne regarde que les unités ennemies
				CombatCardAbility ability = supportCard.getAbilitiesList().get(abilityName);
				if (abilityAgainstDefenderScope(ability, battle)){
					this.attackerAbilities.add(ability);
				}
			}
		}
	}
	
	/**rajoute les capacités des cartes de combat pour le défenseur**/
	private void addDefenderAbility(StarcraftBattle battle){
		this.addFrontCardAbility
		(this.frontDefenseCard, this.defendingUnit, this.defendingSupports, this.defenderAbilities, battle);
		
		if (this.supportDefenseCard != null){
			CombatCard supportCard = this.supportDefenseCard;
			for (String abilityName : supportCard.getAbilitiesList().keySet()){
				//dans les cartes de support, le scope allié est déjà vérifié, on ne regarde que les unités ennemies
				CombatCardAbility ability = supportCard.getAbilitiesList().get(abilityName);
				if (abilityAgainstAttackerScope(ability, battle)){
					this.defenderAbilities.add(ability);
				}
			}
		}
	}
	
	/**application des différentes formes de splash**/
	private void applyAttackerSplash(CombatCardAbility ability, StarcraftBattle battle){
		HashSet<Long> unitsToDestroy = new HashSet<Long>();
		if (ability.getHostileUnitNames().size() == 0){
			for (long unitId:battle.getDefendingUnits()){
				unitsToDestroy.add(unitId);
			}
		}else{
			for (long unitId:battle.getDefendingUnits()){
				StarcraftUnit unit = battle.getBattleUnit(unitId);
				if (abilityIsAppliedAgainst(ability, unit)){
					unitsToDestroy.add(unitId);
				}
			}
		}
		if (unitsToDestroy.size() > 0){
			battle.addDefendersToSplash(unitsToDestroy);
		}
	}

	/**application des différentes formes de splash**/
	private void applyDefenderSplash(CombatCardAbility ability, StarcraftBattle battle){
		HashSet<Long> unitsToDestroy = new HashSet<Long>();
		if (ability.getHostileUnitNames().size() == 0){
			for (long unitId:battle.getAttackingUnits()){
				unitsToDestroy.add(unitId);
			}
		}else{
			for (long unitId:battle.getAttackingUnits()){
				StarcraftUnit unit = battle.getBattleUnit(unitId);
				if (abilityIsAppliedAgainst(ability, unit)){
					unitsToDestroy.add(unitId);
				}
			}
		}
		if (unitsToDestroy.size() > 0){
			battle.addAttackersToSplash(unitsToDestroy);
		}
	}
	
	/**vérifie si la capacité choisie s'applique à l'unitée ennemie sélectionnée**/
	private Boolean abilityIsAppliedAgainst(CombatCardAbility ability, StarcraftUnit unit){
		Boolean result = false;
		if (ability.getHostileUnitNames().contains("ground") && unit.getMoveType().equals("ground")){
			result = true;
		}else if (ability.getHostileUnitNames().contains("flying") && unit.getMoveType().equals("flying")){
			result = true;
		}else if (ability.getHostileUnitNames().contains(unit.getName())){
			result = true;
		}
		return result;
	}
	
	/**modife le type d'attaque**/
	private String modifiyAttackType(CombatCardAbility ability, StarcraftUnit unit){
		String result = unit.getAttackType();
		if (ability.getName().equals("cancelGroundAttack")){
			if (abilityIsAppliedAgainst(ability, unit)){
				if (result.equals("all")){
					result = "flying";
				}else if (result.equals("ground")){
					result = "none";
				}
			}
		}else if (ability.getName().equals("cancelEnemyAttack") || ability.getName().equals("cancelBothAttacks")){
			if (abilityIsAppliedAgainst(ability, unit)){
				result = "none";
			}
		}else if (ability.getName().equals("cancelAirAttack")){
			if (abilityIsAppliedAgainst(ability, unit)){
				if (result.equals("all")){
					result = "ground";
				}else if (result.equals("flying")){
					result = "none";
				}
			}
		}
		return result;
	}
	
	/**renvoie toutes les unités de l'attaquant is true, toutes celles du défenseur sinon**/
	private Map<Long, StarcraftUnit> getAllPlayerUnits(Boolean isAttacker){
		Map<Long, StarcraftUnit> result = new HashMap<Long, StarcraftUnit>();
		if (isAttacker){
			result.put(this.attackingUnit.getId(), this.attackingUnit);
			result.putAll(this.attackingSupports);
		}else{
			result.put(this.defendingUnit.getId(), this.defendingUnit);
			result.putAll(this.defendingSupports);
		}
		return result;
	}
	
	/****/
	private void cancelCardAbilities(CombatCard card, Boolean isAttacker){
		for (String abilityName:card.getAbilitiesList().keySet()){
			CombatCardAbility ability = card.getAbilitiesList().get(abilityName);
			if (isAttacker){
				this.attackerAbilities.remove(ability);
			}else{
				this.defenderAbilities.remove(ability);
			}
		}
	}
	
	/**rajoute les capacités de combats dues aux bonus temporaires du joueur**/
	private void addTempBonuses(StarcraftBattle battle){
		if (battle.getAttackingPlayer().getTempBonusList().contains("antiAir")){
			if (this.defendingUnit.getMoveType().equals("flying")){
				CombatCardAbility antiAirAbility = new CombatCardAbility();
				antiAirAbility.setName("attackBonus");
				antiAirAbility.setAmount(1);
				this.attackerAbilities.add(antiAirAbility);
			}
		}
		if (battle.getAttackingPlayer().getTempBonusList().contains("globalDetection")){
			CombatCardAbility detectorAbility = new CombatCardAbility();
			detectorAbility.setName("detector");
			this.attackerAbilities.add(detectorAbility);
		}
		
		if (battle.getDefendingPlayer().getTempBonusList().contains("antiAir")){
			if (this.attackingUnit.getMoveType().equals("flying")){
				CombatCardAbility antiAirAbility = new CombatCardAbility();
				antiAirAbility.setName("attackBonus");
				antiAirAbility.setAmount(1);
				this.defenderAbilities.add(antiAirAbility);
			}
		}
		if (battle.getDefendingPlayer().getTempBonusList().contains("globalDetection")){
			CombatCardAbility detectorAbility = new CombatCardAbility();
			detectorAbility.setName("detector");
			this.defenderAbilities.add(detectorAbility);
		}
	}
}
