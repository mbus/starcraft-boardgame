package gameEntities.gameMap;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import gameEntities.GameConstants;
import gameEntities.StarcraftGame;
import gameEntities.StarcraftPlayer;
import gameEntities.methods.GameTurnHandler;
import gameEntities.playerItems.CombatCard;
import gameEntities.playerItems.StarcraftModule;
import gameEntities.playerItems.StarcraftUnit;

public class StarcraftBattle implements java.io.Serializable {
	private static final long serialVersionUID = 2895157665886278943L;
	private final String frontLineTurn = "placeFrontlineUnits";
	private final String supportLineTurn = "placeSupportUnits";
	private final String placeCardTurn = "placeCombatCards";
	
	private final String destroyUnitTurn = "destroyUnits";
	//
	private Map<Long, StarcraftUnit> unplacedUnits = new HashMap<Long, StarcraftUnit>();
	private int[] coordinates = new int[]{0, 0, 0};
	private StarcraftPlayer attackingPlayer;
	private Set<Long> attackingUnits = new HashSet<Long>();
	private StarcraftPlayer defendingPlayer;
	private Set<Long> defendingUnits = new HashSet<Long>();
	private int skirmishNumber;
	private int maxDefenseDepth;
	private int maxAttackDepth;
	private Map<Integer, StarcraftSkirmish> skirmishList = new HashMap<Integer, StarcraftSkirmish>();
	private ArrayList<HashSet<Long>> attackingUnitsToDestroy = new ArrayList<HashSet<Long>>();
	private ArrayList<HashSet<Long>> defendingUnitsToDestroy = new ArrayList<HashSet<Long>>();
	private ArrayList<HashSet<Long>> attackingUnitsToSplash = new ArrayList<HashSet<Long>>();
	private ArrayList<HashSet<Long>> defendingUnitsToSplash = new ArrayList<HashSet<Long>>();
	//il s'agit des unités invisibles
	private Set<Long> cloakedUnits = new HashSet<Long>();
	private Set<Long> retreatingUnits = new HashSet<Long>();
	private Boolean attackerHasCombatUnits = true;
	private Boolean defenderHasCombatUnits = true;
	private ArrayList<Object> activeAttackerBonuses = new ArrayList<Object>();
	private ArrayList<Object> activeDefenderBonuses = new ArrayList<Object>();
	private Boolean delayBattle = false;
	
	public Boolean getDelayBattle (){
		return this.delayBattle;
	}
	
	public void setDelayBattle(Boolean delay){
		this.delayBattle = delay;
	}
	
	/**ajoute une liste d'unité à détruire dans la liste des attaquants**/
	public void addAttackersToDestroy(HashSet<Long> unitSet){
		int i = 0;
		Boolean added = false;
		while (i < this.attackingUnitsToDestroy.size() && !added){
			if (unitSet.size() <= this.attackingUnitsToDestroy.get(i).size()){
				this.attackingUnitsToDestroy.add(i, unitSet);
				added = true;
			}else{
				i++;
			}
		}
		if (i == this.attackingUnitsToDestroy.size()){
			this.attackingUnitsToDestroy.add(unitSet);
		}
	}
	
	/**ajoute une liste d'unité à détruire(splash) dans la liste des attaquants**/
	public void addAttackersToSplash(HashSet<Long> unitSet){
		int i = 0;
		Boolean added = false;
		while (i < this.attackingUnitsToSplash.size() && !added){
			if (unitSet.size() <= this.attackingUnitsToSplash.get(i).size()){
				this.attackingUnitsToSplash.add(i, unitSet);
				added = true;
			}else{
				i++;
			}
		}
		if (i == this.attackingUnitsToSplash.size()){
			this.attackingUnitsToSplash.add(unitSet);
		}
	}
	
	public void addAttackingUnit(long unitId) {
		this.attackingUnits.add(unitId);
	}
	
	/**transfert une carte du champs de batille au menu de sélection, indique en plus si une carte supportait celle enlevée**/
	public int addBattleCardToMenu(int cardId, String playerName){
		int result = -1;
		String[] cardPosition = this.getBattleCardPosition(cardId, playerName);
		if (cardPosition[1].equals("frontAttackCardPlace")){
			StarcraftSkirmish skirmish = this.skirmishList.get(Integer.parseInt(cardPosition[0].substring(8)));
			if (skirmish.getSupportAttackCard() != null){
				result = skirmish.getSupportAttackCard().getId();
			}
		}else if (cardPosition[1].equals("frontDefenseCardPlace")){
			StarcraftSkirmish skirmish = this.skirmishList.get(Integer.parseInt(cardPosition[0].substring(8)));
			if (skirmish.getSupportDefenseCard() != null){
				result = skirmish.getSupportDefenseCard().getId();
			}
		}
		this.removeCardFromOldPlace(cardId, playerName);
		return result;
	}

	/**ajoute une carte à l'escarmouche choisie**/
	public void addCardToSkirmish(int skimishId, String place, int cardId, String playerName){
		CombatCard cardToAdd = getBattleCard(cardId, playerName);
		removeCardFromOldPlace(cardId, playerName);
		if (place.equals("frontAttackCardPlace")){
			this.skirmishList.get(skimishId).setFrontAttackCard(cardToAdd);
		}else if (place.equals("supportAttackCardPlace")){
			this.skirmishList.get(skimishId).setSupportAttackCard(cardToAdd);
		}else if (place.equals("frontDefenseCardPlace")){
			this.skirmishList.get(skimishId).setFrontDefenseCard(cardToAdd);
		}else if (place.equals("supportDefenseCardPlace")){
			this.skirmishList.get(skimishId).setSupportDefenseCard(cardToAdd);
		}
	}

	/**ajoute une liste d'unité à détruire dans la liste des défenseurs**/
	public void addDefendersToDestroy(HashSet<Long> unitSet){
		int i = 0;
		Boolean added = false;
		while (i < this.defendingUnitsToDestroy.size() && !added){
			if (unitSet.size() <= this.defendingUnitsToDestroy.get(i).size()){
				this.defendingUnitsToDestroy.add(i, unitSet);
				added = true;
			}else{
				i++;
			}
		}
		if (i == this.defendingUnitsToDestroy.size()){
			this.defendingUnitsToDestroy.add(unitSet);
		}
	}

	/**ajoute une liste d'unité à détruire dans la liste des défenseurs**/
	public void addDefendersToSplash(HashSet<Long> unitSet){
		int i = 0;
		Boolean added = false;
		while (i < this.defendingUnitsToSplash.size() && !added){
			if (unitSet.size() <= this.defendingUnitsToSplash.get(i).size()){
				this.defendingUnitsToSplash.add(i, unitSet);
				added = true;
			}else{
				i++;
			}
		}
		if (i == this.defendingUnitsToSplash.size()){
			this.defendingUnitsToSplash.add(unitSet);
		}
	}

	public void addDefendingUnit(long unitId) {
		this.defendingUnits.add(unitId);
	}

	private void addTempBonuses(StarcraftGame game){
		if (this.defendingPlayer != null){
			if (this.attackingPlayer.getBonusList().contains("antiAir")){
				if (game.getGalaxy().countFriendlyUnitsInPlace
						(this.coordinates[0], this.coordinates[1], this.coordinates[2], "base", this.attackingPlayer) > 0){
					this.attackingPlayer.addTempBonus("antiAir");
					this.attackingPlayer.addTempBonus("globalDetection");
					for (StarcraftModule module:this.attackingPlayer.getOwnedModules()){
						if (module.getName().equals("antiAir")){
							this.activeAttackerBonuses.add(module);
							break;
						}
					}
				}
			}
			if (this.attackingPlayer.getActiveSpecialOrderBonus()){
				this.activeAttackerBonuses.add("specialBonus");
			}
			if (this.defendingPlayer.getBonusList().contains("antiAir")){
				if (game.getGalaxy().countFriendlyUnitsInPlace
						(this.coordinates[0], this.coordinates[1], this.coordinates[2], "base", this.defendingPlayer) > 0){
					this.defendingPlayer.addTempBonus("antiAir");
					this.defendingPlayer.addTempBonus("globalDetection");
					for (StarcraftModule module:this.defendingPlayer.getOwnedModules()){
						if (module.getName().equals("antiAir")){
							this.activeDefenderBonuses.add(module);
							break;
						}
					}
				}
			}
			if (this.defendingPlayer.getBonusList().contains("defensiveModule")){
				this.defendingPlayer.addTempBonus("defensiveModule");
				for (StarcraftModule module:this.defendingPlayer.getOwnedModules()){
					if (module.getName().equals("defensive")){
						this.activeDefenderBonuses.add(module);
						break;
					}
				}
			}
		}
	}
	
	//TODO ajouter la destruction des transports si nécessaires
	private Boolean antiAirTransports(StarcraftGame game){
		Boolean delay = false;
		if (this.defendingPlayer.getTempBonusList().contains("antiAir")){
			for (long unitId:this.attackingUnits){
				StarcraftUnit unit = game.getGalaxy().getUnitList().get(unitId);
				if (unit.getOldCoordinates()[0] != unit.getCoordinates()[0]
						|| unit.getOldCoordinates()[1] != unit.getCoordinates()[1]){
					int[] coordinate1 = new int[]{unit.getOldCoordinates()[0], unit.getOldCoordinates()[1]};
					int[] coordinate2 = new int[]{unit.getCoordinates()[0], unit.getCoordinates()[1]};
					String ownerName = this.attackingPlayer.getName();
					HashSet<Long> transportsToDestroy =
							game.getGalaxy().getUsedTransports(coordinate1, coordinate2, ownerName);
					Boolean alreadyUsed = false;
					for (HashSet<Long> unitList:this.attackingUnitsToDestroy){
						for (long id:transportsToDestroy){
							if (unitList.contains(id)){
								alreadyUsed = true;
								break;
							}
						}
					}
					if (!alreadyUsed){
						this.attackingUnitsToDestroy.add(transportsToDestroy);
					}
				}
			}
			for (HashSet<Long> unitList:this.attackingUnitsToDestroy){
				for (long id:unitList){
					this.attackingUnits.add(id);
					this.unplacedUnits.put(id, null);
				}
			}
			this.applyUnitDestruction(game);
			if (this.attackingUnitsToDestroy.size() > 0){
				game.addSpecialTurn(this.attackingPlayer.getName(), "removeTransportsFromAttackers");
				game.addSpecialTurn(this.attackingPlayer.getName(), "galaxyUnitDestruction");
				delay = true;
			}
		}
		return delay;
	}

	/**ajoute une unité à l'escarmouche choisie**/
	public void addUnitToSkirmish(int skimishId, String place, long unitId){
		StarcraftUnit unitToAdd = getBattleUnit(unitId);
		removeUnitFromOldPlace(unitId);
		if (place.equals("frontAttackPlace")){
			this.skirmishList.get(skimishId).setAttackingUnit(unitToAdd);
		}else if (place.equals("frontDefensePlace")){
			this.skirmishList.get(skimishId).setDefendingUnit(unitToAdd);
		}else if (place.equals("supportAttackPlace")){
			this.skirmishList.get(skimishId).addAttackingSupport(unitToAdd);
		}else if (place.equals("supportDefensePlace")){
			this.skirmishList.get(skimishId).addDefendingSupport(unitToAdd);
		}
	}
	
	/**transfert une unité du champs de batille au menu de sélection**/
	public void addUnitToUnplacedUnits(long unitId){
		StarcraftUnit unitToAdd = getBattleUnit(unitId);
		removeUnitFromOldPlace(unitId);
		this.addUnplacedUnit(unitToAdd);
	}
	
	public void addUnplacedUnit(StarcraftUnit unit) {
		this.unplacedUnits.put(unit.getId(), unit);
	}
	
	/**active les bonus liés aux escarmouches**/
	public void applyPlayerBonuses(){
		for (int skirmishId:this.skirmishList.keySet()){
			this.skirmishList.get(skirmishId).addGeneralBonuses(this);
		}
	}
	
	/**on applique la destruction des unités**/
	public void applyUnitDestruction(StarcraftGame game){
		//destruction automatique des unités attaquantes
		while (this.attackingUnitsToDestroy.size() > 0 && getAttackerSetSize(this.attackingUnitsToDestroy.get(0)) < 2){
			if (getAttackerSetSize(this.attackingUnitsToDestroy.get(0)) == 0){
				this.attackingUnitsToDestroy.remove(0);
			}else{
				for (long unitId:this.attackingUnitsToDestroy.get(0)){
					if (this.attackingUnits.contains(unitId) && !this.retreatingUnits.contains(unitId)){
						this.destroyChosenUnit(unitId, game);
						break;
					}
				}
			}
		}

		//destruction automatique des unités défendantes
		while (this.defendingUnitsToDestroy.size() > 0 && getDefenderSetSize(this.defendingUnitsToDestroy.get(0)) < 2){
			if (getDefenderSetSize(this.defendingUnitsToDestroy.get(0)) == 0){
				this.defendingUnitsToDestroy.remove(0);
			}else{
				for (long unitId:this.defendingUnitsToDestroy.get(0)){
					if (this.defendingUnits.contains(unitId) && !this.retreatingUnits.contains(unitId)){
						this.destroyChosenUnit(unitId, game);
						break;
					}
				}
			}
		}
		
		if (this.attackingUnitsToDestroy.size() == 0){
			for (long id:this.attackingUnits){
				if (this.cloakedUnits.contains(id)){
					this.cloakedUnits.remove(id);
				}
			}
		}
		if (this.defendingUnitsToDestroy.size() == 0){
			for (long id:this.defendingUnits){
				if (this.cloakedUnits.contains(id)){
					this.cloakedUnits.remove(id);
				}
			}
		}

		//destruction automatique des unités attaquantes
		while (this.attackingUnitsToSplash.size() > 0 && getAttackerSetSize(this.attackingUnitsToSplash.get(0)) < 2){
			if (getAttackerSetSize(this.attackingUnitsToSplash.get(0)) == 0){
				this.attackingUnitsToSplash.remove(0);
			}else{
				for (long unitId:this.attackingUnitsToSplash.get(0)){
					if (this.attackingUnits.contains(unitId) && !this.retreatingUnits.contains(unitId)){
						this.destroyChosenUnit(unitId, game);
						break;
					}
				}
			}
		}

		//destruction automatique des unités défendantes
		while (this.defendingUnitsToSplash.size() > 0 && getDefenderSetSize(this.defendingUnitsToSplash.get(0)) < 2){
			if (getDefenderSetSize(this.defendingUnitsToSplash.get(0)) == 0){
				this.defendingUnitsToSplash.remove(0);
			}else{
				for (long unitId:this.defendingUnitsToSplash.get(0)){
					if (this.defendingUnits.contains(unitId) && !this.retreatingUnits.contains(unitId)){
						this.destroyChosenUnit(unitId, game);
						break;
					}
				}
			}
		}
	}
	
	public Boolean canEndTurn(String player, StarcraftGame game){
		Boolean result = true;
		Galaxy galaxy = game.getGalaxy();
		Boolean succesfulAttack = isSuccessfulAttack(game);
		PlanetArea area = galaxy.returnPlanetAt(this.coordinates[0], this.coordinates[1]).getArea(this.coordinates[2]);
		
		if (succesfulAttack){
			//retraite des unités en trop
			if (player.equals(this.attackingPlayer.getName())){
				if (this.attackingUnits.size() > area.getUnitLimit(this.attackingPlayer, game.getGalaxy())){
					if (galaxy.countFriendlyUnitsInPlace(this.coordinates[0],
							this.coordinates[1],
							this.coordinates[2],
							"mobile",
							this.attackingPlayer) != area.getUnitLimit(this.attackingPlayer, game.getGalaxy())){
						result = false;
					}
				}
			}else{
				if (galaxy.countFriendlyUnitsInPlace(this.coordinates[0],
						this.coordinates[1],
						this.coordinates[2],
						"mobile",
						this.defendingPlayer) > 0){
					result = false;
				}
			}
		}else{
			if (player.equals(this.attackingPlayer.getName())){
				if (galaxy.countFriendlyUnitsInPlace(this.coordinates[0],
						this.coordinates[1],
						this.coordinates[2],
						"mobile",
						this.attackingPlayer) > 0){
					result = false;
				}
			}
		}
		return result;
	}
	
	//TODO
	/**vérifie si on peut finir le tour**/
	public Boolean checkEndBattleTurn(StarcraftGame game){
		Boolean result = false;
		if (game.getTurnPart().equals(this.frontLineTurn)){
			if (this.unplacedUnits.size() + 2 * this.skirmishNumber == this.attackingUnits.size()+ this.defendingUnits.size()){
				result = true;
			}
		}else if (game.getTurnPart().equals(this.supportLineTurn)){
			if (game.getPlayerCurrentlyPlaying().equals(this.attackingPlayer.getName())){
				if (this.unplacedUnits.size() + 2 * this.skirmishNumber + this.maxAttackDepth - 1
						== this.attackingUnits.size()+ this.defendingUnits.size()){
					result = true;
				}
			}else{
				if (this.unplacedUnits.size() ==0){
					result = true;
				}
			}
		}
		return result;
	}
	
	/**détruit une unité choisie parmi un ensemble d'unités à détruire puis enlève cet ensemble**/
	public void destroyChosenUnit(long id, StarcraftGame game){
		if (this.cloakedUnits.contains(id)){
			this.retreatingUnits.add(id);
		}else{
			removeUnitFromOldPlace(id);
			if (this.attackingUnits.contains(id)){
				this.attackingUnits.remove(id);
				if (this.attackingUnitsToDestroy.size() > 0){
					this.attackingUnitsToDestroy.remove(0);
				}else if (!this.attackingUnitsToSplash.isEmpty()){
					this.attackingUnitsToSplash.remove(0);
				}
			}else{
				this.defendingUnits.remove(id);
				if (this.defendingUnitsToDestroy.size() > 0){
					this.defendingUnitsToDestroy.remove(0);
				}else if (!this.defendingUnitsToSplash.isEmpty()){
					this.defendingUnitsToSplash.remove(0);
				}
			}
			game.getGalaxy().removeUnit(id, game);
			GameTurnHandler.removeUnitDisplay(id, game);
		}
	}
	
	public void drawCombatCards(){
		if (this.defendingPlayer != null){
			int attackingPlayerCards = 3;
			if (this.attackingPlayer.getActiveSpecialOrderBonus()){
				attackingPlayerCards += 2;
			}
			if (this.attackingPlayer.getBonusList().contains("aggressiveBonus")){
				attackingPlayerCards += 1;
			}
			this.attackingPlayer.drawCombatCards(attackingPlayerCards);
			
			int defendingPlayerCards = 1;
			if (this.defendingPlayer.getSpecies().equals("Protoss")){
				defendingPlayerCards += 2;
			}
			if (this.defendingPlayer.getBonusList().contains("defenseCardBonus")){
				defendingPlayerCards += 2;
			}
			this.defendingPlayer.drawCombatCards(defendingPlayerCards);
		}
		
	}
	
	/**détermine toutes les étapes possible après la fin de premier tour**/
	public void endFrontLineTurn(StarcraftGame game){
		if (this.maxAttackDepth > 1){
			game.addSpecialTurnEnd(this.attackingPlayer.getName(), this.supportLineTurn);
		}
		if (this.maxDefenseDepth > 1){
			game.addSpecialTurnEnd(this.defendingPlayer.getName(), this.supportLineTurn);
		}
		if (this.defendingPlayer.getTempBonusList().contains("defensiveModule") && this.defendingUnits.size() > 1){
			game.addSpecialTurnEnd(this.defendingPlayer.getName(),  "swapBattleUnitsTurn");
		}
		if (this.defendingPlayer.getTempBonusList().contains("reconnaissance")
				|| !this.attackingPlayer.getTempBonusList().contains("reconnaissance")){
			game.addSpecialTurnEnd(this.attackingPlayer.getName(), this.placeCardTurn);
			game.addSpecialTurnEnd(this.defendingPlayer.getName(), this.placeCardTurn);
		}else{
			game.addSpecialTurnEnd(this.defendingPlayer.getName(), this.placeCardTurn);
			game.addSpecialTurnEnd(this.attackingPlayer.getName(), this.placeCardTurn);
		}
		
		game.addSpecialTurnEnd(this.attackingPlayer.getName(), "revealBattleCards");
		game.addSpecialTurnEnd(this.attackingPlayer.getName(), this.destroyUnitTurn);
		game.addSpecialTurnEnd(this.defendingPlayer.getName(), this.destroyUnitTurn);
		game.addSpecialTurnEnd(this.defendingPlayer.getName(), GameConstants.moveRetreatUnitTurnName);
		game.addSpecialTurnEnd(this.attackingPlayer.getName(), GameConstants.moveRetreatUnitTurnName);
		game.addSpecialTurnEnd(this.attackingPlayer.getName(), "endBattleTurn");
	}
	
	/**indique quelles unités sont sélectionnables en fonction de l'étape de résolution de bataille**/
	public ArrayList<Long> getActiveUnits(StarcraftGame game){
		ArrayList<Long> activeUnits = new ArrayList<Long>();
		if (game.getTurnPart().equals(this.frontLineTurn)){
			//on récupère toutes les unités pouvant être placées devant
			for (long attackingUnitId:this.attackingUnits){
				if (this.unplacedUnits.containsKey(attackingUnitId)){
					if (isValidFrontLine(this.unplacedUnits.get(attackingUnitId))){
						activeUnits.add(attackingUnitId);
					}
				}else{
					//l'unité a déjà été placée et est donc valide
					activeUnits.add(attackingUnitId);
				}
			}
			for (long defendingUnitId:this.defendingUnits){
				if (this.unplacedUnits.containsKey(defendingUnitId)){
					if (isValidFrontLine(this.unplacedUnits.get(defendingUnitId))){
						activeUnits.add(defendingUnitId);
					}
				}else{
					//l'unité a déjà été placée et est donc valide
					activeUnits.add(defendingUnitId);
				}
			}
		}else if (game.getTurnPart().equals(this.supportLineTurn)){
			//le joueur courant est celui qui attaque
			if (game.getPlayerCurrentlyPlaying().equals(this.attackingPlayer.getName())){
				for (long attackingUnitId:this.attackingUnits){
					if (this.unplacedUnits.containsKey(attackingUnitId)){
						activeUnits.add(attackingUnitId);
					}else{
						for (int skirmishId:this.skirmishList.keySet()){
							if (this.skirmishList.get(skirmishId).getAttackingSupports().containsKey(attackingUnitId)){
								activeUnits.add(attackingUnitId);
								break;
							}
						}
					}
				}
			}else{
				//le joueur courant est celui qui défend
				for (long defendingUnitId:this.defendingUnits){
					if (this.unplacedUnits.containsKey(defendingUnitId)){
						activeUnits.add(defendingUnitId);
					}else{
						for (int skirmishId:this.skirmishList.keySet()){
							if (this.skirmishList.get(skirmishId).getDefendingSupports().containsKey(defendingUnitId)){
								activeUnits.add(defendingUnitId);
								break;
							}
						}
					}
				}
			}
		}
		return activeUnits;
	}
	
	public ArrayList<CombatCard> getAllCombatCards(){
		ArrayList<CombatCard> result = new ArrayList<CombatCard>();
		for (int skirmishId:this.skirmishList.keySet()){
			StarcraftSkirmish skirmish = this.skirmishList.get(skirmishId);
			if (skirmish.getFrontAttackCard() != null){
				result.add(skirmish.getFrontAttackCard());
			}
			if (skirmish.getFrontDefenseCard() != null){
				result.add(skirmish.getFrontDefenseCard());
			}
			if (skirmish.getSupportAttackCard() != null){
				result.add(skirmish.getSupportAttackCard());
			}
			if (skirmish.getSupportDefenseCard() != null){
				result.add(skirmish.getSupportDefenseCard());
			}
		}
		return result;
	}
	
	public ArrayList<CombatCard> getAllValidRechargeCards(String playerName){
		ArrayList<CombatCard> result = new ArrayList<CombatCard>();
		if (playerName.equals(this.attackingPlayer.getName())){
			for (int skirmishId:this.skirmishList.keySet()){
				if (this.skirmishList.get(skirmishId).getSupportAttackCard() != null){
					CombatCard supportCard = this.skirmishList.get(skirmishId).getSupportAttackCard();
					if (!supportCard.getName().equals("")){
						result.add(supportCard);
					}
				}
			}
		}else{
			for (int skirmishId:this.skirmishList.keySet()){
				if (this.skirmishList.get(skirmishId).getSupportDefenseCard() != null){
					CombatCard supportCard = this.skirmishList.get(skirmishId).getSupportDefenseCard();
					if (!supportCard.getName().equals("")){
						result.add(supportCard);
					}
				}
			}
		}
		return result;
	}
	
	/**compte le nombre d'unités vivantes dans un set d'unités**/
	private int getAttackerSetSize(HashSet<Long> attackerSet){
		int result = 0;
		if (attackerSet.size() > 0){
			for (long unitId:attackerSet){
				if (this.attackingUnits.contains(unitId) && !this.retreatingUnits.contains(unitId)){
					result++;
				}
			}
		}
		return result;
	}
	
	public StarcraftPlayer getAttackingPlayer() {
		return attackingPlayer;
	}
	
	public Set<Long> getAttackingUnits() {
		return this.attackingUnits;
	}
	
	/**renvoie la liste des unités à détruire**/
	public ArrayList<HashSet<Long>> getAttackingUnitsToDestroy() {
		 ArrayList<HashSet<Long>> result = new  ArrayList<HashSet<Long>>();
		 if (this.attackingUnitsToDestroy.size() > 0){
			 result = this.attackingUnitsToDestroy;
		 }else{
			 result = this.attackingUnitsToSplash;
		 }
		return result;
	}
	
	/**renvoie la carte du joueur ayant l'identifiant choisi**/
	public CombatCard getBattleCard(int cardId, String playerName){
		CombatCard result = null;
		if (playerName.equals(this.attackingPlayer.getName())){
			if (this.attackingPlayer.getCombatCardsInHand().containsKey(cardId)){
				result = this.attackingPlayer.getCombatCardsInHand().get(cardId);
			}
		}else{
			if (this.defendingPlayer.getCombatCardsInHand().containsKey(cardId)){
				result = this.defendingPlayer.getCombatCardsInHand().get(cardId);
			}
		}
		if (result == null){
			String[] cardPosition = this.getBattleCardPosition(cardId, playerName);
			if (cardPosition != null){
				StarcraftSkirmish skirmish = this.skirmishList.get(Integer.parseInt(cardPosition[0].substring(8)));
				if (cardPosition[1].equals("supportAttackCardPlace")){
					result = skirmish.getSupportAttackCard();
				}else if (cardPosition[1].equals("frontAttackCardPlace")){
					result = skirmish.getFrontAttackCard();
				}else if (cardPosition[1].equals("frontDefenseCardPlace")){
					result = skirmish.getFrontDefenseCard();
				}else{
					result = skirmish.getSupportDefenseCard();
				}
			}
		}
		return result;
	}

	/**renvoie l'endroit de la bataille où est située la carte, null si la carte n'est pas placée**/
	public String[] getBattleCardPosition(int cardId, String playerName){
		String[] result = null;
		Boolean isAttacker = false;
		if (playerName.equals(this.attackingPlayer.getName())){
			isAttacker = true;
		}
		if (isAttacker){
			for (int skimishId:this.skirmishList.keySet()){
				StarcraftSkirmish skirmish = this.skirmishList.get(skimishId);
				if (skirmish.getFrontAttackCard() != null){
					if (skirmish.getFrontAttackCard().getId() == cardId){
						result= new String[]{"skirmish" + Integer.toString(skimishId), "frontAttackCardPlace"};
						break;
					}
				}
				if (skirmish.getSupportAttackCard() != null){
					if (skirmish.getSupportAttackCard().getId() == cardId){
						result= new String[]{"skirmish" + Integer.toString(skimishId), "supportAttackCardPlace"};
						break;
					}
				}
			}
		}else{
			for (int skimishId:this.skirmishList.keySet()){
				StarcraftSkirmish skirmish = this.skirmishList.get(skimishId);
				if (skirmish.getFrontDefenseCard() != null){
					if (skirmish.getFrontDefenseCard().getId() == cardId){
						result= new String[]{"skirmish" + Integer.toString(skimishId), "frontDefenseCardPlace"};
						break;
					}
				}
				if (skirmish.getSupportDefenseCard() != null){
					if (skirmish.getSupportDefenseCard().getId() == cardId){
						result= new String[]{"skirmish" + Integer.toString(skimishId), "supportDefenseCardPlace"};
						break;
					}
				}
			}
		}
		return result;
	}
	
	/**renvoie l'endroit de la bataille où est située l'unité, null si l'unité n'est pas placée**/
	public String[] getBattlePosition(long unitId){
		String[] result = null;
		if (!this.unplacedUnits.containsKey(unitId)){
			Boolean attackingUnit = false;
			if (this.attackingUnits.contains(unitId)){
				attackingUnit = true;
			}
			for (int skimishId:this.skirmishList.keySet()){
				StarcraftSkirmish skirmish = this.skirmishList.get(skimishId);
				if (attackingUnit){
					if (skirmish.getAttackingUnit() != null){
						if (skirmish.getAttackingUnit().getId() == unitId){
							result= new String[]{"skirmish" + Integer.toString(skimishId), "frontAttackPlace"};
							break;
						}
					}
					if (skirmish.getAttackingSupports().containsKey(unitId)){
						result= new String[]{"skirmish" + Integer.toString(skimishId), "supportAttackPlace"};
						break;
					}
				}else{
					if (skirmish.getDefendingUnit() != null){
						if (skirmish.getDefendingUnit().getId() == unitId){
							result= new String[]{"skirmish" + Integer.toString(skimishId), "frontDefensePlace"};
							break;
						}
					}
					if (skirmish.getDefendingSupports().containsKey(unitId)){
						result= new String[]{"skirmish" + Integer.toString(skimishId), "supportDefensePlace"};
						break;
					}
				}
			}
		}
		return result;
	}
	
	/**renvoie l'unité ayant l'identifiant choisi**/
	public StarcraftUnit getBattleUnit(long unitId){
		StarcraftUnit result = null;
		if (!this.unplacedUnits.containsKey(unitId)){
			String[] unitPosition = this.getBattlePosition(unitId);
			StarcraftSkirmish skirmish = this.skirmishList.get(Integer.parseInt(unitPosition[0].substring(8)));
			if (unitPosition[1].equals("supportAttackPlace")){
				result= skirmish.getAttackingSupports().get(unitId);
			}else if (unitPosition[1].equals("frontAttackPlace")){
				result= skirmish.getAttackingUnit();
			}else if (unitPosition[1].equals("frontDefensePlace")){
				result= skirmish.getDefendingUnit();
			}else{
				result= skirmish.getDefendingSupports().get(unitId);
			}
		}else{
			result = this.unplacedUnits.get(unitId);
		}
		return result;
	}

	public  Set<Long> getCloackedUnits() {
		return this.cloakedUnits;
	}
	
	public int[] getCoordinates() {
		return coordinates;
	}
	
	/**compte le nombre d'unités vivantes dans un set d'unités**/
	private int getDefenderSetSize(HashSet<Long> defenderSet){
		int result = 0;
		if (defenderSet.size() > 0){
			for (long unitId:defenderSet){
				if (this.defendingUnits.contains(unitId) && !this.retreatingUnits.contains(unitId)){
					result++;
				}
			}
		}
		return result;
	}
	
	public StarcraftPlayer getDefendingPlayer() {
		return defendingPlayer;
	}
	
	public Set<Long> getDefendingUnits() {
		return this.defendingUnits;
	}
	
	public ArrayList<HashSet<Long>> getDefendingUnitsToDestroy() {
		ArrayList<HashSet<Long>> result = new  ArrayList<HashSet<Long>>();
		 if (this.defendingUnitsToDestroy.size() > 0){
			 result = this.defendingUnitsToDestroy;
		 }else{
			 result = this.defendingUnitsToSplash;
		 }
		return result;
	}
	
	public int getMaxAttackDepth() {
		return this.maxAttackDepth;
	}
	
	public int getMaxDefenseDepth() {
		return this.maxDefenseDepth;
	}
	
	public CombatCard getRandomCard(String playerName) throws Exception{
		CombatCard result = null;
		CombatCard randomCard = null;
		Boolean cardUsed = false;
		if (playerName.equals(this.attackingPlayer.getName())){
			while (!cardUsed && this.attackingPlayer.getCombatCardDeck().size() > 0){
				randomCard = this.attackingPlayer.getCombatCardDeck().get(0);
				this.attackingPlayer.getCombatCardDeck().remove(0);
				if (randomCard.getMaxAttack() > -1){
					this.attackingPlayer.useUpCombatCard(randomCard);
					result = randomCard;
					cardUsed = true;
				}else{
					this.attackingPlayer.getDismissedCombatCards().add(randomCard);
				}
			}
			if (result == null){
				if (this.attackingPlayer.getDismissedCombatCards().size() > 0){
					this.attackingPlayer.getCombatCardDeck().addAll(this.attackingPlayer.getDismissedCombatCards());
					Collections.shuffle(this.attackingPlayer.getCombatCardDeck());
					this.attackingPlayer.getDismissedCombatCards().removeAll(this.attackingPlayer.getDismissedCombatCards());
					result = getRandomCard(playerName);
				}else{
					throw new Exception("Aucune carte valide n'est disponible dans la pioche du joueur");
				}
			}
		}else{
			while (!cardUsed && this.defendingPlayer.getCombatCardDeck().size() > 0){
				randomCard = this.defendingPlayer.getCombatCardDeck().get(0);
				this.defendingPlayer.getCombatCardDeck().remove(0);
				if (randomCard.getMaxAttack() > -1){
					this.defendingPlayer.useUpCombatCard(randomCard);
					result = randomCard;
					cardUsed = true;
				}else{
					this.defendingPlayer.getDismissedCombatCards().add(randomCard);
				}
			}
			if (result == null){
				if (this.defendingPlayer.getDismissedCombatCards().size() > 0){
					this.defendingPlayer.getCombatCardDeck().addAll(this.defendingPlayer.getDismissedCombatCards());
					Collections.shuffle(this.defendingPlayer.getCombatCardDeck());
					this.defendingPlayer.getDismissedCombatCards().removeAll(this.defendingPlayer.getDismissedCombatCards());
					result = getRandomCard(playerName);
				}else{
					throw new Exception("Aucune carte valide n'est disponible dans la pioche du joueur");
				}
			}
		}
		
		return result;
	}
	
	public Set<Long> getRetreatedUnits() {
		return retreatingUnits;
	}
	
	/**liste des unités pouvant faire retraite, cette fonction renvoie aussi les unité déjà parties pour que le joueur
	 * puisse continuer à les manipuler pour annuler ou modifier leurs actions**/
	public ArrayList<Long> getRetreatingUnits(String player, StarcraftGame game){
		Galaxy galaxy = game.getGalaxy();
		Boolean succesfulAttack = isSuccessfulAttack(game);
		
		ArrayList<Long> result = new ArrayList<Long>();
		PlanetArea area = galaxy.returnPlanetAt(this.coordinates[0], this.coordinates[1]).getArea(this.coordinates[2]);
		if (succesfulAttack){
			//retraite des unités en trop
			if (player.equals(this.attackingPlayer.getName())){
				if (this.attackingUnits.size() > area.getUnitLimit(this.attackingPlayer, game.getGalaxy())){
					result.addAll(this.attackingUnits);
				}
			}else{
				result.addAll(this.defendingUnits);
			}
		}else{
			if (player.equals(this.attackingPlayer.getName())){
				result.addAll(this.attackingUnits);
			}
		}
		return result;
	}
	
	public Map<Integer, StarcraftSkirmish> getSkirmishList() {
		return skirmishList;
	}
	
	public int getSkirmishNumber() {
		return skirmishNumber;
	}
	
	public Map<Long, StarcraftUnit> getUnplacedUnits() {
		return unplacedUnits;
	}

	/**description des places vides dans lesquelles la carte peut être déplacée**/
	public ArrayList<String[]> getValidCardPlacement(String playerName, int cardId, StarcraftGame game){
		ArrayList<String[]> result = new ArrayList<String[]>();
		StarcraftPlayer activePlayer = game.getPlayer(playerName);
		CombatCard card = activePlayer.getCombatCardsInHand().get(cardId);
		Boolean isAttackingPlayer = false;
		if (playerName.equals(this.attackingPlayer.getName())){
			isAttackingPlayer = true;
		}
		Boolean isSupport = true;
		if (card.getMaxAttack() > -1){
			isSupport = false;
		}
		for (int skimishId:this.skirmishList.keySet()){
			StarcraftSkirmish skirmish = this.skirmishList.get(skimishId);
			if (isAttackingPlayer){
				if (isSupport){
					if (skirmish.getFrontAttackCard() != null && skirmish.getSupportAttackCard() == null){
						if (card.fullfillRequirements(skirmish, isAttackingPlayer, game)){
							result.add(new String[]{"skirmish" + Integer.toString(skimishId), "supportAttackCardPlace"});
						}
					}
				}else{
					if  (skirmish.getFrontAttackCard() == null){
						result.add(new String[]{"skirmish" + Integer.toString(skimishId), "frontAttackCardPlace"});
					}
				}
			}else{
				if (isSupport){
					if (skirmish.getFrontDefenseCard() != null && skirmish.getSupportDefenseCard() == null){
						if (card.fullfillRequirements(skirmish, isAttackingPlayer, game)){
							result.add(new String[]{"skirmish" + Integer.toString(skimishId), "supportDefenseCardPlace"});
						}
					}
				}else{
					if  (skirmish.getFrontDefenseCard() == null){
						result.add(new String[]{"skirmish" + Integer.toString(skimishId), "frontDefenseCardPlace"});
					}
				}
			}
		}
		return result;
	}

	/**description des places vides dans lesquelles l'unité peut être déplacée**/
	public ArrayList<String[]> getValidPlacement(long unitId, StarcraftGame game){
		ArrayList<String[]> result = new ArrayList<String[]>();
		if (game.getTurnPart().equals(this.frontLineTurn)){
			if (this.attackingUnits.contains(unitId)){
				for (int skimishId:this.skirmishList.keySet()){
					if (this.skirmishList.get(skimishId).getAttackingUnit() == null){
						result.add(new String[]{"skirmish" + Integer.toString(skimishId), "frontAttackPlace"});
					}
				}
			}else if (this.defendingUnits.contains(unitId)){
				for (int skimishId:this.skirmishList.keySet()){
					if (this.skirmishList.get(skimishId).getDefendingUnit() == null){
						result.add(new String[]{"skirmish" + Integer.toString(skimishId), "frontDefensePlace"});
					}
				}
			}
		}else if (game.getTurnPart().equals(this.supportLineTurn)){
			if (this.attackingUnits.contains(unitId)){
				for (int skimishId:this.skirmishList.keySet()){
					if (!this.skirmishList.get(skimishId).getAttackingSupports().containsKey(unitId)){
						result.add(new String[]{"skirmish" + Integer.toString(skimishId), "supportAttackPlace"});
					}
				}
			}else if (this.defendingUnits.contains(unitId)){
				for (int skimishId:this.skirmishList.keySet()){
					if (!this.skirmishList.get(skimishId).getDefendingSupports().containsKey(unitId)){
						result.add(new String[]{"skirmish" + Integer.toString(skimishId), "supportDefensePlace"});
					}
				}
			}
		}
		return result;
	}
	
	/**détermine si l'attaque est un succès**/
	public Boolean isSuccessfulAttack(StarcraftGame game){
		Galaxy galaxy = game.getGalaxy();
		Boolean successfulAttack = true;
		if (!this.defendingPlayer.getTempBonusList().contains("retreat")){
			if (this.attackingPlayer.getTempBonusList().contains("retreat")){
				successfulAttack = false;
			}else{
				if (this.attackingUnits.size() == 0){
					successfulAttack = false;
				}else{
					Boolean hasAttackUnit = false;
					for (long unitId:this.attackingUnits){
						if (!this.retreatingUnits.contains(unitId)){
							StarcraftUnit unit = galaxy.getUnitList().get(unitId);
							if (unit.getAttackType() != null){
								hasAttackUnit = true;
								break;
							}
						}
					}
					//si l'attaquant n'a que des unités de support et le défenseur à au moins une unité qui ne soit pas en
					//retraite, l'attaque échoue
					if (!hasAttackUnit && this.defendingUnits.size() > 0){
						for (long unitId:this.defendingUnits){
							if (!this.retreatingUnits.contains(unitId)){
								successfulAttack = false;
								break;
							}
						}
					}else if (hasAttackUnit){
						if (this.defendingUnits.size() > 0){
							for (long unitId:this.defendingUnits){
								if (!this.retreatingUnits.contains(unitId)){
									StarcraftUnit unit = galaxy.getUnitList().get(unitId);
									if (unit.getAttackType() != null){
										successfulAttack = false;
										break;
									}else{
										String skirmishPosition = this.getBattlePosition(unitId)[1];
										if (skirmishPosition.equals("frontDefensePlace")){
											successfulAttack = false;
											break;
										}
									}
								}
							}
						}
					}
				}
			}
		}
		return successfulAttack;
	}

	public Boolean isValidFrontLine(StarcraftUnit unit) {
		Boolean result = false;
		if (!this.attackerHasCombatUnits && this.attackingUnits.contains(unit.getId())){
			result = true;
		}else if (!this.defenderHasCombatUnits && this.defendingUnits.contains(unit.getId())){
			result = true;
		}else if (unit.getAttackType() != null){
			result = true;
		}
		return result;
	}

	public void removeAttackingUnit(Long unitId) {
		this.attackingUnits.remove(unitId);
	}
	
	/**retire la carte de la place où elle était**/
	public void removeCardFromOldPlace(int cardId, String playerName){
		String[] cardPosition = this.getBattleCardPosition(cardId, playerName);
		if (cardPosition != null){
			StarcraftSkirmish skirmish = this.skirmishList.get(Integer.parseInt(cardPosition[0].substring(8)));
			if (cardPosition[1].equals("supportAttackCardPlace")){
				skirmish.setSupportAttackCard(null);;
			}else if (cardPosition[1].equals("frontAttackCardPlace")){
				skirmish.setFrontAttackCard(null);
			}else if (cardPosition[1].equals("frontDefenseCardPlace")){
				skirmish.setFrontDefenseCard(null);
			}else{
				skirmish.setSupportDefenseCard(null);
			}
		}
	}
	
	public void removeDefendingUnit(Long unitId) {
		this.defendingUnits.remove(unitId);
	}

	/**retire l'unité de la place où elle était**/
	public void removeUnitFromOldPlace(long unitId){
		if (this.unplacedUnits.containsKey(unitId)){
			this.unplacedUnits.remove(unitId);
		}else{
			String[] unitPosition = this.getBattlePosition(unitId);
			StarcraftSkirmish skirmish = this.skirmishList.get(Integer.parseInt(unitPosition[0].substring(8)));
			if (unitPosition[1].equals("supportAttackPlace")){
				skirmish.removeAttackingSupport(unitId);
			}else if (unitPosition[1].equals("frontAttackPlace")){
				skirmish.setAttackingUnit(null);
			}else if (unitPosition[1].equals("frontDefensePlace")){
				skirmish.setDefendingUnit(null);
			}else{
				skirmish.removeDefendingSupport(unitId);
			}
		}
	}

	public void removeUnplacedUnit(Long unitId) {
		this.unplacedUnits.remove(unitId);
	}

	/**résout la bataille**/
	public void resolveBattle(StarcraftGame game){
		for (int skirmishId:this.skirmishList.keySet()){
			this.skirmishList.get(skirmishId).resolveSkirmish(this, game);
		}
		if (!this.delayBattle){
			applyUnitDestruction(game);
		}else{
			
		}
	}
	
	public void setAttackingPlayer(StarcraftPlayer attackingPlayer) {
		this.attackingPlayer = attackingPlayer;
	}
	
	public void setCoordinates(int[] newCoordinates) {
		this.coordinates = newCoordinates;
	}
	
	/**enlève les cartes de combats utilisées des mains du joueur**/
	public ArrayList<CombatCard> setEndCardTurn(String playerName, StarcraftGame game){
		ArrayList<CombatCard> addedCardList = new ArrayList<CombatCard>();
		if (playerName.equals(this.attackingPlayer.getName())){
			Boolean hasvalidRechargeCard = false;
			for (int skirmishId:this.skirmishList.keySet()){
				if (this.skirmishList.get(skirmishId).getFrontAttackCard() != null){
					CombatCard card = this.skirmishList.get(skirmishId).getFrontAttackCard();
					this.attackingPlayer.useUpCombatCard(card);
					if (this.skirmishList.get(skirmishId).getSupportAttackCard() != null){
						CombatCard supportCard = this.skirmishList.get(skirmishId).getSupportAttackCard();
						this.attackingPlayer.useUpCombatCard(supportCard);
						if (!supportCard.getName().equals("")){
							hasvalidRechargeCard = true;
						}
					}
				}else{
					try {
						CombatCard randomCard = getRandomCard(playerName);
						this.skirmishList.get(skirmishId).setFrontAttackCard(randomCard);
						addedCardList.add(randomCard);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
			if (this.attackingPlayer.getHasRecharge() && hasvalidRechargeCard){
				game.addSpecialTurn(playerName, "rechargeTurn", 1);
			}
		}else{
			Boolean hasvalidRechargeCard = false;
			for (int skirmishId:this.skirmishList.keySet()){
				if (this.skirmishList.get(skirmishId).getFrontDefenseCard() != null){
					CombatCard card = this.skirmishList.get(skirmishId).getFrontDefenseCard();
					this.defendingPlayer.useUpCombatCard(card);
					if (this.skirmishList.get(skirmishId).getSupportDefenseCard() != null){
						CombatCard supportCard = this.skirmishList.get(skirmishId).getSupportDefenseCard();
						this.defendingPlayer.useUpCombatCard(supportCard);
						if (!supportCard.getName().equals("")){
							hasvalidRechargeCard = true;
						}
					}
				}else{
					try {
						CombatCard randomCard = getRandomCard(playerName);
						this.skirmishList.get(skirmishId).setFrontDefenseCard(randomCard);
						addedCardList.add(randomCard);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
			if (this.defendingPlayer.getHasRecharge() && hasvalidRechargeCard){
				game.addSpecialTurn(playerName, "rechargeTurn", 1);
			}
		}
		GameTurnHandler.callUpdateCardNumber(this.attackingPlayer);
		GameTurnHandler.callUpdateCardNumber(this.defendingPlayer);
		return addedCardList;
	}
	
	/**fonction commençant la bataille**/
	public void startBattle(StarcraftGame game){
		//on met fin aux tours normaux
		game.removeAllTurnParts();
		
		Galaxy galaxy = game.getGalaxy();
		PlanetArea area = galaxy.returnPlanetAt(this.coordinates[0], this.coordinates[1]).getArea(this.coordinates[2]);
		for (long unitId:area.getUnitIdList()){
			StarcraftUnit starcraftUnit = galaxy.getUnitList().get(unitId);
			if (!this.getAttackingUnits().contains(unitId)){
				if (starcraftUnit.getType().equals("mobile")){
					addDefendingUnit(starcraftUnit.getId());
					this.addUnplacedUnit(starcraftUnit);
					if (defendingPlayer == null){
						this.defendingPlayer = game.getPlayer(starcraftUnit.getOwner());
					}
				}
			}
		}
		addTempBonuses(game);
		if (activateGlobalAbilities(game)){
			game.addSpecialTurnEnd(this.attackingPlayer.getName(), "delayedBattleStart");
		}else{
			setSkirmishNumber(game);
		}
	}
	
	/**mise en place de capacités de début de combat**/
	private Boolean activateGlobalAbilities(StarcraftGame game){
		Boolean delayBattleStart = false;
		if (this.defendingPlayer != null){
			delayBattleStart = this.startingCardAbilities(game);
			if (antiAirTransports(game)){
				delayBattleStart = true;
			}
			if (this.defendingPlayer.getBonusList().contains("charon boosters")){
				Boolean hasGoliath = false;
				for (long unitId:this.defendingUnits){
					if (game.getGalaxy().getUnitList().get(unitId).getName().equals("goliath")){
						hasGoliath = true;
						break;
					}
				}
				if (hasGoliath){
					HashSet<Long> unitsTodestroy = new HashSet<Long>();
					for (long unitId:this.attackingUnits){
						if (game.getGalaxy().getUnitList().get(unitId).getMoveType().equals("flying")){
							unitsTodestroy.add(unitId);
						}
					}
					if (unitsTodestroy.size() > 0){
						this.attackingUnitsToDestroy.add(unitsTodestroy);
						this.applyUnitDestruction(game);
						if (this.attackingUnitsToDestroy.size() > 0){
							game.addSpecialTurn(this.attackingPlayer.getName(), "galaxyUnitDestruction");
							delayBattleStart = true;
						}
					}
				}
			}
			if (this.defendingPlayer.getBonusList().contains("restoration")){
				for (long unitId:this.defendingUnits){
					if (game.getGalaxy().getUnitList().get(unitId).getName().equals("medic")){
						this.defendingPlayer.drawCombatCards(1);
						break;
					}
				}
			}
			if (this.attackingPlayer.getBonusList().contains("restoration")){
				for (long unitId:this.attackingUnits){
					if (game.getGalaxy().getUnitList().get(unitId).getName().equals("medic")){
						this.attackingPlayer.drawCombatCards(1);
						break;
					}
				}
			}
		}
		return delayBattleStart;
	}
	
	/**fonction permettant aux joueurs d'activer des cartes au début de la bataille**/
	private Boolean startingCardAbilities(StarcraftGame game){
		Boolean delayBattleStart = false;
		if (this.defendingPlayer.getSpecialActions().hasUsableCards(this.frontLineTurn)){
			delayBattleStart = true;
			game.addSpecialTurn(this.defendingPlayer.getName(), "useStartOfBattleCards");
		}
		if (this.attackingPlayer.getSpecialActions().hasUsableCards(this.frontLineTurn)){
			delayBattleStart = true;
			game.addSpecialTurn(this.attackingPlayer.getName(), "useStartOfBattleCards");
		}
		return delayBattleStart;
	}
	
	public void setSkirmishNumber(StarcraftGame game){
		//on n'engage la bataille que si le joueur en défense a des unités capable de défendre et que les unités
		//attaquantes n'ont pas été détruites au début de la bataille
		drawCombatCards();
		//l'invisibilité s'active après les étapes préliminaires
		this.activateCloaking();
		if (this.defendingUnits.size() > 0 && this.attackingUnits.size() > 0){
			game.addSpecialTurnEnd(this.attackingPlayer.getName(), this.frontLineTurn);
			int attackingFrontline = 0;
			int defendingFrontline = 0;
			for (long attackingUnitId:this.attackingUnits){
				if (this.unplacedUnits.get(attackingUnitId).getAttackType() != null){
					attackingFrontline++;
				}
			}
			for (long defendingUnitId:this.defendingUnits){
				if (this.unplacedUnits.get(defendingUnitId).getAttackType() != null){
					defendingFrontline++;
				}
			}
			if (attackingFrontline == 0){
				attackingFrontline++;
				this.attackerHasCombatUnits = false;
			}
			if (defendingFrontline == 0){
				defendingFrontline++;
				this.defenderHasCombatUnits = false;
			}
			if (defendingFrontline > attackingFrontline){
				this.skirmishNumber = attackingFrontline;
			}else{
				this.skirmishNumber = defendingFrontline;
			}
			this.maxDefenseDepth = this.defendingUnits.size() - this.skirmishNumber + 1;
			this.maxAttackDepth = this.attackingUnits.size() - this.skirmishNumber + 1;
			for (int i = 0; i < this.skirmishNumber; i++){
				StarcraftSkirmish skirmish = new StarcraftSkirmish();
				skirmish.setId(i);
				this.skirmishList.put(i, skirmish);
			}
		}else{
			//TODO faire la retraite si l'attaquant a mis trop d'unités
			game.getGalaxy().setStarcraftBattle(null);
			this.attackingPlayer.setActiveSpecialOrderBonus(false);
			game.nextTurn();
		}
	}

	public ArrayList<Object> getActiveAttackerBonuses() {
		return activeAttackerBonuses;
	}

	public ArrayList<Object> getActiveDefenderBonuses() {
		return activeDefenderBonuses;
	}
	
	public void removeTransportsFromAttackers(Galaxy galaxy){
		HashSet<Long> unitsToRemove = new HashSet<Long>();
		for (long unitId:this.attackingUnits){
			if (!galaxy.getUnitList().get(unitId).getType().equals("mobile")){
				unitsToRemove.add(unitId);
			}
		}
		for (long unitId:unitsToRemove){
			this.attackingUnits.remove(unitId);
			this.unplacedUnits.remove(unitId);
		}
	}
	
	public ArrayList<Long> getSwappableUnits(){
		ArrayList<Long> result = new ArrayList<Long>();
		for (int skirmishId:this.skirmishList.keySet()){
			StarcraftSkirmish skirmish = this.skirmishList.get(skirmishId);
			if (skirmish.getDefendingUnit().getAttackType() != null){
				result.add(skirmish.getDefendingUnit().getId());
			}
		}
		return result;
	}
	
	public ArrayList<Long> getSwappableUnits(long selectedUnitId){
		ArrayList<Long> result = new ArrayList<Long>();
		int skirmishUsed = -1;
		StarcraftUnit opposingUnit = null;
		StarcraftUnit selectedUnit = null;
		for (int skirmishId:this.skirmishList.keySet()){
			StarcraftSkirmish skirmish = this.skirmishList.get(skirmishId);
			if (skirmish.getDefendingUnit().getId() == selectedUnitId){
				skirmishUsed = skirmishId;
				opposingUnit = skirmish.getAttackingUnit();
				selectedUnit = skirmish.getDefendingUnit();
				break;
			}
		}
		for (int skirmishId:this.skirmishList.keySet()){
			StarcraftSkirmish skirmish = this.skirmishList.get(skirmishId);
			if (skirmishUsed != skirmishId){
				StarcraftUnit frontUnit = skirmish.getDefendingUnit();
				if (frontUnit.getAttackType() != null){
					StarcraftUnit opposingFrontUnit = skirmish.getAttackingUnit();
					if ((opposingUnit.getAttackType().equals("all")
							|| opposingUnit.getAttackType().equals(frontUnit.getMoveType()))
							&& (opposingFrontUnit.getAttackType().equals("all")
									|| opposingFrontUnit.getAttackType().equals(selectedUnit.getMoveType()))){
						result.add(frontUnit.getId());
					}
				}
			}
			for (long supportId:skirmish.getDefendingSupports().keySet()){
				StarcraftUnit supportUnit = skirmish.getDefendingSupports().get(supportId);
				if (supportUnit.getAttackType() != null){
					if (opposingUnit.getAttackType().equals("all")
							|| opposingUnit.getAttackType().equals(supportUnit.getMoveType())){
						result.add(supportUnit.getId());
					}
				}
			}
		}
		result.add(selectedUnitId);
		return result;
	}
	
	public void swapDefendingUnitPlacement(long unitId1, long unitId2){
		String place1 = "";
		String place2 = "";
		int skirmish1 = -1;
		int skirmish2 = -1;
		StarcraftUnit unit1 = null;
		StarcraftUnit unit2 = null;
		for (int skirmishId:this.skirmishList.keySet()){
			StarcraftSkirmish skirmish = this.skirmishList.get(skirmishId);
			if (skirmish.getDefendingUnit().getId() == unitId1){
				place1 = "front";
				skirmish1 = skirmishId;
				unit1 = skirmish.getDefendingUnit();
			}else if (skirmish.getDefendingUnit().getId() == unitId2){
				place2 = "front";
				skirmish2 = skirmishId;
				unit2 = skirmish.getDefendingUnit();
			}
			for (long unitId:skirmish.getDefendingSupports().keySet()){
				if (unitId == unitId1){
					place1 = "support";
					skirmish1 = skirmishId;
					unit1 = skirmish.getDefendingSupports().get(unitId);
				}else if (unitId == unitId2){
					place2 = "support";
					skirmish2 = skirmishId;
					unit2 = skirmish.getDefendingSupports().get(unitId);
				}
			}
		}
		if (place1.equals("front")){
			this.skirmishList.get(skirmish1).setDefendingUnit(unit2);
		}else{
			this.skirmishList.get(skirmish1).removeDefendingSupport(unitId1);
			this.skirmishList.get(skirmish1).addDefendingSupport(unit2);
		}
		if (place2.equals("front")){
			this.skirmishList.get(skirmish2).setDefendingUnit(unit1);
		}else{
			this.skirmishList.get(skirmish2).removeDefendingSupport(unitId2);
			this.skirmishList.get(skirmish2).addDefendingSupport(unit1);
		}
	}
	
	private void activateCloaking(){
		setUnitsInBattleMode();
		for (long battleUnitId:this.unplacedUnits.keySet()){
			if (this.unplacedUnits.get(battleUnitId).getAbilities().contains("cloaking")){
				this.cloakedUnits.add(battleUnitId);
			}
		}
	}
	
	public void setUnitsInBattleMode(){
		for (long battleUnitId:this.unplacedUnits.keySet()){
			this.unplacedUnits.get(battleUnitId).setStartingSituation("inBattle");
		}
	}
}
