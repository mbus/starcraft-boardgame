package gameEntities.playerItems;

import java.util.ArrayList;

public class CombatCardAbility implements java.io.Serializable {

	private static final long serialVersionUID = -7466986278296050475L;
	private String name;
	private int amount = 0;
	private ArrayList<String> alliedUnitNames = new ArrayList<String>();
	private ArrayList<String> hostileUnitNames = new ArrayList<String>();
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAmount() {
		return amount;
	}
	public void setAmount(int amount) {
		this.amount = amount;
	}
	public ArrayList<String> getAlliedUnitNames() {
		return alliedUnitNames;
	}
	public void setAlliedUnitNames(ArrayList<String> alliedUnitNames) {
		this.alliedUnitNames = alliedUnitNames;
	}
	public ArrayList<String> getHostileUnitNames() {
		return hostileUnitNames;
	}
	public void setHostileUnitNames(ArrayList<String> hostileUnitNames) {
		this.hostileUnitNames = hostileUnitNames;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((alliedUnitNames == null) ? 0 : alliedUnitNames.hashCode());
		result = prime * result + amount;
		result = prime * result + ((hostileUnitNames == null) ? 0 : hostileUnitNames.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CombatCardAbility other = (CombatCardAbility) obj;
		if (alliedUnitNames == null) {
			if (other.alliedUnitNames != null)
				return false;
		} else if (!alliedUnitNames.containsAll(other.alliedUnitNames) || !other.alliedUnitNames.containsAll(alliedUnitNames))
			return false;
		if (amount != other.amount)
			return false;
		if (hostileUnitNames == null) {
			if (other.hostileUnitNames != null)
				return false;
		} else if (!hostileUnitNames.containsAll(other.hostileUnitNames) || !other.hostileUnitNames.containsAll(hostileUnitNames))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
	

}
