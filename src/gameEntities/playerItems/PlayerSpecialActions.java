package gameEntities.playerItems;

import java.util.ArrayList;
import java.util.Arrays;

import gameEntities.GameConstants;
import gameEntities.StarcraftPlayer;
import gameEntities.gameMap.StarcraftBattle;
import gameEntities.methods.GameTurnHandler;

public class PlayerSpecialActions implements java.io.Serializable {

	private static final long serialVersionUID = 1749908066263847129L;
	private StarcraftPlayer player;
	
	public PlayerSpecialActions(StarcraftPlayer player){
		this.player = player;
	}
	
	public Boolean hasUsableCards(String turnName){
		Boolean result = false;
		for (String cardName:this.player.getGlobalCards().keySet()){
			CombatCard card = this.player.getGlobalCards().get(cardName);
			if (card.getTriggerName() != null && card.getTriggerName().equals(turnName)){
				result = true;
				break;
			}
		}
		if (!result){
			for (EventCard card:this.player.getEventCards().getUsableEventCards()){
				if (card.getType().equals("trigger") && card.getTurnName().equals(turnName)){
					result = true;
					break;
				}
			}
		}
		return result;
	}

	public ArrayList<CombatCard> getTriggeredCombatCards(String turnName){
		ArrayList<CombatCard> result = new ArrayList<CombatCard>();
		for (String cardName:this.player.getGlobalCards().keySet()){
			CombatCard card = this.player.getGlobalCards().get(cardName);
			if (card.getTriggerName() != null && card.getTriggerName().equals(turnName)){
				result.add(card);
			}
		}
		return result;
	}
	
	public ArrayList<EventCard> getTriggeredEventCards(String turnName){
		ArrayList<EventCard> result = new ArrayList<EventCard>();
		for (EventCard card:this.player.getEventCards().getUsableEventCards()){
			if (card.getType().equals("trigger") && card.getTurnName().equals(turnName)){
				result.add(card);
			}
		}
		return result;
	}
	
	public void useStartOfBattleCard(int cardId, StarcraftBattle battle){
		CombatCard usedCard = null;
		String newBonus = "";
		for (String cardName:this.player.getGlobalCards().keySet()){
			CombatCard card = this.player.getGlobalCards().get(cardName);
			if (card.getId() == cardId){
				usedCard = card;
				if (this.player.getName().equals(battle.getAttackingPlayer().getName())){
					battle.getActiveAttackerBonuses().add(usedCard);
				}else{
					battle.getActiveDefenderBonuses().add(usedCard);
				}
				break;
			}
		}
		EventCard usedEventCard = null;
		if (usedCard == null){
			for (EventCard card:this.player.getEventCards().getUsableEventCards()){
				if (card.getId() == cardId){
					usedEventCard = card;
					if (this.player.getName().equals(battle.getAttackingPlayer().getName())){
						battle.getActiveAttackerBonuses().add(usedEventCard);
					}else{
						battle.getActiveDefenderBonuses().add(usedEventCard);
					}
					break;
				}
			}
			newBonus = usedEventCard.getBonusName();
			this.player.getEventCards().getUsableEventCards().remove(usedEventCard);
			GameTurnHandler.updateActiveEventCardNumber(this.player.getName(), this.player.getGame());
		}else{
			newBonus = usedCard.getGlobalBonus();
			if (usedCard.getAbilitiesList().containsKey("sendToTechnologyDeck")){
				this.player.getBuyableCards().put(usedCard.getName(), new ArrayList<CombatCard>(Arrays.asList(usedCard)));
				this.player.getGlobalCards().remove(usedCard.getName());
				GameTurnHandler.updateActiveCombatCards(this.player.getName(), this.player.getGame());
			}
		}
		this.player.addTempBonus(newBonus);
		if (newBonus.equals("retreat")){
			while (this.player.getGame().specialTurnSize() > 1){
				this.player.getGame().removeSpecialTurn(this.player.getGame().specialTurnSize() - 1);
			}
			String opposingPlayer = "";
			if (this.player.getName().equals(battle.getAttackingPlayer().getName())){
				opposingPlayer = battle.getDefendingPlayer().getName();
			}else{
				opposingPlayer = battle.getAttackingPlayer().getName();
			}
			battle.setUnitsInBattleMode();
			this.player.getGame().addSpecialTurnEnd(this.player.getName(), GameConstants.moveRetreatUnitTurnName);
			this.player.getGame().addSpecialTurnEnd(opposingPlayer, GameConstants.moveRetreatUnitTurnName);
			this.player.getGame().addSpecialTurnEnd(this.player.getName(), "endBattleTurn");
		}else if (newBonus.equals("observers")){
			this.player.getGame().addSpecialTurn(this.player.getName(), "cardActivation", 1);
			CardActivationItem cardUse = new CardActivationItem();
			cardUse.setBonusName("globalDetection");
			cardUse.setType("combatCard");
			cardUse.setCombatCard(usedCard);
			cardUse.setGasCost(1);
			this.player.setBuyingOrder(cardUse);
		}
	}
	
	public void buyActivationCard(){
		CardActivationItem cardUse = (CardActivationItem) this.player.getBuyingOrder().getItem();
		this.player.addTempBonus(cardUse.getBonusName());
	}
	
	public void cancelActivationCard(){
		CardActivationItem cardUse = (CardActivationItem) this.player.getBuyingOrder().getItem();
		StarcraftBattle battle = this.player.getGame().getGalaxy().getStarcraftBattle();
		if (this.player.getName().equals(battle.getAttackingPlayer().getName())){
			battle.getActiveAttackerBonuses().remove(cardUse.getCombatCard());
		}else{
			battle.getActiveDefenderBonuses().remove(cardUse.getCombatCard());
		}
		this.player.cancelBuyingOrder(this.player.getGame());
	}
}
