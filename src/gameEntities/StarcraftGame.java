package gameEntities;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import entities.SaveGame;
import gameEntities.gameMap.PlanetArea;
import gameEntities.gameMap.RoadLink;
import gameEntities.gameMap.StarcraftBattle;
import gameEntities.methods.GameTurnHandler;
import gameEntities.methods.GlobalMethods;
import gameEntities.playerItems.EventCard;
import gameEntities.playerItems.OrderToken;
import gameEntities.playerItems.StarcraftUnit;
import gameEntities.playerItems.UnitPool;
import gameEntities.gameMap.AreaResource;
import gameEntities.gameMap.Galaxy;
import gameEntities.gameMap.Planet;

public class StarcraftGame implements java.io.Serializable {

	private static final long serialVersionUID = 4640965522133792796L;
	private SaveGame      saveGame;
	private Map<String, StarcraftPlayer> playerList = new HashMap<String, StarcraftPlayer>();
	private ArrayList<String> playerTurns = new ArrayList<String>();
	/**liste des joueurs ayant des trours spéciaux**/
	private ArrayList<String> playerSpecialTurns = new ArrayList<String>();
	private int currentTurn = 0;
	private String turnName;
	private ArrayList<String> specialTurnNames = new ArrayList<String>();
	// indique l'époque du jeu
	private int age = 1;
	private Galaxy galaxy = new Galaxy();
	private List<Planet> planetDeck = Collections.synchronizedList(new ArrayList<Planet>());
	private ArrayList<SpecialTurnEvent> speciaTurnEvents = new ArrayList<SpecialTurnEvent>();
	private ArrayList<String> turnPart = new ArrayList<String>();
	private ArrayList<String> turnPart2 = new ArrayList<String>();
	private OrderToken currentOrder;
	private Map<List<Integer>, PlanetArea> alreadyOwnedResourceAreas = new HashMap<List<Integer>, PlanetArea>();
	public UnitIdGenerator unitIdGenerator = new UnitIdGenerator();
	public CardIdGenerator cardIdGenerator = new CardIdGenerator();
	private ArrayList<EventCard> eventCards1 = new ArrayList<EventCard>();
	private final ArrayList<String> colorList =
			new ArrayList<String>(Arrays.asList("#9A32CD", "#9CCB19", "#FC1501", "#67E6EC", "#FFFF00", "#FFA500"));
	private int colorUsed = 0;
	private Set<String> defeatedPlayers = new HashSet<String>();
	private String winner;
	private Boolean interruption = false;
	
	public void surrenderStarcraftGame(String playerName){
		ArrayList<Long> unitsToRemove = new ArrayList<Long>();
		for (long unitId:galaxy.getUnitList().keySet()){
			if (galaxy.getUnitList().get(unitId).getOwner().equals(playerName)){
				unitsToRemove.add(unitId);
			}
		}
		for (long deletingId:unitsToRemove){
			galaxy.removeUnit(deletingId, this);
			GameTurnHandler.removeUnitDisplay(deletingId, this);
		}
		StarcraftPlayer player = this.getPlayer(playerName);
		for (List<Integer> areaCoordinates:player.getAreaResourceList().keySet()){
			PlanetArea area = player.getAreaResourceList().get(areaCoordinates);
			area.setWorkerAmount(0);
			GameTurnHandler.updateAreaWorkerDisplay(areaCoordinates, player, this);
		}
		player.getAreaResourceList().clear();
		for (String orderCoordinate:this.galaxy.getOrderList().keySet()){
			for (OrderToken orderToken:this.galaxy.getOrderList().get(orderCoordinate)){
				if (orderToken.getOwner().equals(playerName)){
					this.galaxy.getOrderList().get(orderCoordinate).remove(orderToken);
				}
			}
			if (this.galaxy.getOrderList().get(orderCoordinate).isEmpty()){
				this.galaxy.getOrderList().remove(orderCoordinate);
			}
		}
		this.defeatedPlayers.add(playerName);
		if (this.defeatedPlayers.size() == this.playerList.size() - 1){
			for (String winnerPossibility:this.playerList.keySet()){
				if (this.defeatedPlayers.contains(winnerPossibility)){
					this.winner =  winnerPossibility;
					break;
				}
			}
			this.turnName = "winnerScreen";
		}
	}
	
	public Set<String> getDefeatedPlayers(){
		return this.defeatedPlayers;
	}
	
	private void addConclavefleetTurn(){
		for (String playerName:this.playerList.keySet()){
			if (this.playerList.get(playerName).getBonusList().contains("conclaveFleet")){
				if (this.playerList.get(playerName).getUnitPools().get("conclaveFleetTurn").getUnitList().size() > 0){
					turnPartInterruption(true);
					this.addSpecialTurnEnd(playerName, "emptyTurn");
					this.addSpecialTurnEnd(playerName, "conclaveFleetTurn");
				}
			}
		}
	}
	
	public void turnPartInterruption(Boolean interrupt){
		if (interrupt){
			this.turnPart.remove(0);
			this.turnPart2.addAll(this.turnPart);
			this.turnPart = new ArrayList<String>();
		}else{
			this.specialTurnNames.remove(0);
			this.playerSpecialTurns.remove(0);
			this.turnPart.addAll(this.turnPart2);
			this.turnPart2 = new ArrayList<String>();
		}
		this.interruption = interrupt;
	}
	
	public void changeFirstPlayer(String playerName){
		if (this.playerTurns.contains(playerName)){
			while (!this.playerTurns.get(0).equals(playerName)){
				String currentFirstPlayer = this.playerTurns.get(0);
	    		this.playerTurns.remove(0);
	    		this.playerTurns.add(currentFirstPlayer);
			}
		}
	}
	
	public void drawChosenPlanet(String planetName, String playerName){
		Planet planetToDraw = null;
		for (Planet planet:this.planetDeck){
			if (planet.getName().equals(planetName)){
				planetToDraw = planet;
				break;
			}
		}
		if (planetToDraw != null){
			this.planetDeck.remove(planetToDraw);
			this.getPlayer(playerName).addPlanet(planetToDraw);
		}
	}
	
	private void checkVictory(){
		String potentialWinner = null;
		if (this.defeatedPlayers.size() > this.playerList.size() - 2){
			for (String playerName:this.playerList.keySet()){
				if (!this.defeatedPlayers.contains(playerName)){
					potentialWinner = playerName;
					break;
				}
			}
		}else{
			byte potentialWinnerPoints = 14;
			int resourceAmount = 0;
			for (String playerName2:this.playerList.keySet()){
				StarcraftPlayer player = this.playerList.get(playerName2);
				if (player.getConquestPoint() > potentialWinnerPoints){
					potentialWinner = playerName2;
					potentialWinnerPoints = player.getConquestPoint();
					int playerResourceAmount = 0;
					for (List<Integer> coordinates:player.getAreaResourceList().keySet()){
						PlanetArea area = player.getAreaResourceList().get(coordinates);
						playerResourceAmount += area.getGasResources() +area.getMineralResources();
					}
					resourceAmount = playerResourceAmount;
				}else if (player.getConquestPoint() == potentialWinnerPoints && potentialWinnerPoints > 14){
					int playerResourceAmount = 0;
					for (List<Integer> coordinates:player.getAreaResourceList().keySet()){
						PlanetArea area = player.getAreaResourceList().get(coordinates);
						playerResourceAmount += area.getGasResources() +area.getMineralResources();
					}
					if (playerResourceAmount > resourceAmount){
						resourceAmount = playerResourceAmount;
						potentialWinner = playerName2;
					}
				}
			}
		}
		if (potentialWinner != null){
			this.winner = potentialWinner;
			this.turnName = "winnerScreen";
		}
	}
	
	private void checkUnits(){
		Set<String> playerWithoutUnits = new HashSet<String>();
		playerWithoutUnits.addAll(this.playerList.keySet());
		for (long unitId:this.galaxy.getUnitList().keySet()){
			if (playerWithoutUnits.isEmpty()){
				break;
			}else{
				StarcraftUnit unit = this.galaxy.getUnitList().get(unitId);
				if (unit.getType().equals("mobile") || unit.getType().equals("base")){
					playerWithoutUnits.remove(unit.getOwner());
				}
			}
		}
		this.defeatedPlayers.addAll(playerWithoutUnits);
		for (String defeatedPlayer:playerWithoutUnits){
			this.getPlayer(defeatedPlayer).getAreaResourceList().clear();
		}
	}
	
	public String getRoadColor(){
		String result = this.colorList.get(colorUsed);
		if (this.colorUsed > this.colorList.size() - 2){
			this.colorUsed = 0;
		}else{
			this.colorUsed++;
		}
		return result;
	}
	
	public String getCurrentOrderCoordinates(){
		String coordinates = null;
		if (this.currentOrder != null){
			int planetX = this.galaxy.getAllPlanets().get(this.galaxy.getPlanetEvent()).getX();
			int planetY = this.galaxy.getAllPlanets().get(this.galaxy.getPlanetEvent()).getY();
			coordinates = Integer.toString(planetX) +"." + Integer.toString(planetY);
		}
		return coordinates;
	}
	
	public JSONObject getCurrentOrderJson(String action){
		StarcraftPlayer starcraftPlayer = this.getPlayer(this.currentOrder.getOwner());
		String coordinates = getCurrentOrderCoordinates();
		JSONObject result = null;
		if (this.currentOrder != null){
			try {
				result = new JSONObject()
						.put("action", action)
						.put("coorddinates", coordinates)
						.put("name", this.currentOrder.getName())
						.put("color", starcraftPlayer.getPlayerColor())
						.put("special", this.currentOrder.getSpecial());
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return result;
	}
	
	/**fin d'un tour d'exécution d'ordre**/
	public void endOrderExecution(String playerName){
		StarcraftPlayer player = this.playerList.get(playerName);
		player.setActiveSpecialOrderBonus(false);
		//on enlève l'affichage du bonus spécial
		GlobalMethods.clearByClass(playerName, "menuItem");
		player.resetTempBonuses();
		this.currentOrder = null;
	}
	
	/**initialisation des cartes évènements**/
	private void initializeEventCards(){
		try {
			URL resources = getClass().getClassLoader().getResource("../../starcraftResources/eventCardList.xml");
			File fXmlFile = new File(resources.toURI());
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(fXmlFile);
			doc.getDocumentElement().normalize();

			NodeList ageList = doc.getElementsByTagName("age");
			//tous les noeuds planet
			for (int i = 0; i < ageList.getLength(); i++){
				Element ageNode = (Element) ageList.item(i);
				NodeList cardList = ageNode.getElementsByTagName("card");
				//tous les noeuds road
				for (int j = 0; j < cardList.getLength(); j++){
					EventCard newCard = new EventCard(cardIdGenerator);
					Element cardNode = (Element) cardList.item(j);
					newCard.setName(cardNode.getAttribute("name"));
					newCard.setType(cardNode.getAttribute("type"));
					if (cardNode.hasAttribute("turnName")){
						newCard.setTurnName(cardNode.getAttribute("turnName"));
					}
					Element bonusNode = (Element) (cardNode.getElementsByTagName("bonus").item(0));
					newCard.setBonusName(bonusNode.getAttribute("name"));
					Element textNode = (Element) (cardNode.getElementsByTagName("text").item(0));
					newCard.setText(textNode.getTextContent());
					NodeList bonusUnitList = cardNode.getElementsByTagName("bonusUnit");
					for (int k = 0; k < bonusUnitList.getLength(); k++){
						Element bonusUnitNode = (Element) bonusUnitList.item(k);
						newCard.addUnitToCard(bonusUnitNode.getAttribute("species"), bonusUnitNode.getAttribute("name"));
					}
					if (i == 0){
						this.eventCards1.add(newCard);
					}
				}
			}
			Collections.shuffle(this.eventCards1);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**fonction ajoutant les points de conquêtes pour tous les joueurs**/
	private void addConquestPoints(){
		for (String planetName:this.galaxy.getAllPlanets().keySet()){
			for (PlanetArea area:this.galaxy.getAllPlanets().get(planetName).getAreaList()){
				if (area.getConquestPoints() > 0 && area.getUnitIdList().size() > 0){
					for (long unitId:area.getUnitIdList()){
						StarcraftUnit unit = this.galaxy.getUnitList().get(unitId);
						StarcraftPlayer player = this.playerList.get(unit.getOwner());
						byte addedPoints = (byte) area.getConquestPoints();
						player.setConquestPoint((byte) (player.getConquestPoint() + addedPoints));
						break;
					}
				}
			}
		}
		for (String playerName:this.playerList.keySet()){
			StarcraftPlayer starcraftPlayer = this.playerList.get(playerName);
			if (starcraftPlayer.getBonusList().contains("Overmind")){
				starcraftPlayer.setConquestPoint((byte) (starcraftPlayer.getConquestPoint() + 1));
			}
		}
	}
	
	/**distribue les zones de ressources aux différents joueurs**/
	private void setPlayerResources(){
		for (String playerName:this.playerList.keySet()){
			StarcraftPlayer starcraftPlayer = this.playerList.get(playerName);
			for (String planetName:this.galaxy.getAllPlanets().keySet()){
				Planet currentPlanet = this.galaxy.getAllPlanets().get(planetName);
				if (this.galaxy.unitOnPlanet(starcraftPlayer, currentPlanet, "base")){
					for (PlanetArea area:currentPlanet.getAreaList()){
						if (area.getMineralResources() + area.getGasResources() > 0){
							List<Integer> coordinates = Arrays.asList(currentPlanet.getX(), currentPlanet.getY(), area.getId());
							if (!this.alreadyOwnedResourceAreas.containsKey(coordinates)){
								if (this.galaxy.countFriendlyUnitsInPlace(coordinates.get(0),
										coordinates.get(1),
										coordinates.get(2), "", starcraftPlayer) > 0){
									this.alreadyOwnedResourceAreas.put(coordinates, area);
									starcraftPlayer.addResourceArea(currentPlanet.getX(), currentPlanet.getY(), area);
								}else if (this.galaxy.countEnemyUnitsInPlace(coordinates.get(0),
										coordinates.get(1),
										coordinates.get(2), "", starcraftPlayer) == 0){
									Boolean enemyBase = false;
									for (String hostilePlayerName:this.playerList.keySet()){
										if (!hostilePlayerName.equals(playerName)){
											StarcraftPlayer hostilePlayer = this.playerList.get(hostilePlayerName);
											if (this.galaxy.unitOnPlanet(hostilePlayer, currentPlanet, "base")){
												enemyBase = true;
												break;
											}
										}
									}
									if (!enemyBase){
										this.alreadyOwnedResourceAreas.put(coordinates, area);
										starcraftPlayer.addResourceArea(currentPlanet.getX(), currentPlanet.getY(), area);
									}
								}
							}
						}
					}
				}
			}
		}
	}
	
	public void removeAllTurnParts(){
		while (this.turnPart.size() > 0){
			this.turnPart.remove(0);
		}
	}
	
	public void addSpecialTurn(String playerName, String playerTurn){
		this.playerSpecialTurns.add(0, playerName);
		this.specialTurnNames.add(0, playerTurn);
	}
	
	public void addSpecialTurn(String playerName, String playerTurn, int index){
		this.playerSpecialTurns.add(index, playerName);
		this.specialTurnNames.add(index, playerTurn);
	}
	
	public void removeSpecialTurn(int index){
		this.playerSpecialTurns.remove(index);
		this.specialTurnNames.remove(index);
	}
	
	public int specialTurnSize(){
		return this.playerSpecialTurns.size();
	}
	
	public void addSpecialTurnEnd(String playerName, String playerTurn){
		this.playerSpecialTurns.add(playerName);
		this.specialTurnNames.add(playerTurn);
	}
	
    // cette fonction initialise la partie et
    // est appelée une unique fois : au moment de la création de la partie
    public void initializeGame(){
    	setRandomTurns();
    	setPlanetDeck();
    	initializeEventCards();
    	turnName = "factionChoice";
    }
    
    private void setRandomTurns(){
    	for (String playerNames : playerList.keySet()){
    		playerTurns.add(playerNames);
    	}
    	Collections.shuffle(playerTurns);
    }
    
    /**exécuter l'ordre en cours**/
    public void executeCurrentOrder() {
    	StarcraftPlayer starcraftPlayer = this.playerList.get(this.getPlayerCurrentlyPlaying());
    	if (this.galaxy.getsSpecialBenefit(starcraftPlayer)){
    		starcraftPlayer.setActiveSpecialOrderBonus(true);
    	}else{
    		if (this.currentOrder.getSpecial()){
        		starcraftPlayer.useSpecialOrder();
        	}
    	}
    	if (this.currentOrder.getName().equals("move")){
    		this.turnPart.add(GameConstants.moveUnitTurnName);
    		this.turnPart.add("endExecutionTurn");
    		addConclavefleetTurn();
    	}else if (this.currentOrder.getName().equals("build")){
    		Planet currentPlanet = this.galaxy.getAllPlanets().get(this.galaxy.getPlanetEvent());
    		if (starcraftPlayer.getActiveSpecialOrderBonus()){
    			starcraftPlayer.addTempBonus("costReduction");
    		}
    		if (this.galaxy.unitOnPlanet(starcraftPlayer, currentPlanet, "base")){
    			this.turnPart.add(GameConstants.buildUnitsTurnName);
    			this.turnPart.add(GameConstants.buildBuildingsTurnName);
    			this.turnPart.add(GameConstants.buildModuleTurnName);
    			this.turnPart.add("endExecutionTurn");
    		}else{
    			this.turnPart.add(GameConstants.buildBuildingsTurnName);
    			this.turnPart.add(GameConstants.buildModuleTurnName);
    			this.turnPart.add(GameConstants.buildBaseTurnName);
    			this.turnPart.add("endExecutionTurn");
    		}
    	}else if (this.currentOrder.getName().equals("research")){
    		starcraftPlayer.getEventCards().drawEventCard(this);
    		starcraftPlayer.drawCombatCards(3);
    		this.turnPart.add(GameConstants.buyResearchTurnName);
    		this.turnPart.add("endExecutionTurn");
    	}
	}
    
    /**indique si le joueur peut : faire une recherche, une construction, un mouvement ou un ordre spécial**/
    public Boolean[] checkActionValidity(){
    	StarcraftPlayer starcraftPlayer = this.playerList.get(this.getPlayerCurrentlyPlaying());
    	Planet currentPlanet = this.galaxy.getAllPlanets().get(this.galaxy.getPlanetEvent());
    	Boolean canResearch = false;
    	Boolean canBuild = false;
    	Boolean canMove = false;
    	Boolean canUseSpecial = false;
    	if (this.galaxy.unitOnPlanet(starcraftPlayer, currentPlanet, "base")){
    		canResearch = true;
    		canBuild = true;
    	}
    	if (this.galaxy.unitOnPlanet(starcraftPlayer, currentPlanet, "mobile")){
    		canMove = true;
    		canBuild = true;
    	}else{
    		for (Planet planet:this.galaxy.getLinkedPlanets(currentPlanet, this.getPlayerCurrentlyPlaying())){
				if (this.galaxy.unitOnPlanet(starcraftPlayer, planet, "mobile")){
					canMove = true;
	    			break;
	    		}
    		}
    	}
    	if (starcraftPlayer.canUseSpecialOrder()){
    		canUseSpecial = true;
    	}
    	Boolean[] result = new Boolean[]{canResearch, canBuild, canMove, canUseSpecial};
    	return result;
    }
      
    /**exécute la pile d'ordre situé sur les coordonnées choisies, si l'ordre ne peut être exécuté, on ne fait rien ce qui entraine
     * le passage au tour suivant**/
    public void executeOrderAt(String coordinates){
    	int separatorIndex = coordinates.indexOf('.');
		int xCoord  = Integer.parseInt(coordinates.substring(0, separatorIndex));
		int yCoord  = Integer.parseInt(coordinates.substring(separatorIndex + 1));
		Planet currentPlanet = this.galaxy.returnPlanetAt(xCoord, yCoord);
    	this.galaxy.setPlanetEvent(currentPlanet.getName());
    	OrderToken order = this.galaxy.getOrderList().get(coordinates).get(0);
    	StarcraftPlayer starcraftPlayer = this.playerList.get(this.getPlayerCurrentlyPlaying());
    	if (!order.getSpecial() || starcraftPlayer.canUseSpecialOrder()){
    		if (order.getName().equals("research")){
        		if (this.galaxy.unitOnPlanet(starcraftPlayer, currentPlanet, "base")){
        			this.currentOrder = order;
            		this.turnPart.add(GameConstants.executeChoiceTurnName);
        		}else{
        			starcraftPlayer.getEventCards().drawEventCard(this);
        		}
        	}else if (order.getName().equals("build")){
        		if (this.galaxy.unitOnPlanet(starcraftPlayer, currentPlanet, "base")
        				|| this.galaxy.unitOnPlanet(starcraftPlayer, currentPlanet, "mobile")){
        			this.turnPart.add(GameConstants.executeChoiceTurnName);
        			this.currentOrder = order;
        		}else{
        			starcraftPlayer.getEventCards().drawEventCard(this);
        		}
        	}else if (order.getName().equals("move")){
        		Boolean hasValidUnit = false;
        		if (starcraftPlayer.getUnitPools().containsKey(GameConstants.moveUnitTurnName)){
        			if (starcraftPlayer.getUnitPools().get(GameConstants.moveUnitTurnName).getUnitList().size() > 0){
        				hasValidUnit = true;
        			}
        		}
        		if (!hasValidUnit){
        			if (this.galaxy.unitOnPlanet(starcraftPlayer, currentPlanet, "mobile")){
            			hasValidUnit = true;
            		}else{
            			for (Planet planet:this.galaxy.getLinkedPlanets(currentPlanet, this.getPlayerCurrentlyPlaying())){
            				if (this.galaxy.unitOnPlanet(starcraftPlayer, planet, "mobile")){
            	    			hasValidUnit = true;
            	    			break;
            	    		}
                		}
            		}
        		}
        		if (hasValidUnit){
        			this.currentOrder = order;
        			this.turnPart.add(GameConstants.executeChoiceTurnName);
        		}else{
        			starcraftPlayer.getEventCards().drawEventCard(this);
        		}
        	}else if (order.getName().equals("starOrder")){
        		Boolean validOrder = false;
        		if (this.galaxy.unitOnPlanet(starcraftPlayer, currentPlanet, "base")){
        			validOrder = true;
        		}else if (this.galaxy.unitOnPlanet(starcraftPlayer, currentPlanet, "mobile")){
        			validOrder = true;
        		}else{
        			for (Planet planet:this.galaxy.getLinkedPlanets(currentPlanet, this.getPlayerCurrentlyPlaying())){
        				if (this.galaxy.unitOnPlanet(starcraftPlayer, planet, "mobile")){
        					validOrder = true;
        	    			break;
        	    		}
            		}
        		}
        		if (validOrder){
        			this.currentOrder = order;
        			this.turnPart.add("starOrderTurn");
        			this.turnPart.add(GameConstants.executeChoiceTurnName);
        		}else{
        			starcraftPlayer.getEventCards().drawEventCard(this);
        		}
        	}
    	}else{
    		starcraftPlayer.getEventCards().drawEventCard(this);
    	}
    	try {
			this.galaxy.removeOrder(coordinates);
		} catch (Exception e) {
			e.printStackTrace();
		}
    }
    
    //TODO prendre en compte les déplacements des bases terran
    /**renvoie toutes les unités sélectionnables par le joueur**/
    public ArrayList<Long> returnAllActiveUnits(){
    	ArrayList<Long> result = new ArrayList<Long>();
    	StarcraftPlayer activePlayer = this.playerList.get(getPlayerCurrentlyPlaying());
    	if (activePlayer.getUnitPools().containsKey(this.getTurnPart())){
    		UnitPool activePool = activePlayer.getUnitPools().get(this.getTurnPart());
    		for (long unitIdPool:activePool.getUnitList().keySet()){
    			result.add(unitIdPool);
        	}
    	}
    	if (this.getTurnPart().equals(GameConstants.placeBaseTurnName) || this.getTurnPart().equals("placeUnit")){
    		for (long unitId:this.galaxy.getUnitList().keySet()){
        		if (this.galaxy.getUnitList().get(unitId).getStartingSituation().equals(GameConstants.startingUnitSituation)){
        			result.add(unitId);
        		}
        	}
    	}else if (this.getTurnPart().equals(GameConstants.moveUnitTurnName)){
    		Planet currentPlanet = this.galaxy.getAllPlanets().get(this.galaxy.getPlanetEvent());
    		for (PlanetArea area:currentPlanet.getAreaList()){
    			for (long unitId:area.getUnitIdList()){
    				//récupération de toutes les unités mobiles amies
    				StarcraftUnit unit = this.galaxy.getUnitList().get(unitId);
    				if (unit.getOwner().equals(this.getPlayerCurrentlyPlaying())){
    					if (unit.getType().equals("mobile")){
    						result.add(unitId);
    					}
    				}
    			}
    		}
    		for (Planet planet:this.galaxy.getLinkedPlanets(currentPlanet, this.getPlayerCurrentlyPlaying())){
    			for (PlanetArea area:planet.getAreaList()){
    				for (long unitId:area.getUnitIdList()){
        				StarcraftUnit unit = this.galaxy.getUnitList().get(unitId);
        				if (unit.getOwner().equals(this.getPlayerCurrentlyPlaying())){
        					if (unit.getType().equals("mobile")){
        						result.add(unitId);
        					}
        				}
        			}
    			}
    		}
    	}else if (this.getTurnPart().equals(GameConstants.moveRetreatUnitTurnName)){
    		StarcraftBattle battle = this.galaxy.getStarcraftBattle();
    		result = battle.getRetreatingUnits(this.getPlayerCurrentlyPlaying(), this);
    	}else if (this.getTurnPart().equals("warpgateTurn")){
    		for (long unitId:this.galaxy.getUnitList().keySet()){
    			if (this.galaxy.getUnitList().get(unitId).getStartingSituation().equals("warpgate")){
    				result.add(unitId);
    			}
    		}
    	}else if (this.getTurnPart().equals("conclaveFleetTurn")){
    		for (long unitId:this.galaxy.getUnitList().keySet()){
    			if (this.galaxy.getUnitList().get(unitId).getStartingSituation().equals("conclaveFleet")){
    				result.add(unitId);
    			}
    		}
    	}
    	return result;
    }
    
    /**récupère toutes les planètes à partir du fichier planetList.xml**/
	private void setPlanetDeck(){
		try {
			URL resources = getClass().getClassLoader().getResource("../../starcraftResources/planetList.xml");
			File fXmlFile = new File(resources.toURI());
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(fXmlFile);
			doc.getDocumentElement().normalize();

			NodeList planetList = doc.getElementsByTagName("planet");
			//tous les noeuds planet
			for (int i = 0; i < planetList.getLength(); i++){
				Element planetNode = (Element) planetList.item(i);
				Planet planet = new Planet();
				String planetName = planetNode.getAttribute("name");
				planet.setName(planetName);
				NodeList roadList = planetNode.getElementsByTagName("road");
				//tous les noeuds road
				for (int j = 0; j < roadList.getLength(); j++){
					Element roadNode = (Element) roadList.item(j);
					String roadPosition = roadNode.getAttribute("position");
					planet.addRoad(Integer.parseInt(roadPosition));
				}
				//tous les noeuds area
				NodeList areaList = planetNode.getElementsByTagName("area");
				for (int k = 0; k < areaList.getLength(); k++){
					Element areaNode = (Element) areaList.item(k);
					PlanetArea area = new PlanetArea();
					area.setAreaType(areaNode.getAttribute("unit"));
					area.setUnitLimit(Integer.parseInt(areaNode.getAttribute("unitLimit")));
					NodeList resourceList = areaNode.getElementsByTagName("resource");
					//tous les noeuds resource
					for (int j = 0; j < resourceList.getLength(); j++){
						Element resourceNode = (Element) resourceList.item(j);
						AreaResource resource = new AreaResource();
						resource.setResourceAmount(Integer.parseInt(resourceNode.getAttribute("amount")));
						resource.setResourceType(resourceNode.getAttribute("type"));
						area.addResource(resource);
					}
					//ajout d'une zone à la planète
					planet.addArea(area);
				}
				planetDeck.add(planet);
			}
			Collections.shuffle(planetDeck);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void discardPlanetDeck(){
		this.planetDeck = null;
	}
	
    public SaveGame getSaveGame() {
        return saveGame;
    }
    
	public Planet drawPlanet(){
		Planet result = planetDeck.get(0);
		planetDeck.remove(0);
		return result;
	}
	
	public List<Planet> getAllPlanets(){
		return this.planetDeck;
	}
	
    public void setSaveGame( SaveGame saveGame ) {
        this.saveGame = saveGame;
    }
    
    public long getId() {
        return saveGame.getId();
    }
	
    public void addPlayer(StarcraftPlayer player){
    	this.playerList.put(player.getName(), player);
    }
    
    public Map<String, StarcraftPlayer> getPlayerList(){
    	return this.playerList;
    }
    
    public StarcraftPlayer getPlayer(String playerName){
    	return this.playerList.get(playerName);
    }
    
    public void remove(String player){
    	this.playerList.remove(player);
    }
    
    /**fini la bataille en cours puis passe au tour suivant (faire attention à ce qu'aucuns tours spéciaux ne restent)**/
    private void endBattle(){
    	for (long id:this.galaxy.getUnitList().keySet()){
			StarcraftUnit unit = this.galaxy.getUnitList().get(id);
			unit.updateOldCoordinates();
			unit.setStartingSituation(GameConstants.inGalaxySituation);
		}
    	StarcraftPlayer attacker = this.galaxy.getStarcraftBattle().getAttackingPlayer();
    	StarcraftPlayer defender = this.galaxy.getStarcraftBattle().getDefendingPlayer();
    	if (attacker.getSpecies().equals("Zerg")){
    		attacker.drawCombatCards(1);
    	}
    	if (defender.getSpecies().equals("Zerg")){
    		defender.drawCombatCards(1);
    	}
    	this.galaxy.setStarcraftBattle(null);
    	GameTurnHandler.callClearBattleStage(this);
    	defender.resetTempBonuses();
    	endOrderExecution(attacker.getName());
    	if (currentTurn == playerTurns.size() - 1){
    		currentTurn = 0;
		}else{
			currentTurn++;
		}
	}
    
    /** regarde si le tour actuel doit être automatisé (le joueur n'a pas de choix d'action)
     * si le tour est automatisé, on accompli les tâches à accomplir puis on passe ou tour suivant**/
    public void isAutomatedTurn(){
    	Boolean isAutomated = false;
    	//si un joueur a perdu, il ne peut pas jouer
    	if (this.defeatedPlayers.contains(getPlayerCurrentlyPlaying())){
    		isAutomated = true;
    	}else{
    		if (this.getTurnPart().equals(GameConstants.placeBaseTurnName)){
        		StarcraftPlayer starcraftPlayer = this.playerList.get(getPlayerCurrentlyPlaying());
        		if (starcraftPlayer.getUnitPools().get(GameConstants.placeBaseTurnName).getUnitList().isEmpty()){
        			isAutomated = true;
        		}
        	}else if (this.getTurnPart().equals(GameConstants.placeZRoadTurnName)){
        		if (this.galaxy.returnAvailableRoads().size() == 0){
        			isAutomated = true;
        		}
        	}else if (this.getTurnPart().equals(GameConstants.galaxyOrderChoiceTurnName)){
        		String playerName = getPlayerCurrentlyPlaying();
        		if (this.galaxy.getAllValidOrdersCoordinates(playerName).size() == 0){
        			if (this.galaxy.hasRemainingOrders(playerName)){
        				this.getPlayer(playerName).getEventCards().drawEventCard(this);
        			}
        			isAutomated = true;
        		}
        	}else if (this.getTurnPart().equals("destroyUnits") || this.getTurnPart().equals("galaxyUnitDestruction")){
        		StarcraftBattle battle = this.galaxy.getStarcraftBattle();
        		if (battle.getAttackingPlayer().getName().equals(getPlayerCurrentlyPlaying())
        				&& battle.getAttackingUnitsToDestroy().size() == 0){
        			isAutomated = true;
        		}else if (battle.getDefendingPlayer().getName().equals(getPlayerCurrentlyPlaying())
        				&& battle.getDefendingUnitsToDestroy().size() == 0){
        			isAutomated = true;  			
        		}
        	}else if (this.getTurnPart().equals(GameConstants.moveRetreatUnitTurnName)){
        		StarcraftBattle battle = this.galaxy.getStarcraftBattle();
        		ArrayList<Long> retreatingUnits = battle.getRetreatingUnits(getPlayerCurrentlyPlaying(), this);
        		if (retreatingUnits.size() == 0){
        			isAutomated = true;
        		}
        	}else if (this.getTurnPart().equals("discardHand")){
        		StarcraftPlayer starcraftPlayer = this.playerList.get(getPlayerCurrentlyPlaying());
        		if (starcraftPlayer.getCardLimit() >= starcraftPlayer.getCombatCardsInHand().size()){
        			isAutomated = true;
    			}
        	}else if (this.getTurnPart().equals(GameConstants.eventCardTurnName)){
        		StarcraftPlayer starcraftPlayer = this.playerList.get(getPlayerCurrentlyPlaying());
        		if (starcraftPlayer.getEventCards().getEventCardList().size()
        				+ starcraftPlayer.getEventCards().getActionCards().size() < 1){
        			isAutomated = true;
        		}
        	}else if (this.getTurnPart().equals("switchPlayerOrder") || this.getTurnPart().equals("emptyTurn")){
        		isAutomated = true;
        	}else if (this.getTurnPart().equals("endExecutionTurn")){
        		endOrderExecution(getPlayerCurrentlyPlaying());
        		isAutomated = true;
        	}else if (this.getTurnPart().equals("glimpseTheFuture")){
        		this.turnName = "placeUnit";
        		StarcraftPlayer starcraftPlayer = this.playerList.get(getPlayerCurrentlyPlaying());
        		ArrayList<RoadLink> linkList = this.galaxy.getAllPossibleLinkEvent(starcraftPlayer, this.turnName);
        		this.createTransport(starcraftPlayer, linkList);
        		isAutomated = true;
        	}else if (this.getTurnPart().equals("initializeResourcesToken")){
        		StarcraftPlayer starcraftPlayer = this.playerList.get(getPlayerCurrentlyPlaying());
        		starcraftPlayer.initializeResourcesTokenToChose();
        		isAutomated = true;
        	}else if (this.getTurnPart().equals("endOfTurn")){
        		isAutomated = true;
        	}else if (this.getTurnPart().equals("delayedBattleStart")){
        		this.galaxy.getStarcraftBattle().setSkirmishNumber(this);
        		isAutomated = true;
        	}else if (this.getTurnPart().equals("endBattleTurn")){
        		this.endBattle();
        		isAutomated = true;
        	}else if (this.getTurnPart().equals("removeTransportsFromAttackers")){
        		this.galaxy.getStarcraftBattle().removeTransportsFromAttackers(this.galaxy);
        		isAutomated = true;
        	}else if (this.getTurnPart().equals("buyBonusResearchTempBonus")){
        		StarcraftPlayer starcraftPlayer = this.playerList.get(getPlayerCurrentlyPlaying());
        		starcraftPlayer.addTempBonus("costReduction");
				starcraftPlayer.addTempBonus("buyBonusResearch");
        		isAutomated = true;
        	}else if (this.getTurnPart().equals("exhaustResourceAreaTempBonus")){
        		StarcraftPlayer starcraftPlayer = this.playerList.get(getPlayerCurrentlyPlaying());
        		starcraftPlayer.addTempBonus("exhaustArea1");
        		isAutomated = true;
        	}else if (this.getTurnPart().equals("researchAndResourceToken")){
        		StarcraftPlayer starcraftPlayer = this.playerList.get(getPlayerCurrentlyPlaying());
        		starcraftPlayer.getEventCards().researchAndResourceTokenBonus();
        		isAutomated = true;
        	}
    	}
    	//si le tour est automatisé, on passe directement au tour suivant
    	if (isAutomated){
    		nextTurn();
    	}
    }
    
    /**divise les tours en plusieurs parties**/
    public void setTurnParts(){
    	if (this.getTurnName().equals("planetChoice")){
    		this.turnPart.add("placePlanet");
    		this.turnPart.add(GameConstants.placeBaseTurnName);
    	} else if (this.getTurnName().equals("executeOrder")){
    		this.turnPart.add(GameConstants.galaxyOrderChoiceTurnName);
    	}
    }
    
    /** renvoie dans quelle partie du tour on est si le tour est décomposé en plusieurs parties
    renvoie le nom du tour sinon**/
    public String getTurnPart(){
    	String result = "";
    	if (this.turnPart.size() == 0){
    		result = this.getTurnName();
    	}else{
    		result = this.turnPart.get(0);
    	}
    	//System.out.println("getTurnPart : " + result);
    	return result;
    }

    
    /**passage au joueur suivant ou au prochain tour**/
    public void nextTurn(){
    	Boolean changedTurn = false;
    	if (this.turnPart.size() < 1){
    		if (!this.interruption){
    			changedTurn = true;
    		}
    		if (this.playerSpecialTurns.isEmpty()){
    			//déroulement normal du tour
    			if (currentTurn == playerTurns.size() - 1){
    				currentTurn = 0;
    				setTurnName();
    			}else{
    				currentTurn++;
    			}
    		}else{
    			this.playerSpecialTurns.remove(0);
    			this.specialTurnNames.remove(0);
    		}
    	}else{
    		this.turnPart.remove(0);
    		if (this.turnPart.size() < 1){
    			nextTurn();
    		}
    	}
    	//gestion des tours spéciaux donnés par les cartes ou autres
    	ArrayList<SpecialTurnEvent> keptEvents = new ArrayList<SpecialTurnEvent>();
    	for (SpecialTurnEvent specialTurnEvent:this.speciaTurnEvents){
    		if (specialTurnEvent.triggerSpecialTurn(this.getPlayerCurrentlyPlaying(), this.getTurnName())){
    			ArrayList<String> specialTurns = specialTurnEvent.getNewPlayerTurns();
    			for (String specialPlayerTurn:specialTurns){
    				this.playerSpecialTurns.add(specialPlayerTurn);
    				this.specialTurnNames.add(specialTurnEvent.getSpecialTurnName());
    			}
    		}else{
    			keptEvents.add(specialTurnEvent);
    		}
    	}
    	this.speciaTurnEvents = keptEvents;
    	if (changedTurn){
    		setTurnParts();
    	}
    	isAutomatedTurn();
    	playEventsAtTurnStart();
    }
    
    private void playEventsAtTurnStart(){
    	if (this.getTurnPart().equals("placeCombatCards")){
    		StarcraftBattle battle = this.galaxy.getStarcraftBattle();
    		if (this.getPlayerCurrentlyPlaying().equals(battle.getAttackingPlayer().getName())){
    			battle.applyPlayerBonuses();
    		}
    	}
    }
    
    //TODO ajouter les tours manquants
    /**passage des différents types de tours (choix des faction, phase de planification, phase d'exécution, etc...)**/
    private void setTurnName(){
    	if (turnName.equals("factionChoice")){
    		turnName = "leadershipChoice";
    	}else if (turnName.equals("leadershipChoice")){
    		turnName = "planetChoice";
    	}else if (turnName.equals("planetChoice")){
    		turnName = GameConstants.placeZRoadTurnName;
    	}else if (turnName.equals(GameConstants.placeZRoadTurnName)){
    		turnName = "placeUnit";
    	}else if (turnName.equals("placeUnit")){
    		//à la fin du tour de placement des unités, on distribue les ressources
    		setPlayerResources();
    		turnName = GameConstants.planningPhaseTurnName + "1";
    	}else if (turnName.equals(GameConstants.planningPhaseTurnName + "1")){
    		turnName = GameConstants.planningPhaseTurnName + "2";
    	}else if (turnName.equals(GameConstants.planningPhaseTurnName + "2")){
    		turnName = GameConstants.planningPhaseTurnName + "3";
    	}else if (turnName.equals(GameConstants.planningPhaseTurnName + "3")){
    		turnName = GameConstants.planningPhaseTurnName + "4";
    	}else if (turnName.equals(GameConstants.planningPhaseTurnName + "4")){
    		turnName = "executeOrder";
    	}else if (turnName.equals("executeOrder")){
    		if (this.galaxy.getOrderList().size() == 0){
    			triggerEndOfTurnEvents();
    			turnName = "endOfTurn";
    			//à faire, vérifier si on a changé d'âge et faire toutes les actions de mises à jour du plateau
    			//destruction de base, mettre les ressources, etc...
    			//turnName = "eventCardChoice";
    		}
    	}else if (turnName.equals("endOfTurn")){
    		endFullTurn();
    	}else if (turnName.equals(GameConstants.eventCardTurnName)){
    		turnName = "discardHand";
    	}else if (turnName.equals("discardHand")){
    		//une fois les cartes de combat défaussées, on change les tours des joueurs
    		this.triggerStartOfTurnEvents();
    		turnName = "switchPlayerOrder";
    		String currentFirstPlayer = this.playerTurns.get(0);
    		this.playerTurns.remove(0);
    		this.playerTurns.add(currentFirstPlayer);
    	}else if (turnName.equals("switchPlayerOrder")){
    		turnName = GameConstants.planningPhaseTurnName + "1";
    	}
    }
    
    /**fin du tour, on enlève et rajoute les cartes ressources et détruit les bases et travailleurs**/
    private void endFullTurn(){
    	this.currentTurn = 0;
		turnName = GameConstants.eventCardTurnName;
    	//suppression des bases
    	Set<Long> basesToRemove = new HashSet<Long>();
    	for (long unitId:this.galaxy.getUnitList().keySet()){
    		StarcraftUnit unit = this.galaxy.getUnitList().get(unitId);
    		if (unit.getType().equals("base") || unit.getType().equals("installation")){
    			StarcraftPlayer owner = this.playerList.get(unit.getOwner());
    			if (this.galaxy.countEnemyUnitsInPlace(unit.getCoordinates()[0],
    					unit.getCoordinates()[1],
    					unit.getCoordinates()[2], "mobile", owner) > 0){
    				basesToRemove.add(unitId);
    			}
    		}
    	}
    	for (long removedUnit:basesToRemove){
    		this.galaxy.removeUnit(removedUnit, this);
    		GameTurnHandler.removeUnitDisplay(removedUnit, this);
    	}
    	Set<Long> transportsToRemove = new HashSet<Long>();
    	for (long unitId:this.galaxy.getUnitList().keySet()){
    		StarcraftUnit unit = this.galaxy.getUnitList().get(unitId);
    		if (unit.getType().equals("transport")){
    			StarcraftPlayer owner = this.playerList.get(unit.getOwner());
    			RoadLink link = this.galaxy.getLinkFromCoordinates(unit.getCoordinates());
    			Planet planet1 = this.galaxy.returnPlanetAt(link.getCoordinates1()[0], link.getCoordinates1()[1]);
    			Planet planet2 = this.galaxy.returnPlanetAt(link.getCoordinates2()[0], link.getCoordinates2()[1]);
    			if (!this.galaxy.unitOnPlanet(owner, planet1, "base")
    					&& !this.galaxy.unitOnPlanet(owner, planet2, "base")){
    				transportsToRemove.add(unitId);
    			}
    		}
    	}
    	for (long removedUnit:transportsToRemove){
    		this.galaxy.removeUnit(removedUnit, this);
    		GameTurnHandler.removeUnitDisplay(removedUnit, this);
    	}
    	//récupération et suppression des travailleurs, pertes des zones de ressources
    	for (String playerName:this.playerList.keySet()){
    		StarcraftPlayer player = this.playerList.get(playerName);
    		player.setSpecialOrderUsed(0);
    		player.setAvailableWorkers(player.getAvailableWorkers() + player.getUnavailableWorkers()
    		+ player.getWorkerOnGas() + player.getWorkerOnMineral());
    		player.setUnavailableWorkers(0);
    		player.setWorkerOnGas(0);
    		player.setWorkerOnMineral(0);
    		Set<List<Integer>> areasToRemove = new HashSet<List<Integer>>();
    		for (List<Integer> areaCoordinates:player.getAreaResourceList().keySet()){
    			PlanetArea area = player.getAreaResourceList().get(areaCoordinates);
    			if (this.galaxy.countEnemyUnitsInPlace(areaCoordinates.get(0),
    					areaCoordinates.get(1),
    					areaCoordinates.get(2), "mobile", player) > 0){
    				//enlève les une zone de ressource et détruit les travailleurs présents si cette perte est due à la
    				//présence d'unités ennemies
    				areasToRemove.add(areaCoordinates);
    			}else if (!this.galaxy.unitOnPlanet(player,
    					this.galaxy.returnPlanetAt(areaCoordinates.get(0), areaCoordinates.get(1)), "base")){
    				//enlève les une zone de ressource et récupère les travailleurs présents si cette perte est due à
    				//la perte d'une base
    				player.setAvailableWorkers(player.getAvailableWorkers() + area.getWorkerAmount());
    				areasToRemove.add(areaCoordinates);
    			}else{
    				player.setAvailableWorkers(player.getAvailableWorkers() + area.getWorkerAmount());
    			}
    			area.setWorkerAmount(0);
    			GameTurnHandler.updateAreaWorkerDisplay(areaCoordinates, player, this);
    		}
    		for (List<Integer> coordinatesToRemove:areasToRemove){
    			player.getAreaResourceList().remove(coordinatesToRemove);
    			this.alreadyOwnedResourceAreas.remove(coordinatesToRemove);
			}
    		player.loadAvailableOrders();
    		//récupération de recharge si nécessaire
        	if (player.getBonusList().contains("recharge")){
        		player.setHasRecharge(true);
        	}
    	}
    	//redistribution des ressources
    	setPlayerResources();
    	addConquestPoints();
    	this.checkUnits();
    	this.checkVictory();
    	for (String playerName:this.playerList.keySet()){
    		GameTurnHandler.updateDisplay(playerName, this);
    	}
    }
    
    /**renvoie le nom tu tour actuel**/
    public String getTurnName(){
    	String currentTurnName = "";
    	if (this.playerSpecialTurns.isEmpty()){
    		currentTurnName = this.turnName;
    	}else{
    		currentTurnName = this.specialTurnNames.get(0);
    	}
    	return currentTurnName;
    }
    
    /**renvoie le nom du joueur en train de jouer**/
    public String getPlayerCurrentlyPlaying(){
    	String currentPlayer ="";
    	if (this.playerSpecialTurns.isEmpty()){
    		currentPlayer = playerTurns.get(currentTurn);
    	}else{
    		currentPlayer = this.playerSpecialTurns.get(0);
    	}
    	return currentPlayer;
    }
    
    public ArrayList<String> getPlayerTurns(){
    	return this.playerTurns;
    }
    
    public int getCurrentTurnNumber(){
    	int currentTurnNumber = -1;
    	if (playerSpecialTurns.isEmpty()){
    		currentTurnNumber = this.currentTurn;
    	}
    	return currentTurnNumber;
    }
    
    public ArrayList<String> getTurnList(){
    	return playerTurns;
    }

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public Galaxy getGalaxy() {
		return galaxy;
	}

	public void setGalaxy(Galaxy galaxy) {
		this.galaxy = galaxy;
	}
	
	public void addSpecialEvent(SpecialTurnEvent specialTurnEvent){
		this.speciaTurnEvents.add(specialTurnEvent);
	}
	
	public void addSpecialEvent(int index, SpecialTurnEvent specialTurnEvent){
		this.speciaTurnEvents.add(index, specialTurnEvent);
	}

	public void addTurnPart(String partName){
		this.turnPart.add(partName);
	}

	public OrderToken getCurrentOrder() {
		return currentOrder;
	}

	public void setCurrentOrder(OrderToken currentOrder) {
		this.currentOrder = currentOrder;
	}

	public ArrayList<EventCard> getEventCards1() {
		return eventCards1;
	}
	
	private void createTransport(StarcraftPlayer starcraftPlayer, ArrayList<RoadLink> linkList){
		for (RoadLink link:linkList){
			StarcraftUnit starcraftunit = new StarcraftUnit("overlord", this.unitIdGenerator);
			starcraftunit.setOwner(starcraftPlayer.getName());
			starcraftunit.setColor(starcraftPlayer.getPlayerColor());
			starcraftunit.setCoordinates(link.getCoordinates1(), this);
			starcraftunit.setOldCoordinates(link.getCoordinates1());
			starcraftunit.setStartingSituation(GameConstants.inGalaxySituation);
			this.getGalaxy().addUnit(starcraftunit);
			GameTurnHandler.updateUnitDisplay(starcraftunit.getId(), this);
		}
	}

	public String getWinner() {
		return winner;
	}
	
	private void triggerEndOfTurnEvents(){
		for (String playerName:this.playerTurns){
			StarcraftPlayer player = this.playerList.get(playerName);
			if (player.getBonusList().contains("storageFacilities")){
				if (player.getAvailableWorkers() > 0){
					this.addSpecialTurnEnd(playerName, "initializeResourcesToken");
					this.addSpecialTurnEnd(playerName, "getResourcesToken");
				}
			}
		}
	}
	
	private void triggerStartOfTurnEvents(){
		for (String playerName:this.playerTurns){
			StarcraftPlayer player = this.playerList.get(playerName);
			if (player.getBonusList().contains("warpgate")){
				if (this.galaxy.prepareWarpgate(player)){
					this.addSpecialTurnEnd(playerName, "warpgateTurn");
				}
			}
			if (player.getBonusList().contains("RichesOfAiur")){
				player.addTempBonus("restoreArea1");
				player.addTempBonus("restoreArea2");
				this.addSpecialTurnEnd(playerName, "restoreResourceArea");
			}
		}
	}

}
